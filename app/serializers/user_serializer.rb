class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :created_at, :updated_at, :first_name, :last_name,
             :account_id, :full_name, :about, :phone, :avatar_thumb_url,
             :avatar_medium_url

  def avatar_thumb_url
    object.avatar.url(:thumb) if object.avatar.file?
  end

  def avatar_medium_url
    object.avatar.url(:medium) if object.avatar.file?
  end
end
