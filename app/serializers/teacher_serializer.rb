class TeacherSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :available_in_morning, :available_in_afternoon

  has_many :levels

  class LevelSerializer < ActiveModel::Serializer
    attributes :name
  end
end
