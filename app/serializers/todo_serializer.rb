class TodoSerializer < ActiveModel::Serializer
  attributes :id, :body, :complete
end
