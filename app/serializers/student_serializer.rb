class StudentSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :hours_left, :available_in_morning, :available_in_afternoon

  has_one :pricing_plan

  has_many :levels

  class LevelSerializer < ActiveModel::Serializer
    attributes :id, :name
  end

  class PricingPlanSerializer < ActiveModel::Serializer
    attributes :name
  end

  def hours_left
    ClassAttendance.hours_left(object, class_schedule_id) / 5
  end
end
