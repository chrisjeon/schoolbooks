class RoomSerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :class_sessions do
    start_time = scope.start_end_time.start_on

    object
      .class_sessions
      .includes(:start_end_time, teacher: [:levels], students: [:levels])
      .joins(:start_end_time)
      .where(
        'start_end_times.start_on > ? AND start_end_times.end_on < ?',
        start_time.beginning_of_day,
        start_time.end_of_day
      )
      .where(class_schedule_id: scope.id)
      .order('start_end_times.start_on')
  end

  class ClassSessionSerializer < ActiveModel::Serializer
    attributes :path, :room_id

    has_one :start_end_time
    has_many :students
    belongs_to :teacher

    def path
      Rails.application.routes.url_helpers.ui_v1_class_session_path(object.id)
    end

    class StartEndTimeSerializer < ActiveModel::Serializer
      TIME_FORMAT = '%H:%M'.freeze

      attributes :start_on, :end_on

      def start_on
        object.start_on.strftime(TIME_FORMAT)
      end

      def end_on
        object.end_on.strftime(TIME_FORMAT)
      end
    end

    class UserSerializer < ActiveModel::Serializer
      attributes :id, :full_name

      has_many :levels

      class LevelSerializer < ActiveModel::Serializer
        attributes :name
      end
    end
  end
end
