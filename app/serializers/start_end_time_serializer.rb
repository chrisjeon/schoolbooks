class StartEndTimeSerializer < ActiveModel::Serializer
  attributes :start_on, :end_on
end
