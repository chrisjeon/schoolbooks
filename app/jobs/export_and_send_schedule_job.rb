require 'csv'

class ExportAndSendScheduleJob < ApplicationJob
  TIME_FORMAT = '%H:%M'

  queue_as :default

  def perform(schedule)
    sessions = class_sessions(schedule)

    csv_string = CSV.generate({ col_sep: ',', quote_char: '"', headers: true, force_quotes: true }) do |csv|
      csv << %w(Name Teacher Class Timetable)

      map_rows(sessions).sort_by(&:first).each { |row| csv << row }
    end

    ScheduleExportsMailer.schedule(involved_emails(sessions), csv_string).deliver_later
  end

  private

  def class_sessions(schedule)
    start_time = schedule.start_end_time.start_on

    schedule
      .class_sessions
      .joins(:start_end_time)
      .includes(:room, :teacher, :students, :start_end_time)
      .where(
        'start_end_times.start_on > ? AND start_end_times.end_on < ?',
        start_time.beginning_of_day,
        start_time.end_of_day
      )
  end

  def map_rows(class_sessions)
    class_sessions.each_with_object([]) do |class_session, memo|
      times = class_session.start_end_time
      room_name = class_session.room.name
      teacher_full_name = class_session.teacher.full_name
      account = times.account
      time = "#{times.start_on.in_time_zone(account.time_zone).strftime(TIME_FORMAT)}-#{times.end_on.in_time_zone(account.time_zone).strftime(TIME_FORMAT)}"

      class_session.students.each { |student| memo << [student.full_name, teacher_full_name, room_name, time] }
    end
  end

  def involved_emails(class_sessions)
    class_sessions.each_with_object({}) do |class_session, memo|
      memo[class_session.teacher.email] = true

      class_session.students.each do |student|
        memo[student.email] = true
      end
    end.keys
  end
end
