class CreateAndSendInvoicesJob < ApplicationJob
  queue_as :default

  def perform(account)
    payments_creator_service = PaymentsCreatorService.new(account)
    payments_creator_service.process!
  end
end
