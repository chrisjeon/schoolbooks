class EnqueueCreateAndSendInvoicesJob < ApplicationJob
  queue_as :default

  def perform
    today    = DateTime.now.strftime('%A')
    accounts = Account.where(invoices_sent_on: ['Everyday', today])
    accounts.each { |account| CreateAndSendInvoicesJob.perform_later(account) }
  end
end
