class EnqueuePaymentItemsCreatorsJob < ApplicationJob
  queue_as :default

  def perform
    Account.find_each { |account| CreatePaymentItemsJob.perform_later(account) }
  end
end
