class CreatePaymentItemsJob < ApplicationJob
  queue_as :default

  def perform(account)
    PaymentItemsCreatorService.new(account).process!
  end
end
