class ProcessIntegrationsJob < ApplicationJob
  queue_as :default

  def perform(integration)
    integration_processor_service = IntegrationProcessorService.new(integration)
    integration_processor_service.process!
  end
end
