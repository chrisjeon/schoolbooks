class StripeService
  attr_reader :account_subscription, :stripe_customer

  def initialize(account_subscription)
    @account_subscription = account_subscription
    load_stripe_customer if @account_subscription.stripe_customer_token
  end

  def create_subscription!(stripe_card_token)
    @account_subscription.stripe_card_token = stripe_card_token
    @account_subscription.create_stripe_customer_and_save_with_payment
    load_stripe_customer
    stripe_subscription = stripe_customer.
                          subscriptions.
                          create(plan: account_subscription.reload.subscription_plan.stripe_plan_id)
    account_subscription.reload.start!
    account_subscription.reload.update_attribute(:stripe_billing_end_date, stripe_subscription.current_period_end.to_s)
  end

  def resume_subscription!
    stripe_subscription = stripe_customer.
                          subscriptions.
                          create(plan: account_subscription.reload.subscription_plan.stripe_plan_id,
                                 trial_end: account_subscription.reload.stripe_billing_end_date.to_i)
    account_subscription.reload.resume!
    account_subscription.reload.update_attribute(:stripe_billing_end_date, stripe_subscription.current_period_end.to_s)
  end

  def pause_subscription!
    subscription_id = stripe_customer.subscriptions.first.id
    stripe_subscription = stripe_customer.subscriptions.retrieve(subscription_id).delete
    account_subscription.reload.pause!
    account_subscription.reload.update_attribute(:stripe_billing_end_date, stripe_subscription.current_period_end.to_s)
  end

  def update_subscription!
    subscription_id = stripe_customer.subscriptions.first.id
    stripe_subscription = stripe_customer.subscriptions.retrieve(subscription_id)
    stripe_subscription.plan = account_subscription.reload.subscription_plan.stripe_plan_id
    stripe_subscription.save
  end

  private

  def load_stripe_customer
    @stripe_customer = Stripe::Customer.retrieve(account_subscription.reload.stripe_customer_token)
  end
end
