class MailgunService
  attr_reader :mailgun_client, :message_params, :batch_builder

  def initialize(message_params)
    @mailgun_client = Mailgun::Client.new ENV['MAILGUN_API_KEY']
    @message_params = message_params
    @batch_builder = Mailgun::BatchMessage.new(@mailgun_client, ENV['MAILGUN_DOMAIN'])
  end

  def process!
    mailgun_client.send_message ENV['MAILGUN_DOMAIN'], message_params
  end
end
