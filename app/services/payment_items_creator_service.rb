class PaymentItemsCreatorService
  attr_reader :account

  def initialize(account)
    @account = account
  end

  def process!
    account.users.students.active.each do |user|
      PaymentItem::ITEMABLE_TYPES.each do |itemable_type|
        payment_items = account.payment_items.where(itemable_type: itemable_type)
        itemables     = itemable_type.constantize.where(user: user, account: @account)
        itemables     = itemables.where.not(id: payment_items.pluck(:itemable_id))
        due_on_date   = Date.today.next_week.advance(days: account.payments_due_on_index - 1)
        itemables.each do |itemable|
          PaymentItem.create!(account: account,
                              itemable: itemable,
                              user: itemable.user,
                              due_on: due_on_date)
        end
      end
    end
  end
end
