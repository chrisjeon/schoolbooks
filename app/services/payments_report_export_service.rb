require 'csv'

class PaymentsReportExportService
  attr_reader :account, :start_date, :end_date, :payment_items

  def initialize(account, start_date = Date.today - 30.days, end_date = Date.today)
    @account         = account
    @start_date      = start_date
    @end_date        = end_date
    @payment_items   = account.
                       payment_items.
                       where(created_at: (start_date - 1.day)..(end_date + 1.day))
  end

  PaymentItemCsvMap = Struct.new(:payment_item) do
    def user_name
      payment_item.user.full_name
    end

    def user_id
      payment_item.user.id
    end

    def type
      payment_item.itemable_type
    end

    def price
      payment_item.price.to_s
    end

    def paid
      payment_item.paid?
    end

    def paid_on
      payment_item.paid? ? payment_item.paid_on.strftime('%m/%d/%Y') : 'N/A'
    end

    def paid_with
      payment_item.payment.payment_method
    end
  end

  def process!
    attributes = %w(user_name user_id type price paid paid_on paid_with)

    CSV.generate(headers: true) do |csv|
      csv << attributes

      payment_items.each do |payment_item|
        payment_item_csv_map = PaymentItemCsvMap.new(payment_item)
        csv << attributes.map { |attr| payment_item_csv_map.send(attr) }
      end
    end
  end
end
