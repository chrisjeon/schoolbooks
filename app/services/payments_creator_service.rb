class PaymentsCreatorService
  attr_reader :account

  def initialize(account)
    @account = account
  end

  def process!
    unpaid_payment_items  = account.payment_items.unpaid
    grouped_payment_items = unpaid_payment_items.group_by(&:user_id)
    grouped_payment_items.each do |user_id, payment_items|
      earliest_payment_due = payment_items.sort_by(&:due_on).first
      payment = Payment.create(account: account,
                               user_id: user_id,
                               due_on: earliest_payment_due.due_on)
      payment_items.each do |payment_item|
        payment_item.update_attributes(payment: payment)
      end

      UserMailer.send_invoice_email(payment).deliver_later
    end
  end
end
