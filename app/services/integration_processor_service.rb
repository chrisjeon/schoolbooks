class IntegrationProcessorService
  require 'csv'

  attr_reader :integration, :integration_type

  def initialize(integration)
    @integration = integration
    @integration_type = integration.class.name.underscore
  end

  def process!
    CSV.foreach(integration.file.path, headers: true, header_converters: :symbol) do |row|
      Callbacks.new.send integration_type, integration, row
    end
  end

  class Callbacks
    def integration_user(integration, row)
      errors = []
      row_hash = integration.csv_row_to_hash(row)
      boolean_integration_columns = integration.class::DEFAULT_INTEGRATION_COLUMNS.select do |default_column|
        default_column[:type] == integration.class::INTEGRATION_TYPE_MESSAGES[:boolean]
      end
      boolean_integration_columns.map { |column| column[:column_name].to_sym }.each do |boolean_key|
        row_hash[boolean_key].downcase!
      end
      unique_attribute = row_hash[:unique_attribute]
      unique_attribute_value = row_hash[unique_attribute]

      assoc_record = User.
                     find_or_initialize_by(
                       account_id: integration.account_id,
                       unique_attribute => unique_attribute_value
                     )
      assoc_record.assign_attributes(row_hash.delete_if { |key, _value| key == :unique_attribute })

      if assoc_record.valid?
        assoc_record.save
      else
        errors << assoc_record.attributes
      end
    end

    def integration_plan_enrollment(integration, row)
      errors = []

      account = integration.account
      pricing_plan = PricingPlan.find_by(account_id: account.id,
                                         code: row[:pricing_plan_code].strip)
      user = User.find_by(account_id: account.id,
                          email: row[:user_email].strip)
      start_on = Date.strptime(row[:start_on].strip, '%m/%d/%Y')
      end_on = Date.strptime(row[:end_on].strip, '%m/%d/%Y')
      price_object = Money.new(row[:price].strip, account.currency)

      plan_enrollment = PlanEnrollment.
                        find_or_initialize_by(
                          account_id: account.id,
                          pricing_plan_id: pricing_plan.id,
                          user_id: user.id
                        )

      plan_enrollment.price = price_object

      return if plan_enrollment.start_end_time.present? && plan_enrollment.start_end_time.start_on == start_on && plan_enrollment.start_end_time.end_on == end_on

      plan_enrollment.build_start_end_time if plan_enrollment.new_record?
      plan_enrollment.start_end_time.start_on = start_on
      plan_enrollment.start_end_time.end_on = end_on
      plan_enrollment.start_end_time.account = account

      if plan_enrollment.valid?
        plan_enrollment.save
      else
        errors << plan_enrollment.attributes
      end
    end

    def integration_housing(integration, row)
      errors = []
      attributes = integration.csv_row_to_hash(row)

      unique_attribute = attributes.delete(:unique_attribute)

      housing = Housing.find_or_initialize_by(
                  account_id: integration.account_id,
                  unique_attribute => attributes[unique_attribute]
                )

      housing.assign_attributes(attributes)

      if housing.valid?
        housing.save
      else
        errors << housing.attributes
      end
    end

    def integration_housing_occupancy(integration, row)
      errors = []
      attributes = integration.csv_row_to_hash(row)

      housing_occupancy = HousingOccupancy.find_or_initialize_by(
                            user_id: User.find_by(full_name: attributes.delete(:full_name)).id,
                            housing_id: Housing.find_by(owner_name: attributes.delete(:owner_name)).id,
                            account_id: integration.account_id
                          )

      attributes[:start_end_time_attributes][:account_id] = integration.account_id

      housing_occupancy.assign_attributes(attributes)

      if housing_occupancy.valid?
        housing_occupancy.save
      else
        errors << housing_occupancy.attributes
      end
    end

    def method_missing(name, *args)
      # noop
    end
  end
end
