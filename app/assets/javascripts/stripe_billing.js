document.addEventListener('turbolinks:load', function() {
  // Billing section
  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'));
  subscription.setupForm();
});

var subscription = {
  setupForm: function() {
    $('form#payment_method').on('submit', function(e) {
      e.preventDefault();
      $('form#payment_method input[type=submit]').attr('disabled', true);
      subscription.processCard();
    });
  },

  processCard: function() {
    var card = {
      name: $('#name_on_card').val(),
      number: $('#card_number').val(),
      address_zip: $('#postal_code').val(),
      exp_month: $('#card_month').val(),
      exp_year: $('#card_year').val(),
      cvc: $('#cvc').val()
    };

    Stripe.createToken(card, subscription.handleStripeResponse);
  },

  handleStripeResponse: function(status, response) {
    if (status === 200) {
      console.log(response.id);
      $('input[type="hidden"]#stripe_card_token').val(response.id);
      $('form#payment_method')[0].submit();
    } else {
      $('#stripe_error').text(response.error.message);
      $('form#payment_method input[type=submit]').attr('disabled', false);
    }
  }
};
