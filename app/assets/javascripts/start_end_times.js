document.addEventListener('turbolinks:load', function() {
  /*
   * Handles start on and end on for StartEndTime forms
   */
  $('.pick-a-time-start-end-time').pickatime();

  $('.pick-a-date-start-end-time').pickadate({
    format: 'm/d/yyyy',
    formatSubmit: 'm/d/yyyy'
  });

  var startDate = $('#pick_a_date_start_date').val();
  var endDate = $('#pick_a_date_end_date').val();
  $('#start_date_hidden_field').val(startDate);
  $('#end_date_hidden_field').val(endDate);

  var startTime = $('#pick_a_date_start_time').val();
  var endTime = $('#pick_a_date_end_time').val();
  $('#start_time_hidden_field').val(startTime);
  $('#end_time_hidden_field').val(endTime);

  $('#pick_a_date_start_date').on('change', function() {
    var date = $(this).val();
    $('#start_date_hidden_field').val(date);
  });

  $('#pick_a_date_end_date').on('change', function() {
    var date = $(this).val();
    $('#end_date_hidden_field').val(date);
  });

  $('#pick_a_date_start_time').on('change', function() {
    var date = $(this).val();
    $('#start_time_hidden_field').val(date);
  });

  $('#pick_a_date_end_time').on('change', function() {
    var date = $(this).val();
    $('#end_time_hidden_field').val(date);
  });
});
