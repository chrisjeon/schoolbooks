document.addEventListener('turbolinks:load', function() {
  /*
   * Handles clickable table rows for payment items
   */
  $('.clickable-row').on('click', function() {
    window.document.location = $(this).data('href');
  });
});
