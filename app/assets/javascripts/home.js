document.addEventListener('turbolinks:load', function() {
  $('#header-search-input').on('submit', function(e) {
    e.preventDefault();
    var searchTerm = $(this).find('input[type="text"]').val();
    window.location = '/users?' + 'search_term=' + searchTerm;
  });

  /*
   * Handle search input bar in users index page
   */
  $('#user-search-input').on('keypress', function(e) {
    if (e.which === 13) {
      var controllerName = $('#user-search-input').data('controller-name');
      window.location = '/' + controllerName + '?' + 'search_term=' + this.value;
    }
  });

  /*
   * Handle search input button in users index page
   */
  $('#user-search-input-btn').on('click', function() {
    var searchTerm = $('#user-search-input').val();
    var controllerName = $('#user-search-input').data('controller-name');
    window.location = '/' + controllerName + '?' + 'search_term=' + searchTerm;
  });

  /*
   *
   */
  $('.toggle').toggles({
    on: true
  });
});
