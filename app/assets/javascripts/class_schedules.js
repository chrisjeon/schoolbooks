//= require export-class-schedule

document.addEventListener('turbolinks:load', function() {
  /*
   * Handles toggling of maximum students per class
   */
  $('#pricing-plan-private-plan-toggle').toggles({
    on: $('#pricing_plan_private_plan').is(':checked')
  });

  $('#pricing-plan-private-plan-toggle').on('toggle', function(e, active) {
    var privatePlanCheckBox = $('#pricing_plan_private_plan');
    privatePlanCheckBox.prop('checked', active);
  });

  /*
   * Handles start on and end on for class schedule forms
   */
  $('.pick-a-date-class-schedule').pickadate({
    format: 'm/d/yyyy',
    formatSubmit: 'm/d/yyyy'
  });
  $('.pick-a-time-event-class-schedule').pickatime();

  window.initClassSchedulesExportListener();
});
