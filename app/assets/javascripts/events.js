document.addEventListener('turbolinks:load', function() {
  /*
   * Handles toggling of has_sessions
   */
  $('#has-sessions-toggle').toggles({
    on: $('#event_has_sessions').is(':checked')
  });

  $('#has-sessions-toggle').on('toggle', function(e, active) {
    var hasSessionsCheckBox = $('#event_has_sessions');
    hasSessionsCheckBox.prop('checked', active);
  });

  /*
   * Handles start on and end on for event forms
   */
  $('.pick-a-date-event').pickadate({
    format: 'm/d/yyyy',
    formatSubmit: 'm/d/yyyy'
  });
  $('.pick-a-time-event').pickatime();
});
