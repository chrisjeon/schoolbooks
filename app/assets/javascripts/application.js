// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require webpack-bundle
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require cable
//= require bootstrap
//= require modernizr.min
//= require pace.min
//= require wow.min
//= require jquery.scrollTo.min
//= require jquery.nicescroll
//= require moment-2.2.1
//= require waypoints.min
//= require jquery.counterup.min
//= require easy-piechart/easypiechart.min
//= require easy-piechart/jquery.easypiechart.min
//= require easy-piechart/example.js
//= require c3-chart/d3.v3.min
//= require c3-chart/c3
//= require morris/morris.min
//= require morris/raphael.min
//= require sparkline-chart/jquery.sparkline.min
//= require sparkline-chart/chart-sparkline
//= require sweet-alert/sweet-alert.min
//= require sweet-alert/sweet-alert.init
//= require modal-effect/classie
//= require modal-effect/modalEffects
//= require jquery.app
//= require jquery.chat
//= require pickadate/picker
//= require pickadate/picker.date
//= require pickadate/picker.time
//= require toggles.min
//= require home
//= require events
//= require class_schedules
//= require start_end_times
//= require payment_items
//= require notify.min
//= require notify-metro
//= require notifications
//= require stripe_billing
