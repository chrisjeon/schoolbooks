window.initClassSchedulesExportListener = function() {
  $('.export-class-schedule').on('click', function() {
    var path = this.dataset.path;

    swal({
      text: 'Do you want to send the schedule CSV to every student and teacher involved?',
      type: 'warning',
      title: 'Are you sure?',
      closeOnConfirm: false,
      showCancelButton: true,
      confirmButtonText: 'Yes!',
      confirmButtonColor: '#DD6B55',
    }, function() {
      $.ajax({
        url: path,
        method: 'POST',
        dataType: 'script',
      })
      .done(function() {
        swal('Ok!', 'The export has been scheduled.', 'success');
      })
      .fail(function() {
        swal('Not Ok!', 'Some error happened :(', 'error');
      });
    });
  });
};
