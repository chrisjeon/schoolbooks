class RoomPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(account: user.account) if user.admin?
    end
  end

  def index?
    user.admin? && user.account == record.account
  end

  def new?
    user.admin? && user.account == record.account
  end

  def show?
    user.admin? && user.account == record.account
  end

  def create?
    user.admin? && user.account == record.account
  end

  def update?
    user.admin? && user.account == record.account
  end

  def destroy?
    user.admin? && user.account == record.account
  end
end
