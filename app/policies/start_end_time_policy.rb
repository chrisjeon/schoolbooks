class StartEndTimePolicy < ApplicationPolicy
  def new?
    user.admin? && record.account == user.account
  end

  def edit?
    user.admin? && record.account == user.account
  end

  def create?
    user.admin? && record.account == user.account
  end

  def udpate?
    user.admin? && record.account == user.account
  end

  def destroy?
    user.admin? && record.account == user.account
  end
end
