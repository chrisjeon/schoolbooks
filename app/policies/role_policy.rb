class RolePolicy < ApplicationPolicy
  def create?
    user.admin?
  end

  def show?
    return true if record.default?
    user.admin? && (user.account == record.account)
  end

  def update?
    user.admin? && (user.account == record.account)
  end

  def destroy?
    user.admin? && (user.account == record.account)
  end
end
