class ScheduleExportPolicy
  attr_reader :user

  def initialize(user, _record)
    @user = user
  end

  def create?
    user.admin?
  end
end
