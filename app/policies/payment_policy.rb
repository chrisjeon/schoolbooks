class PaymentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(account: user.account) if user.admin?
    end
  end

  def show?
    return false unless record.account == user.account
    user.admin? || (record.user == user)
  end

  def edit?
    user.admin? && record.account == user.account
  end

  def update?
    user.admin? && record.account == user.account
  end
end
