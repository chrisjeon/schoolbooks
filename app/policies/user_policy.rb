class UserPolicy < ApplicationPolicy
  def create?
    user.admin?
  end

  def destroy?
    user.admin? && record != user
  end

  def update?
    user.admin? || record == user
  end
end
