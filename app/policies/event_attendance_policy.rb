class EventAttendancePolicy < ApplicationPolicy
  def create?
    user.admin? && user.account == record.account
  end

  def destroy?
    user.admin? && user.account == record.account
  end
end
