class LevelMembershipPolicy < ApplicationPolicy
  def create?
    user.admin? && record.account == user.account
  end

  def update?
    user.admin? && record.account == user.account
  end

  def destroy?
    user.admin? && record.account == user.account
  end
end
