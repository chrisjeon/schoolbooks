class RegistrationFormPolicy < ApplicationPolicy
  def show?
    user.admin? && user.account == record.account
  end

  def update?
    user.admin? && user.account == record.account
  end
end
