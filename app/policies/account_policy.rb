class AccountPolicy < ApplicationPolicy
  def show?
    user.admin? && user.account == record
  end

  def update?
    user.admin? && user.account == record
  end
end
