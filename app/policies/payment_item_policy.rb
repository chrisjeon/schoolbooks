class PaymentItemPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(account: user.account) if user.admin?
    end
  end

  def new?
    user.admin? && record.account == user.account
  end

  def show?
    user.admin? && record.account == user.account
  end

  def edit?
    user.admin? && record.account == user.account
  end

  def create?
    user.admin? && record.account == user.account
  end

  def update?
    user.admin? && record.account == user.account
  end

  def destroy?
    user.admin? && record.account == user.account
  end
end
