class UsersRolePolicy < ApplicationPolicy
  def create?
    user.admin? && (record.user.account == user.account)
  end

  def destroy?
    user.admin? && (record.user.account == user.account)
  end
end
