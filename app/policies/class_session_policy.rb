class ClassSessionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(account: user.account) if user.admin?
    end
  end

  def new?
    user.admin? && record.account == user.account
  end

  alias_method :show?, :new?
  alias_method :edit?, :new?
  alias_method :create?, :new?
  alias_method :update?, :new?
  alias_method :destroy?, :new?
end
