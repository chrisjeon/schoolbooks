class TodoPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(account: user.account, user: user) if user.admin?
    end
  end

  def new?
    user.admin? && user.account == record.account
  end

  def show?
    user.admin? && user.account == record.account
  end

  def edit?
    user.admin? && user.account == record.account
  end

  def create?
    user.admin? && user.account == record.account
  end

  def update?
    belongs_to_account = if record.is_a?(Todo::ActiveRecord_Relation)
                           record.all? { |todo| user.account_id == todo.account_id }
                         else
                           user.account_id == record.account_id
                         end

    user.admin? && belongs_to_account
  end

  def destroy?
    user.admin? && user.account == record.account
  end
end
