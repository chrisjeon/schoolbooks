class ScheduleExportsMailer < ApplicationMailer
  FROM = 'info@schoolbookshq.com'

  def schedule(emails, schedule_csv)
    filename = 'schedule.csv'

    if Rails.env.production?
      file = Tempfile.new(filename)

      file.write(schedule_csv)
      file.rewind

      mg_service = MailgunService.new({})

      mg_service.batch_builder.from(FROM)
      mg_service.batch_builder.subject(I18n.t('weekly_schedule'))
      mg_service.batch_builder.body_html((render_to_string(template: 'schedule_exports_mailer/schedule')).to_str)
      mg_service.batch_builder.add_attachment(file, filename)

      emails.each do |email|
        mg_service.batch_builder.add_recipient(:to, email)
      end

      mg_service.batch_builder.finalize

      file.close
      file.unlink
    else
      attachments[filename] = schedule_csv

      mail(
        bcc: emails,
        from: FROM,
        subject: I18n.t('weekly_schedule')
      )
    end
  end
end
