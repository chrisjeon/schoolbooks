class UserMailer < ApplicationMailer
  def user_created_email(user, password)
    @user     = user
    @password = password
    if Rails.env.production?
      mg_service = MailgunService.new(
        from: 'info@schoolbookshq.com',
        to: @user.email,
        subject: I18n.t('your_schoolbooks_account_has_been_created'),
        html: (render_to_string(template: 'user_mailer/user_created_email')).to_str
      )
      mg_service.process!
    else
      mail(to: @user.email,
           from: 'info@schoolbookshq.com',
           subject: I18n.t('your_schoolbooks_account_has_been_created'))
    end
  end

  def send_invoice_email(payment)
    @user    = payment.user
    @account = payment.account
    @payment = payment
    if Rails.env.production?
      mg_service = MailgunService.new(
        from: 'info@schoolbookshq.com',
        to: @user.email,
        subject: I18n.t('you_have_a_new_invoice'),
        html: (render_to_string(template: 'user_mailer/send_invoice_email')).to_str
      )
      mg_service.process!
    else
      mail(to: @user.email,
           from: 'info@schoolbookshq.com',
           subject: I18n.t('you_have_a_new_invoice'))
    end
  end
end
