module UserAvailable
  NOON = '14:00'.freeze
  MORNING = '09:00'.freeze
  EVENING = '19:00'.freeze
  SHORT_TIME = '%H:%M'.freeze

  def available_at?(user, start_end_time)
    end_on = start_end_time.end_on.strftime(SHORT_TIME)
    start_on = start_end_time.start_on.strftime(SHORT_TIME)

    start_on >= (user.available_in_morning? ? MORNING : NOON) &&
    end_on <= (user.available_in_afternoon? ? EVENING : NOON)
  end
end
