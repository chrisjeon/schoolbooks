module Codeable
  extend ActiveSupport::Concern

  def set_code_if_blank
    self.code = "#{name.split(' ').reject(&:blank?).join('').upcase}-#{id}"
    self.save
  end
end
