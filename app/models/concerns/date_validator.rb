module DateValidator
  extend ActiveSupport::Concern

  def valid_start_and_end_dates?
    return if start_on.blank? || end_on.blank?
    errors.add(:start_on, I18n.t('must_be_less_than_end_date')) if start_on > end_on
    errors.add(:end_on, I18n.t('must_be_greater_than_start_date')) if end_on < start_on
  end
end
