class MiscPaymentItem < ApplicationRecord
  has_one :payment_item, as: :itemable, dependent: :destroy
  belongs_to :account
  belongs_to :user

  validates :account, presence: true
  validates :user, presence: true

  monetize :price_cents,
           allow_nil: false,
           with_model_currency: :currency

  def payable_amount
    price
  end
end
