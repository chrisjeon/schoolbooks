class Account < ApplicationRecord
  include Codeable

  INVOICES_SENT_ON_DAYS = %w(Everyday Sunday Monday Tuesday Wednesday
                             Thursday Friday Saturday)

  PAYMENTS_DUE_ON_DAYS = %w(Sunday Monday Tuesday Wednesday Thursday Friday Saturday)

  has_one :registration_form, dependent: :destroy
  has_one :account_subscription, dependent: :destroy
  has_one :subscription_plan, through: :account_subscription
  has_many :users, dependent: :destroy
  has_many :levels, dependent: :destroy
  has_many :level_memberships, dependent: :destroy
  has_many :pricing_plans, dependent: :destroy
  has_many :plan_enrollments, dependent: :destroy
  has_many :rooms, dependent: :destroy
  has_many :roles, dependent: :destroy
  has_many :housings, dependent: :destroy
  has_many :housing_occupancies
  has_many :events, dependent: :destroy
  has_many :event_sessions, dependent: :destroy
  has_many :event_attendances, dependent: :destroy
  has_many :class_schedules, dependent: :destroy
  has_many :class_sessions, dependent: :destroy
  has_many :class_attendances, dependent: :destroy
  has_many :start_end_times, dependent: :destroy
  has_many :payments, dependent: :destroy
  has_many :payment_items, dependent: :destroy
  has_many :misc_payment_items, dependent: :destroy
  has_many :todos, dependent: :destroy
  has_many :integrations, dependent: :destroy
  has_many :terms_of_services, dependent: :destroy
  has_many :terms_of_service_acceptances, dependent: :destroy
  has_many :registration_informations, dependent: :destroy

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :email, presence: true, email: true, uniqueness: { case_sensitive: false }
  validates :code, uniqueness: { case_sensitive: false }
  validates :invoices_sent_on, presence: true, inclusion: { in: INVOICES_SENT_ON_DAYS }
  validates :payments_due_on, presence: true, inclusion: { in: PAYMENTS_DUE_ON_DAYS }

  before_save :set_default_currency, unless: :currency
  after_save :create_registration_form, if: Proc.new { |account| account.has_registration_form? && account.registration_form.nil? }
  after_save :set_code_if_blank, if: Proc.new { |account| account.code.blank? }
  before_validation :set_default_payments_due_on,
                    if: Proc.new { |account| account.payments_due_on.blank? }

  accepts_nested_attributes_for :account_subscription

  def self.invoices_sent_on_collection
    INVOICES_SENT_ON_DAYS.collect { |day| [day.capitalize, day] }
  end

  def self.payments_due_on_collection
    PAYMENTS_DUE_ON_DAYS.collect { |day| [day.capitalize, day] }
  end

  def teachers_collection(teachers_to_exclude = [])
    (users.teachers.active - teachers_to_exclude).collect { |user| [user.full_name, user.id] }
  end

  def students_collection(students_to_exclude = [])
    (users.students.active - students_to_exclude).collect { |user| [user.full_name, user.id] }
  end

  def users_collection(users_to_exclude = [])
    (users - users_to_exclude).collect { |user| [user.full_name, user.id] }
  end

  def address_available?
    address.present? && city.present? && state.present? && zip_code.present?
  end

  def payments_due_on_index
    PAYMENTS_DUE_ON_DAYS.find_index(self.payments_due_on)
  end

  private

  def set_default_currency
    self.currency = 'USD'
  end

  def set_default_payments_due_on
    self.payments_due_on = 'Monday'
  end

  def create_registration_form
    RegistrationForm.create(account: self)
  end
end
