class ClassSession < ApplicationRecord
  include UserAvailable

  has_many :class_attendances, dependent: :destroy, inverse_of: :class_session
  has_many :students, through: :class_attendances, source: :user
  has_one :start_end_time, as: :timeable, dependent: :destroy, inverse_of: :timeable
  belongs_to :account
  belongs_to :class_schedule
  belongs_to :teacher, class_name: 'User', foreign_key: :user_id
  belongs_to :room

  validates :account, presence: true
  validates :class_schedule, presence: true
  validates :room, presence: true

  validate :teacher_availability, :room_availability, :day_availability

  accepts_nested_attributes_for :start_end_time
  accepts_nested_attributes_for :class_attendances, allow_destroy: true

  scope :in_between, -> (start_date, end_date) do
    joins("INNER JOIN start_end_times ON start_end_times.timeable_id = class_sessions.id AND start_end_times.timeable_type = 'ClassSession'").
      where('start_end_times.start_on >= ? AND start_end_times.end_on <= ?', start_date, end_date)
  end

  def with_siblings
    self
      .class
      .joins(:start_end_time)
      .where(
        "to_char(start_end_times.start_on, 'HH24:MI') = ?",
        start_end_time.start_on.utc.strftime('%H:%M')
      )
      .where(
        room_id: room_id,
        class_schedule_id: class_schedule_id
      )
      .order(:id)
  end

  def in_seconds
    start_end_time.end_on - start_end_time.start_on
  end

  def check_students_presence
    return if students.any?

    errors.add(:students, I18n.t('activerecord.errors.models.class_session.attributes.students.too_short', count: 1))
  end

  private

  def teacher_availability
    object_availability(teacher, :user_id, 'teacher')
  end

  def day_availability
    return if teacher.blank? || available_at?(teacher, start_end_time)

    errors.add(:teacher, 'The teacher is not available for given time')
  end

  def room_availability
    object_availability(room, :room_id, 'room')
  end

  def object_availability(object, foreign_key, label)
    return if object.blank?
    return if StartEndTime
                .joins(
                  <<-SQL.strip_heredoc
                    INNER JOIN "class_sessions"
                    ON "class_sessions"."id" = "start_end_times"."timeable_id"
                    AND "start_end_times"."timeable_type" = 'ClassSession'
                  SQL
                )
                .where(class_sessions: { foreign_key => object.id })
                .where(
                  '(start_on, end_on) OVERLAPS (:start, :end)',
                  end: start_end_time.end_on,
                  start: start_end_time.start_on
                )
                .none?

    errors.add(label, "The #{label} is not available for given time")
  end
end
