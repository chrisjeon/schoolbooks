class Room < ApplicationRecord
  has_many :class_sessions
  belongs_to :account

  validates :account, presence: true
  validates :name, presence: true
end
