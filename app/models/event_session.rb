class EventSession < ApplicationRecord
  has_many :event_attendances, dependent: :destroy
  has_many :attendees, through: :event_attendances, source: :user
  has_one :start_end_time, as: :timeable, dependent: :destroy
  belongs_to :account
  belongs_to :event

  validates :account, presence: true
  validates :event, presence: true

  accepts_nested_attributes_for :start_end_time
end
