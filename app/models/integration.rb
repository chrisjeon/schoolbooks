class Integration < ApplicationRecord
  INTEGRATION_TYPES = %w(IntegrationUser IntegrationPlanEnrollment IntegrationHousing IntegrationHousingOccupancy)
  INTEGRATION_TYPE_MESSAGES = {
    number: 'A number',
    two_hundred_characters: 'A character string up to 200 characters in length.',
    boolean: "A boolean value of either 'true' or 'false' (case sensitive).",
    foreign_key: 'Foreign key of related association (integer)'
  }

  belongs_to :account

  validates :account, presence: true

  has_attached_file :file
  validates_attachment :file,
                       presence: true,
                       content_type: { content_type: %w(text/comma-separated-values text/csv application/csv application/excel) }

  def self.integration_types_collection
    INTEGRATION_TYPES.collect do |integration_type|
      [integration_type.remove('Integration'), integration_type]
    end
  end
end
