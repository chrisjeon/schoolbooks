class IntegrationHousing < Integration
  ASSOC_KLASS = Housing

  DEFAULT_INTEGRATION_COLUMNS = [
    { default: nil, csv_map: 'A', is_required: true, column_name: 'owner_name', label_name: 'Owner name', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Owner name' },
    { default: nil, csv_map: 'B', is_required: true, column_name: 'address', label_name: 'Address', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Address' },
    { default: nil, csv_map: 'C', is_required: true, column_name: 'email', label_name: 'Email', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Email' },
    { default: nil, csv_map: 'D', is_required: true, column_name: 'phone', label_name: 'Phone', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Phone' },
    { default: nil, csv_map: 'E', is_required: false, column_name: 'description', label_name: 'Description', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Description' },
    { default: nil, csv_map: 'F', is_required: false, column_name: 'additional_services_and_costs',  label_name: 'Additional services and costs', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Additional services and costs' },
    { default: nil, csv_map: 'G', is_required: true, column_name: 'maximum_occupancy', label_name: 'Maximum occupancy', type: INTEGRATION_TYPE_MESSAGES[:number], description: 'Maximum occupancy' },
    { default: nil, csv_map: 'H', is_required: false, column_name: 'monthly_cost_cents', label_name: 'Monthly cost', type: INTEGRATION_TYPE_MESSAGES[:number], description: 'Monthly cost' },
    { default: nil, csv_map: 'I', is_required: false, column_name: 'weekly_cost_cents', label_name: 'Weekly cost', type: INTEGRATION_TYPE_MESSAGES[:number], description: 'Weekly cost' },
    { default: nil, csv_map: 'J', is_required: false, column_name: 'confirmed_host', label_name: 'Confirmed host', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Confirmed host' },
    { default: nil, csv_map: 'K', is_required: false, column_name: 'note', label_name: 'Note', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Note' },
    { default: nil, csv_map: 'L', is_required: false, column_name: 'url', label_name: 'URL', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'URL' }
  ]

  def csv_row_to_hash(row)
    row_hash = row.to_hash
    note = row_hash.delete(:note)
    row_hash[:unique_attribute] = :email

    row_hash[:note_attributes] = { body: note } if note.present?

    row_hash
  end
end
