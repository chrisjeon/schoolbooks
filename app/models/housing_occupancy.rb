class HousingOccupancy < ApplicationRecord
  has_many :payment_items, as: :itemable, dependent: :destroy
  has_one :start_end_time, as: :timeable, dependent: :destroy
  belongs_to :user
  belongs_to :account
  belongs_to :housing

  validates :user, presence: true
  validates :account, presence: true
  validates :housing, presence: true

  accepts_nested_attributes_for :start_end_time

  monetize :price_cents,
           allow_nil: false,
           with_model_currency: :currency

  scope :active, -> do
    joins("INNER JOIN start_end_times ON start_end_times.timeable_id = housing_occupancies.id AND start_end_times.timeable_type = 'HousingOccupancy'").
      where('start_end_times.end_on >= ?', Date.today)
  end

  scope :inactive, -> do
    joins("INNER JOIN start_end_times ON start_end_times.timeable_id = housing_occupancies.id AND start_end_times.timeable_type = 'HousingOccupancy'").
      where('start_end_times.end_on < ?', Date.today)
  end

  def payable_amount
    price
  end
end
