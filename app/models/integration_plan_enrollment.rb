class IntegrationPlanEnrollment < Integration
  ASSOC_KLASS = PlanEnrollment

  DEFAULT_INTEGRATION_COLUMNS = [
    { default: nil, csv_map: 'A', is_required: true, column_name: 'account_id', label_name: 'Email', type: INTEGRATION_TYPE_MESSAGES[:foreign_key], description: 'Account ID' },
    { default: nil, csv_map: 'B', is_required: true, column_name: 'user_id', label_name: 'Password', type: INTEGRATION_TYPE_MESSAGES[:foreign_key], description: 'User ID' },
    { default: nil, csv_map: 'C', is_required: true, column_name: 'pricing_plan_id', label_name: 'First Name', type: INTEGRATION_TYPE_MESSAGES[:foreign_key], description: 'Pricing Plan ID' }
  ]

  def csv_row_to_hash(row)
    row_hash = row.to_hash
  end
end
