class User < ApplicationRecord
  include Filterable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :async

  rolify

  has_attached_file :avatar,
                    styles: { medium: '300x300>', thumb: '100x100>' },
                    default_url: '/avatars/:style/missing.png'

  paginates_per 10

  attr_accessor :set_admin, :set_student, :set_teacher, :class_schedule_preference

  has_one :plan_enrollment, dependent: :destroy
  has_one :pricing_plan, through: :plan_enrollment
  has_one :registration_information, dependent: :destroy
  has_many :level_memberships, dependent: :destroy
  has_many :levels, through: :level_memberships
  has_many :housing_occupancies, dependent: :destroy
  has_many :housings, through: :housing_occupancies
  has_many :event_attendances, dependent: :destroy
  has_many :attended_events, through: :event_attendances, source: :event
  has_many :attended_event_sessions, through: :event_attendances, source: :event_session
  has_many :teaching_classes, class_name: 'ClassSession', foreign_key: :user_id
  has_many :class_attendances, dependent: :destroy
  has_many :class_sessions, through: :class_attendances
  has_many :payments, dependent: :destroy
  has_many :payment_items, dependent: :destroy
  has_many :misc_payment_items, dependent: :destroy
  has_many :todos, dependent: :destroy
  has_many :terms_of_service_acceptances, dependent: :destroy
  has_many :terms_of_services, through: :terms_of_service_acceptances

  belongs_to :account, optional: true

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates_attachment :avatar,
                       content_type: { content_type: /\Aimage\/.*\Z/ },
                       size: { in: 0..5.megabytes }

  before_save :update_full_name, :save_roles, :set_class_schedule_preference

  scope :search_term, -> (term) { where('lower(full_name) LIKE ? OR lower(email) LIKE ?', "%#{term.downcase}%", "%#{term.downcase}%") }
  scope :students, -> { joins(:roles).where('roles.name = ?', 'student') }
  scope :teachers, -> { joins(:roles).where('roles.name = ?', 'teacher') }
  scope :schedulable, -> { where(schedulable: true) }
  scope :active, -> { where(active: true) }
  scope :inquired, -> (account_id) { where(id: RegistrationInformation.where(account_id: account_id).pluck(:user_id), active: false) }

  def self.gender_options
    [['Male', 'male'], ['Female', 'female']]
  end

  def admin?
    Role.default_roles.pluck(:name).include?('admin')
  end

  def teacher?
    Role.default_roles.pluck(:name).include?('teacher')
  end

  def student?
    Role.default_roles.pluck(:name).include?('student')
  end

  def can_teach_all_levels?
    return false unless has_role?(:teacher)
    levels.none? || account.levels.count == levels.count
  end

  def group_student?
    !pricing_plan.try(:private_plan?)
  end

  def private_student?
    pricing_plan.try(:private_plan?)
  end

  private

  def update_full_name
    self.full_name = "#{first_name} #{last_name}"
  end

  def save_roles
    add_role :admin if !set_admin.nil? && set_admin == '1'
    add_role :student if !set_student.nil? && set_student == '1'
    add_role :teacher if !set_teacher.nil? && set_teacher == '1'
  end

  def set_class_schedule_preference
    case class_schedule_preference
    when 'both'
      self.available_in_morning = true
      self.available_in_afternoon = true
    when 'morning'
      self.available_in_morning = true
      self.available_in_afternoon = false
    when 'afternoon'
      self.available_in_morning = false
      self.available_in_afternoon = true
    end
  end
end
