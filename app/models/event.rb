class Event < ApplicationRecord
  has_many :event_sessions, dependent: :destroy
  has_many :event_attendances, dependent: :destroy
  has_many :attendees, through: :event_attendances, source: :user
  has_one :start_end_time, as: :timeable, dependent: :destroy
  belongs_to :account

  validates :account, presence: true
  validates :name, presence: true
  validates :maximum_capacity, presence: true
  validates :currency, presence: true

  monetize :price_cents,
           allow_nil: false,
           with_model_currency: :currency

  accepts_nested_attributes_for :start_end_time
end
