class LevelMembership < ApplicationRecord
  belongs_to :account
  belongs_to :user
  belongs_to :level

  validates :account, presence: true
  validates :level, presence: true
  validates :user, presence: true
  validates_uniqueness_of :user_id, scope: :level_id
end
