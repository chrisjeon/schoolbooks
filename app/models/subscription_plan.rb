class SubscriptionPlan < ApplicationRecord
  has_many :account_subscriptions, dependent: :destroy
  has_many :accounts, through: :account_subscriptions

  validates :name, presence: true

  monetize :price_cents,
           allow_nil: false,
           with_model_currency: :currency

  def self.name_collection
    all.collect { |subscription_plan| [subscription_plan.name, subscription_plan.id] }
  end

  def free?
    name.in?(['Free', 'God Mode'])
  end
end
