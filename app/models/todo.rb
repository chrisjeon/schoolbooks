class Todo < ApplicationRecord
  has_one :note, as: :noteable, dependent: :destroy
  belongs_to :account
  belongs_to :user
  belongs_to :requester, class_name: 'User', optional: true

  validates :account, presence: true
  validates :user, presence: true

  accepts_nested_attributes_for :note
end
