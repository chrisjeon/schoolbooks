class IntegrationUser < Integration
  ASSOC_KLASS = User

  DEFAULT_INTEGRATION_COLUMNS = [
    { default: nil, csv_map: 'A', is_required: true, column_name: 'email', label_name: 'Email', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'User Email' },
    { default: nil, csv_map: 'B', is_required: true, column_name: 'password', label_name: 'Password', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'User Password' },
    { default: nil, csv_map: 'C', is_required: true, column_name: 'first_name', label_name: 'First Name', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'User First Name' },
    { default: nil, csv_map: 'D', is_required: true, column_name: 'last_name', label_name: 'Last Name', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'User Last Name' },
    { default: nil, csv_map: 'E', is_required: false, column_name: 'about', label_name: 'About', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'About section of user profile' },
    { default: nil, csv_map: 'F', is_required: false, column_name: 'phone', label_name: 'Phone Number', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Phone section of user profile' },
    { default: nil, csv_map: 'G', is_required: true, column_name: 'schedulable', label_name: 'Schedulable', type: INTEGRATION_TYPE_MESSAGES[:boolean], description: 'Is User Schedulable' },
    { default: nil, csv_map: 'H', is_required: true, column_name: 'available_in_morning', label_name: 'Available in the morning', type: INTEGRATION_TYPE_MESSAGES[:boolean], description: 'User is available in the mornings' },
    { default: nil, csv_map: 'I', is_required: true, column_name: 'available_in_afternoon', label_name: 'Available in the afternoon', type: INTEGRATION_TYPE_MESSAGES[:boolean], description: 'User is available in the evenings' },
    { default: nil, csv_map: 'J', is_required: true, column_name: 'active', label_name: 'Active', type: INTEGRATION_TYPE_MESSAGES[:boolean], description: 'User is an active user (current student, teacher, etc.)' }
  ]

  def csv_row_to_hash(row)
    row_hash = row.to_hash
    row_hash[:password_confirmation]   = row[:password]
    row_hash[:unique_attribute] = :email
    row_hash
  end
end
