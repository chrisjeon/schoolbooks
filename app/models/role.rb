class Role < ApplicationRecord
  has_and_belongs_to_many :users, join_table: :users_roles

  has_one :note, as: :noteable, dependent: :destroy

  belongs_to :resource, polymorphic: true, optional: true
  belongs_to :account, optional: true

  validates :resource_type,
            inclusion: { in: Rolify.resource_types },
            allow_nil: true

  accepts_nested_attributes_for :note

  scopify

  scope :default_roles, -> { where(account: nil) }
  scope :valid_roles, -> (account) { where(account: [account, nil]) }

  def default?
    account.nil?
  end
end
