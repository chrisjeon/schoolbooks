class EventAttendance < ApplicationRecord
  has_many :payment_items, as: :itemable, dependent: :destroy
  belongs_to :event
  belongs_to :event_session, optional: true
  belongs_to :user
  belongs_to :account

  validates :event, presence: true
  validates :user, presence: true
  validates :account, presence: true

  def payable_amount
    event.price
  end
end
