class ClassSchedule < ApplicationRecord
  has_many :class_sessions, dependent: :destroy
  has_many :class_attendances, dependent: :destroy
  has_many :teachers, through: :class_sessions, source: :teacher
  has_many :students, through: :class_attendances, source: :user
  has_one :start_end_time, as: :timeable, dependent: :destroy
  belongs_to :account

  validates :account, :start_end_time, presence: true

  validate :dates_range

  accepts_nested_attributes_for :start_end_time

  before_save :set_name, if: Proc.new { |cs| cs.name.blank? }

  delegate :end_on, :start_on, to: :start_end_time

  def students_left_to_assign
    account.users.students.active.schedulable.where.not(id: self.students).count
  end

  def group_students_left_to_assign
    group_plans = account.pricing_plans.group_plans
    group_plan_enrollments = account.plan_enrollments.where(pricing_plan: group_plans)
    group_students = account.users.where(id: group_plan_enrollments.pluck(:user_id))
    group_students.where.not(id: students.pluck(:id))
  end

  def private_students_left_to_assign
    private_plans = account.pricing_plans.private_plans
    private_plan_enrollments = account.plan_enrollments.where(pricing_plan: private_plans)
    private_students = account.users.where(id: private_plan_enrollments.pluck(:user_id))
    private_students.where.not(id: students.pluck(:id))
  end

  private

  def dates_range
    return if start_end_time.blank? || start_end_time.start_date.blank? || start_end_time.end_date.blank?

    end_date = Date.strptime(start_end_time.end_date, StartEndTime::DATE_FORMAT)
    start_date = Date.strptime(start_end_time.start_date, StartEndTime::DATE_FORMAT)

    errors.add(:start_end_time, :same_week) if (start_date..end_date).count > 7
    errors.add('start_end_time.end_on', I18n.t('activerecord.errors.friday')) if end_date.wday != 5
    errors.add('start_end_time.start_on', I18n.t('activerecord.errors.monday')) if start_date.wday != 1
  end

  def set_name
    return if start_end_time.blank? || account.blank?

    self.name = "#{start_end_time.start_on.in_time_zone(account.time_zone).strftime('%-m/%-d/%Y').strip} #{start_end_time.end_on.in_time_zone(account.time_zone).strftime('%-m/%-d/%Y').strip}"
  end
end
