class PaymentItem < ApplicationRecord
  attr_accessor :due_date

  has_one :note, as: :noteable, dependent: :destroy
  belongs_to :itemable, polymorphic: true
  belongs_to :account
  belongs_to :user
  belongs_to :payment, optional: true, touch: true

  validates :itemable, presence: true
  validates :account, presence: true
  validates :user, presence: true

  before_validation :set_due_on
  before_save :set_price
  after_save :update_payment_subtotal, if: Proc.new { |item| item.payment.present? }

  accepts_nested_attributes_for :note, :itemable

  monetize :price_cents,
           allow_nil: false,
           with_model_currency: :currency

  ITEMABLE_TYPES = %w(EventAttendance HousingOccupancy PlanEnrollment MiscPaymentItem)

  scope :paid, -> { where(paid: true) }
  scope :unpaid, -> { where(paid: false) }

  private

  def set_due_on
    return if account.blank? || self.due_on.present? || self.due_date.blank?
    Time.zone = account.time_zone
    self.due_on = Time.zone.strptime(self.due_date, '%m/%d/%Y')
  end

  def set_price
    self.price = itemable.payable_amount
  end

  def update_payment_subtotal
    payment.set_subtotal!
  end
end
