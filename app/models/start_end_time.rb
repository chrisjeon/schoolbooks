class StartEndTime < ApplicationRecord
  include DateValidator

  DATE_FORMAT = '%m/%d/%Y'.freeze

  attr_accessor :start_date, :start_time, :end_date, :end_time

  belongs_to :account
  belongs_to :timeable, polymorphic: true, optional: true

  validates :account, presence: true
  validates :start_on, :end_on, presence: true

  validate :valid_start_and_end_dates?

  before_validation :set_start_on_and_end_on

  TIMEABLES_WITH_DATE_AND_TIME = %w(Event EventSession)
  TIMEABLES_WITH_DATE = %w(ClassSchedule PlanEnrollment)
  TIMEABLES_WITH_TIME = %w(PricingPlan ClassSession)

  def display_date_and_time_inputs?
    TIMEABLES_WITH_DATE_AND_TIME.include?(timeable_type)
  end

  def display_date_inputs?
    TIMEABLES_WITH_DATE.include?(timeable_type)
  end

  def display_time_inputs?
    TIMEABLES_WITH_TIME.include?(timeable_type)
  end

  private

  def set_start_on_and_end_on
    return if account.nil?
    Time.zone = account.time_zone

    if start_date.present? && start_time.present? && end_date.present? && end_time.present?
      self.start_on = Time.zone.strptime("#{self.start_date} #{self.start_time}", '%m/%d/%Y %H:%M %P')
      self.end_on = Time.zone.strptime("#{self.end_date} #{self.end_time}", '%m/%d/%Y %H:%M %P')
    elsif start_date.present? && end_date.present? && start_time.blank? && end_time.blank?
      self.start_on = Time.zone.strptime(self.start_date, DATE_FORMAT)
      self.end_on = Time.zone.strptime(self.end_date, DATE_FORMAT)
    elsif start_time.present? && end_time.present? && start_date.blank? && end_date.blank?
      self.start_on = Time.zone.strptime(self.start_time, '%H:%M %P')
      self.end_on = Time.zone.strptime(self.end_time, '%H:%M %P')
    end
  end
end
