class RegistrationInformation < ApplicationRecord
  belongs_to :account
  belongs_to :user
  belongs_to :pricing_plan

  validates :account, presence: true
  validates :user, presence: true
end
