class TermsOfService < ApplicationRecord
  belongs_to :account
  has_many :terms_of_service_acceptances, dependent: :destroy
  has_many :users, through: :terms_of_service_acceptances

  validates :account, presence: true
  validates :name, presence: true
  validates :body, presence: true

  before_save :invalidate_defaults, if: :default?

  private

  def invalidate_defaults
    account.terms_of_services.update_all(default: false)
  end
end
