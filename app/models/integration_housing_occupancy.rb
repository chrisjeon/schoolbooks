class IntegrationHousingOccupancy < Integration
  ASSOC_KLASS = HousingOccupancy

  DEFAULT_INTEGRATION_COLUMNS = [
    { default: nil, csv_map: 'A', is_required: true, column_name: 'owner_name', label_name: 'Housing owner name', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Housing owner name' },
    { default: nil, csv_map: 'B', is_required: true, column_name: 'full_name', label_name: 'User name', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'User name' },
    { default: nil, csv_map: 'C', is_required: true, column_name: 'start_date', label_name: 'Start date', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'Start date' },
    { default: nil, csv_map: 'D', is_required: true, column_name: 'end_date', label_name: 'End date', type: INTEGRATION_TYPE_MESSAGES[:two_hundred_characters], description: 'End date' },
  ]

  def csv_row_to_hash(row)
    row_hash = row.to_hash
    end_date = row_hash.delete(:end_date)
    start_date = row_hash.delete(:start_date)

    if [start_date, end_date].all?(&:present?)
      row_hash[:start_end_time_attributes] = { start_date: start_date, end_date: end_date }
    end

    row_hash
  end
end
