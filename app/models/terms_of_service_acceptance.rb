class TermsOfServiceAcceptance < ApplicationRecord
  belongs_to :account
  belongs_to :user
  belongs_to :terms_of_service

  validates :account, presence: true
  validates :user, presence: true
  validates :terms_of_service, presence: true
end
