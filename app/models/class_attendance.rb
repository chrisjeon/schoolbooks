class ClassAttendance < ApplicationRecord
  include UserAvailable

  TIMES_JOIN = <<-SQL.strip_heredoc.freeze
    INNER JOIN "class_sessions"
    ON "class_sessions"."id" = "start_end_times"."timeable_id"
    AND "start_end_times"."timeable_type" = 'ClassSession'
    INNER JOIN "class_attendances"
    ON "class_attendances"."class_session_id" = "class_sessions"."id"
  SQL

  belongs_to :account
  belongs_to :class_schedule
  belongs_to :class_session
  belongs_to :user

  validates :account, presence: true
  validates :class_schedule, presence: true
  validates :class_session, presence: true
  validates :user, presence: true

  validate :day_availability, :student_overlaps, :student_pricing_plan, unless: 'user.blank?'

  def self.hours_left(user, class_schedule_id)
    duration = StartEndTime
      .joins(TIMES_JOIN)
      .where(class_attendances: { user_id: user.id, class_schedule_id: class_schedule_id })
      .inject(0) { |memo, time| memo + (time.end_on - time.start_on) }

    left = user.pricing_plan ? (user.pricing_plan.hours_per_week || 0) - duration / 3600 : 0

    left < 0 ? 0 : left
  end

  private

  def student_overlaps
    start_end_time = class_session.start_end_time

    return if StartEndTime
                .joins(TIMES_JOIN)
                .where(class_attendances: { user_id: user_id })
                .where(
                  '(start_on, end_on) OVERLAPS (:start, :end)',
                  end: start_end_time.end_on,
                  start: start_end_time.start_on
                )
                .none?

    add_availability_error
  end

  def day_availability
    return if available_at?(user, class_session.start_end_time)

    add_availability_error
  end

  def add_availability_error
    errors.add(
      :user,
      text: "#{user.full_name} is not available for given time",
      type: :availability,
      student_id: user_id
    )
  end

  def student_pricing_plan
    current_duration = (class_session.start_end_time.end_on - class_session.start_end_time.start_on) / 3600

    return if self.class.hours_left(user, class_schedule_id) >= current_duration

    errors.add(
      :user,
      text: "#{user.full_name} has no enough hours in pricing plan",
      type: :pricing,
      student_id: user_id
    )
  end
end
