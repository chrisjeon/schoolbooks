class AccountSubscription < ApplicationRecord
  include AASM

  belongs_to :subscription_plan
  belongs_to :account

  validates :subscription_plan, presence: true
  validates :account, presence: true

  attr_accessor :stripe_card_token

  aasm do
    state :trial, initial: true
    state :ongoing
    state :paused

    event :start do
      transitions from: :trial, to: :ongoing
    end

    event :pause do
      transitions from: :ongoing, to: :paused
    end

    event :resume do
      transitions from: :paused, to: :ongoing
    end

    event :reset do
      transitions from: [:paused, :ongoing], to: :trial
    end
  end

  def create_stripe_customer_and_save_with_payment
    if valid?
      customer = Stripe::Customer.create(email: account.email, card: stripe_card_token)
      self.stripe_customer_token = customer.id
      save!
    end
  rescue Stripe::InvalidRequestError => e
    logger.error "Stripe error while creating customer: #{e.message}"
    errors.add :base, "There was a problem with your credit card."
    false
  end

  def save_with_payment
    if valid?
      customer = Stripe::Customer.retrieve(self.stripe_customer_token)
      customer.sources.create(source: stripe_card_token)
      save!
    end
  rescue Stripe::InvalidRequestError => e
    logger.error "Stripe error while saving with payment: #{e.message}"
    errors.add :base, "There was a problem with your credit card."
    false
  end
end
