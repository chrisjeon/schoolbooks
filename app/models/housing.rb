class Housing < ApplicationRecord
  has_one :note, as: :noteable, dependent: :destroy
  has_many :housing_occupancies, dependent: :destroy
  has_many :occupants, through: :housing_occupancies, source: :user
  belongs_to :account

  validates :account, presence: true
  validates :owner_name, presence: true
  validates :email, presence: true
  validates :email, uniqueness: true
  validates :phone, presence: true
  validates :maximum_occupancy, presence: true

  monetize :monthly_cost_cents,
           allow_nil: false,
           with_model_currency: :currency

  monetize :weekly_cost_cents,
           allow_nil: false,
           with_model_currency: :currency

  accepts_nested_attributes_for :note
end
