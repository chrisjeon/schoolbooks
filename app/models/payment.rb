class Payment < ApplicationRecord
  has_paper_trail

  PAYMENT_METHODS = %w(cash credit debit paypal)

  has_many :payment_items, dependent: :destroy
  belongs_to :account
  belongs_to :user

  validates :account, presence: true
  validates :user, presence: true
  validates :due_on, presence: true
  validates :payment_method, inclusion: { in: PAYMENT_METHODS }, allow_nil: true
  validates :paid_on, presence: true, if: :paid

  monetize :subtotal_cents,
           allow_nil: false,
           with_model_currency: :currency

  monetize :total_cents,
           allow_nil: false,
           with_model_currency: :currency

  before_save :set_discount, :set_total
  after_update :update_payment_items_to_paid,
               if: Proc.new { |payment| payment.changed? && payment.paid? }

  scope :overdue, -> { where('due_on < ?', Date.today) }

  def self.payment_methods_collection
    PAYMENT_METHODS.collect { |payment_method| [payment_method.capitalize, payment_method] }
  end

  def set_subtotal!
    self.subtotal = payment_items.map(&:price).sum
    self.save
  end

  def discount_to_number
    self.discount * 100
  end

  def payable_by_student?
    account.country == 'USA'
  end

  def payment_status
    paid? ? I18n.t('paid') : I18n.t('pending')
  end

  private

  def set_discount
    self.discount = 0.0 and return if self.discount.nil?
    self.discount /= 100 and return if self.discount > 1
  end

  def set_total
    self.total = self.subtotal - (self.subtotal * self.discount)
  end

  def update_payment_items_to_paid
    payment_items.update_all(paid: true, paid_on: paid_on)
  end
end
