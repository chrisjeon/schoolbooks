class Level < ApplicationRecord
  has_many :level_memberships, dependent: :destroy
  has_many :level_members, through: :level_memberships, source: :user
  belongs_to :account

  validates :account, presence: true
  validates :name, presence: true, uniqueness: { scope: :account_id }
end
