class PricingPlan < ApplicationRecord
  include Codeable

  has_many :plan_enrollments, dependent: :destroy
  has_many :users, through: :plan_enrollments
  has_many :start_end_times, as: :timeable, dependent: :destroy
  has_many :registration_informations, dependent: :destroy
  has_one :note, as: :noteable, dependent: :destroy
  belongs_to :account

  FREQUENCY_PLANS = %w(hour day week)

  validates :account, presence: true
  validates :name, presence: true
  validates :frequency,
            presence: true,
            numericality: { only_integer: true, greater_than: 0 }
  validates :frequency_unit,
            presence: true,
            inclusion: { in: FREQUENCY_PLANS }
  validates :code, uniqueness: { scope: :account_id }

  monetize :price_cents,
           allow_nil: false,
           with_model_currency: :currency

  after_save :set_code_if_blank, if: Proc.new { |pp| pp.code.blank? }

  accepts_nested_attributes_for :note

  scope :private_plans, -> { where(private_plan: true) }
  scope :group_plans, -> { where(private_plan: false) }


  def self.currency_collection
    Money::Currency.table.collect do |currency|
      ["#{currency.last[:name]} (#{currency.last[:iso_code]})",
       currency.last[:iso_code]]
    end
  end

  def self.frequency_plan_collection
    FREQUENCY_PLANS.collect { |plan| [plan.capitalize, plan] }
  end

  def active_users
    account.users.where(id: plan_enrollments.active.pluck(:user_id))
  end

  def inactive_users
    account.users.where(id: plan_enrollments.inactive.pluck(:user_id))
  end
end
