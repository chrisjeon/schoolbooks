class HomeController < ApplicationController
  def index
    if user_signed_in?
      render :index
    else
      render 'home/landing/index'
    end
  end
end
