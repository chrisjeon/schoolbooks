class LevelsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_level, only: [:edit, :update, :destroy]

  def index
    @levels = current_account.levels
  end

  def new
    @level = Level.new(account: current_user.account)
    authorize @level
  end

  def show
    @level = current_account.levels.find(params[:id])
    @level_members = @level.level_members
    @teachers_in_level = @level_members.teachers
    @students_in_level = @level_members.students
    @teachers_to_be_added = current_account.
                            users.
                            teachers.
                            where.
                            not(id: @level_members.pluck(:user_id) << current_user.id).
                            distinct.
                            order(:full_name)
    @students_to_be_added = current_account.
                            users.
                            students.
                            where.
                            not(id: @level_members.pluck(:user_id) << current_user.id).
                            distinct.
                            order(:full_name)
    authorize @level
  end

  def create
    @level = Level.new(level_params.merge(account: current_account))
    authorize @level

    if @level.save
      flash.now[:success] = I18n.t('level_successfully_created')
      redirect_to @level
    else
      flash.now[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def edit
    authorize @level
  end

  def update
    if @level.update_attributes(level_params)
      flash[:success] = I18n.t('level_successfully_updated')
      redirect_to @level
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @level
    @level.destroy
    flash[:success] = I18n.t('pricing_plan_successfully_deleted')
    redirect_to pricing_plans_path
  end

  private

  def level_params
    params.require(:level).permit(:name, :active)
  end

  def find_level
    @level = current_account.levels.find(params[:id])
  end
end
