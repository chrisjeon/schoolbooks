class EventSessionsController < ApplicationController
  before_action :authenticate_user!, :find_event
  before_action :find_event_session, only: [:edit, :show, :update, :destroy]

  def new
    @event_session = @event.event_sessions.new(account: current_account)
    authorize @event_session
    @event_session.build_start_end_time
  end

  def edit
    authorize @event_session
  end

  def show
    authorize @event_session
  end

  def create
    @event_session = @event.event_sessions.new(event_session_params.merge(account: current_account))
    authorize @event_session
    @event_session.start_end_time.timeable = @event_session
    @event_session.start_end_time.account = current_account

    if @event_session.save
      flash[:success] = I18n.t('event_session_successfully_created')
      redirect_to event_event_session_path(@event, @event_session)
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    @event_session.assign_attributes(event_session_params)
    authorize @event_session
    @event_session.start_end_time.timeable = @event_session
    @event_session.start_end_time.account = current_account

    if @event_session.save
      flash[:success] = I18n.t('event_session_successfully_updated')
      redirect_to event_event_session_path(@event, @event_session)
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @event_session
    @event_session.destroy
    flash[:success] = I18n.t('event_session_successfully_deleted')
    redirect_to @event
  end

  private

  def find_event
    @event = current_account.events.find(params[:event_id])
  end

  def find_event_session
    @event_session = @event.event_sessions.find(params[:id])
  end

  def event_session_params
    params.require(:event_session).permit(:maximum_capacity,
      start_end_time_attributes: [:id, :start_date, :start_time, :end_date, :end_time, :timeable_id, :timeable_type, :account_id])
  end
end
