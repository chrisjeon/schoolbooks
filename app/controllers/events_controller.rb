class EventsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_event, only: [:show, :edit, :update, :destroy]

  def index
    @events = policy_scope(Event)
  end

  def new
    @event = current_account.events.new
    authorize @event
    @event.build_start_end_time
  end

  def show
    authorize @event
  end

  def edit
    authorize @event
  end

  def create
    @event = current_account.events.new(event_params)
    authorize @event
    @event.start_end_time.timeable = @event
    @event.start_end_time.account = current_account
    @event.price_currency = current_account.currency
    @event.currency = current_account.currency
    @event.price = if event_params[:price_cents].try { |pc| pc.include?('.') }
                     event_params[:price_cents].try(:to_f)
                   else
                     event_params[:price_cents].try(:to_i)
                   end


    if @event.save
      flash[:success] = I18n.t('event_successfully_created')
      redirect_to @event
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    @event.assign_attributes(event_params)
    authorize @event
    @event.start_end_time.timeable = @event
    @event.start_end_time.account = current_account
    @event.price_currency = current_account.currency
    @event.currency = @event.currency
    @event.price = if event_params[:price_cents].try { |pc| pc.include?('.') }
                     event_params[:price_cents].try(:to_f)
                   else
                     event_params[:price_cents].try(:to_i)
                   end
    if @event.save
      flash[:success] = I18n.t('event_successfully_updated')
      redirect_to @event
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    @event.destroy
    flash[:success] = I18n.t('event_successfully_deleted')
    redirect_to events_url
  end

  private

  def find_event
    @event = current_account.events.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:name, :maximum_capacity, :has_sessions,
      :price_cents, :description,
      start_end_time_attributes: [:id, :start_date, :start_time, :end_date, :end_time, :timeable_id, :timeable_type, :account_id])
  end
end
