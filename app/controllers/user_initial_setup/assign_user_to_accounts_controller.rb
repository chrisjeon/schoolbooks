class UserInitialSetup::AssignUserToAccountsController < ApplicationController
  before_action :authenticate_user!

  def create
    @account = Account.find_by(code: params[:code])
    current_user.update_attributes(account: @account)
    current_user.add_role :student
    redirect_to root_path
  end
end
