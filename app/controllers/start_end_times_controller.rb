class StartEndTimesController < ApplicationController
  before_action :authenticate_user!, :find_timeable
  before_action :find_start_end_time, only: [:edit, :destroy, :update]

  def new
    @start_end_time = StartEndTime.new(account: current_account, timeable: @timeable)
    authorize @start_end_time
  end

  def edit
    authorize @start_end_time
  end

  def create
    @start_end_time = current_account.start_end_times.new(timeable: @timeable)
    authorize @start_end_time
    @start_end_time.assign_attributes(start_end_time_params)

    if @start_end_time.save
      flash[:success] = I18n.t('session_successfully_created')
      redirect_to @timeable
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    authorize @start_end_time

    if @start_end_time.update_attributes(start_end_time_params)
      flash[:success] = I18n.t('session_successfully_updated')
      redirect_to @timeable
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @start_end_time
    @start_end_time.destroy
    flash[:success] = I18n.t('session_successfully_deleted')
    redirect_to @timeable
  end

  private

  def find_timeable
    @timeable = params[:timeable_type].constantize.find(params[:timeable_id])
  end

  def find_start_end_time
    @start_end_time = current_account.start_end_times.find_by(timeable: @timeable, id: params[:id])
  end

  def start_end_time_params
    params.require(:start_end_time).permit(:start_date, :start_time, :end_date, :end_time)
  end
end
