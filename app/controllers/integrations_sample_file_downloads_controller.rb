class IntegrationsSampleFileDownloadsController < ApplicationController
  before_action :authenticate_user!

  def download_user_csv
    file = File.join(Rails.root, 'test', 'fixtures', 'files', 'user_integration_sample.csv')
    send_file(file)
  end

  def download_housing_csv
    file = File.join(Rails.root, 'test', 'fixtures', 'files', 'housing_integration_sample.csv')
    send_file(file)
  end

  def download_housing_occupancy_csv
    file = File.join(Rails.root, 'test', 'fixtures', 'files', 'housing_occupancy_integration_sample.csv')
    send_file(file)
  end

  def download_plan_enrollment_csv
    file = File.join(Rails.root, 'test', 'fixtures', 'files', 'plan_enrollment_integration_sample.csv')
    send_file(file)
  end
end
