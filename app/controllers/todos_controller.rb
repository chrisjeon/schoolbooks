class TodosController < ApplicationController
  before_action :authenticate_user!
  before_action :find_user
  before_action :find_todo, only: [:show, :edit, :update, :destroy]

  def new
    @todo = @user.todos.new(account: current_account)
    @todo.build_note
    authorize @todo
  end

  def show
    authorize @todo
  end

  def edit
    authorize @todo
  end

  def index
    @todos = policy_scope(Todo).includes(:user)

    respond_to do |format|
      format.html
      format.json { render json: @todos.to_json }
    end
  end

  def create
    @todo = @user.todos.new(todo_params.merge(account: current_account))
    @todo.build_note if @todo.note.nil?
    authorize @todo
    @todo.note.noteable = @todo

    if @todo.save
      flash[:success] = I18n.t('todo_successfully_created')
      redirect_to user_todo_path(@user, @todo)
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    authorize @todo

    if @todo.update_attributes(todo_params)
      flash[:success] = I18n.t('todo_successfully_updated')
      redirect_to user_todo_path(@user, @todo)
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @todo
    @todo.destroy
    flash[:success] = I18n.t('todo_successfully_deleted')
    redirect_to user_todos_url(@user)
  end

  private

  def find_user
    @user = current_account.users.find(params[:user_id])
  end

  def find_todo
    @todo = @user.todos.find(params[:id])
  end

  def todo_params
    params.require(:todo).permit(:account_id, :user_id, :requester_id, :body,
      :complete, :completed_on,
      note_attributes: [:id, :noteable, :noteable_id, :noteable_type, :body]
      )
  end
end
