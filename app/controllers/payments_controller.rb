class PaymentsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_payment, only: [:show, :edit, :update]

  def index
    @payments = policy_scope(Payment)
    @payments = @payments.includes(:user)
  end

  def show
    authorize @payment
    @payment_items = @payment.payment_items.includes(:itemable)
  end

  def edit
    authorize @payment
  end

  def update
    authorize @payment

    if @payment.update_attributes(payment_params)
      flash[:success] = I18n.t('payment_successfully_updated')
      redirect_to @payment
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :show
    end
  end

  private

  def find_payment
    @payment = current_account.payments.find(params[:id])
  end

  def payment_params
    params.require(:payment).permit(:paid, :due_on, :discount,
      :payment_method, :paid_on)
  end
end

