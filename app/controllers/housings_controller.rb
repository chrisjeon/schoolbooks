class HousingsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_housing, only: [:show, :edit, :update, :destroy]

  def index
    @housings = policy_scope(Housing)
  end

  def new
    @housing = current_account.housings.new
    @housing.build_note
    authorize @housing
  end

  def show
    authorize @housing
  end

  def edit
    authorize @housing
  end

  def create
    @housing = Housing.new(housing_params.merge(account: current_account))
    @housing.build_note if @housing.note.nil?
    authorize @housing
    @housing.monthly_cost_currency = current_account.currency
    @housing.weekly_cost_currency = current_account.currency
    @housing.currency = current_account.currency
    @housing.monthly_cost = if housing_params[:monthly_cost_cents].try { |c| c.include?('.') }
                              housing_params[:monthly_cost_cents].try(:to_f)
                            else
                              housing_params[:monthly_cost_cents].try(:to_i)
                            end
    @housing.weekly_cost  = if housing_params[:weekly_cost_cents].try { |c| c.include?('.') }
                              housing_params[:weekly_cost_cents].try(:to_f)
                            else
                              housing_params[:weekly_cost_cents].try(:to_i)
                            end
    @housing.note.noteable = @housing

    if @housing.save
      flash[:success] = I18n.t('housing_successfully_created')
      redirect_to @housing
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    authorize @housing
    @housing.assign_attributes(housing_params)
    @housing.monthly_cost_currency = current_account.currency
    @housing.weekly_cost_currency = current_account.currency
    @housing.currency = current_account.currency
    @housing.monthly_cost = if housing_params[:monthly_cost_cents].try { |c| c.include?('.') }
                              housing_params[:monthly_cost_cents].try(:to_f)
                            else
                              housing_params[:monthly_cost_cents].try(:to_i)
                            end
    @housing.weekly_cost  = if housing_params[:weekly_cost_cents].try { |c| c.include?('.') }
                              housing_params[:weekly_cost_cents].try(:to_f)
                            else
                              housing_params[:weekly_cost_cents].try(:to_i)
                            end

    if @housing.save
      flash[:success] = I18n.t('housing_successfully_updated')
      redirect_to @housing
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @housing
    @housing.destroy
    redirect_to housings_url
  end

  private

  def housing_params
    params.require(:housing).permit(:owner_name, :email, :phone, :description,
      :additional_services_and_costs, :maximum_occupancy, :confirmed_host,
      :url, :currency, :monthly_cost_cents, :weekly_cost_cents, :address,
      note_attributes: [:id, :noteable, :noteable_id, :noteable_type, :body])
  end

  def find_housing
    @housing = current_account.housings.find(params[:id])
  end
end
