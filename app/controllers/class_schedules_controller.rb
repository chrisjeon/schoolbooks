class ClassSchedulesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_class_schedule, except: [:index, :create, :new]

  def index
    @class_schedules = policy_scope(ClassSchedule)
  end

  def new
    @class_schedule = current_account.class_schedules.new
    authorize @class_schedule
    @class_schedule.build_start_end_time
  end

  def show
    authorize @class_schedule
    # TODO: investigate the need of this
    # @teachers = current_account.users.with_role(:teacher).schedulable
    # @students = current_account.users.with_role(:student).schedulable
  end

  def edit
    authorize @class_schedule
    @class_schedule.build_start_end_time if @class_schedule.start_end_time.nil?
  end

  def create
    @class_schedule = current_account.class_schedules.new(class_schedule_params)
    authorize @class_schedule
    @class_schedule.start_end_time.timeable = @class_schedule
    @class_schedule.start_end_time.account = current_account

    save_and_render(:new)
  end

  def update
    @class_schedule.assign_attributes(class_schedule_params)
    authorize @class_schedule

    @class_schedule.start_end_time.timeable = @class_schedule
    @class_schedule.start_end_time.account = current_account

    save_and_render(:edit)
  end

  def destroy
    authorize @class_schedule
    @class_schedule.destroy
    flash[:success] = I18n.t('class_schedule_successfully_deleted')
    redirect_to class_schedules_path
  end

  private

  def save_and_render(action_to_fail)
    if @class_schedule.save
      flash[:success] = I18n.t('class_schedule_successfully_created')

      redirect_to @class_schedule
    else
      flash[:error] = @class_schedule.errors.full_messages.join('<br>').html_safe

      render action_to_fail
    end
  end

  def find_class_schedule
    @class_schedule = current_account.class_schedules.find(params[:id])
  end

  def class_schedule_params
    params.require(:class_schedule).permit(:maximum_students_per_class,
      :name,
      start_end_time_attributes: [:id, :start_date, :end_date, :timeable_type, :timeable_id, :account_id])
  end
end
