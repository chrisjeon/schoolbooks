class AccountSubscriptionsController < ApplicationController
  before_action :authenticate_user!, :find_account_subscription

  def show
    @account_subscription = AccountSubscription.find(params[:id])
    if @account_subscription.stripe_customer_token.present? && !Rails.env.test?
      @stripe_customer = Stripe::Customer.retrieve(@account_subscription.stripe_customer_token)
      @credit_cards = @stripe_customer.sources.all(object: 'card')
    end
    authorize @account_subscription
  end

  def update
    @account_subscription = AccountSubscription.find(params[:id])
    authorize @account_subscription

    original_subscription_plan = @account_subscription.subscription_plan

    if @account_subscription.update_attributes(account_subscription_params)
      if original_subscription_plan != @account_subscription.subscription_plan && @account_subscription.stripe_customer_token.present?
        unless @account_subscription.subscription_plan.free?
          stripe_service = StripeService.new(@account_subscription)
          stripe_service.update_subscription!
        end
      end
      flash[:success] = I18n.t('account_subscription_successfully_updated')
    else
      flash[:error] = I18n.t('something_went_wrong')
    end
    redirect_to @account_subscription
  end

  private

  def find_account_subscription
    @account_subscription = AccountSubscription.find(params[:id])
  end

  def account_subscription_params
    params.require(:account_subscription).permit(:subscription_plan_id)
  end
end
