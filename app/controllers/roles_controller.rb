class RolesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_role, only: [:edit, :show, :update, :destroy]

  def index
    @roles = Role.valid_roles(current_account)
  end

  def new
    @role = current_account.roles.new
    authorize(@role)
    @role.build_note
  end

  def edit
    authorize @role
  end

  def show
    authorize @role
  end

  def create
    @role = Role.new(role_params.merge(account: current_account))
    @role.build_note
    @role.note.noteable = @role

    if @role.save
      flash[:success] = I18n.t('role_successfully_created')
      redirect_to @role
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    authorize @role

    if @role.update_attributes(role_params)
      flash[:success] = I18n.t('role_successfully_updated')
      redirect_to @role
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @role
    @role.destroy
    flash[:success] = I18n.t('role_successfully_deleted')
    redirect_to roles_path
  end

  private

  def find_role
    @role = Role.find(params[:id])
  end

  def role_params
    params.require(:role).permit(:name, note_attributes: [:id, :noteable, :noteable_id, :noteable_type, :body])
  end
end
