class StripeSubscriptionsController < ApplicationController
  before_action :authenticate_user!, :load_account_subscription
  before_action :find_stripe_customer, only: [:delete_card, :make_default_card]

  def subscribe
    redirect_to :back, error: I18n.t('you_are_already_subscribed_to_a_plan') and return unless @account_subscription.trial?
    stripe_service = StripeService.new(@account_subscription)
    stripe_service.create_subscription!(params[:stripe_card_token])
    redirect_to account_subscription_path(@account_subscription)
  end

  def resume_subscription
    stripe_service = StripeService.new(@account_subscription)
    stripe_service.resume_subscription!
    redirect_to account_subscription_path(@account_subscription)
  end

  def pause_subscription
    stripe_service = StripeService.new(@account_subscription)
    stripe_service.pause_subscription!
    redirect_to account_subscription_path(@account_subscription)
  end

  def add_a_new_card
    @account_subscription.assign_attributes(stripe_card_token: params[:stripe_card_token])

    if @account_subscription.save_with_payment
      redirect_to account_subscription_path(@account_subscription), success: 'Payment successfully saved.'
    else
      redirect_to account_subscription_path(@account_subscription), alert: 'Something has gone wrong.'
    end
  end

  def delete_card
    @stripe_customer.sources.retrieve(params[:card_id]).delete
    redirect_to account_subscription_path(@account_subscription), success: 'Card deleted.'
  end

  def make_default_card
    @stripe_customer.default_source = params[:card_id]
    @stripe_customer.save
    redirect_to account_subscription_path(@account_subscription), success: 'Default card saved.'
  end

  private

  def load_account_subscription
    @account_subscription = current_account.account_subscription
  end

  def find_stripe_customer
    @stripe_customer = Stripe::Customer.retrieve(
                       current_account.account_subscription.stripe_customer_token)
  end
end
