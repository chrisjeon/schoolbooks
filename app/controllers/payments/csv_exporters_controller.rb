class Payments::CsvExportersController < ApplicationController
  before_action :authenticate_user!

  def index
    authorize current_account, :update?

    start_date = params[:start_date].to_date
    end_date = params[:end_date].to_date

    respond_to do |format|
      format.csv do
        send_data PaymentsReportExportService.new(current_account, start_date, end_date).process!, filename: "payments-#{Date.today}.csv"
      end
    end
  end
end
