class Payments::PaymentItemsController < ApplicationController
  before_action :authenticate_user!

  def destroy
    @payment_item = current_account.payment_items.find(params[:id])
    authorize @payment_item, :update?
    @payment      = @payment_item.payment
    @payment_item.update_attributes(payment: nil)
    @payment.set_subtotal!
    redirect_to payment_path(@payment)
  end
end
