class UsersRolesController < ApplicationController
  before_action :authenticate_user!

  def create
    @user = current_account.users.find(params[:user_id])
    @role = Role.find(params[:role_id])
    @users_role = UsersRole.new(role: @role, user: @user)
    authorize @users_role
    @users_role.save
    flash[:success] = I18n.t('user_added_to_role')
    redirect_to @role
  end

  def destroy
    @users_role = UsersRole.find(params[:id])
    authorize @users_role
    @users_role.destroy
    flash[:error] = I18n.t('user_removed_from_role')
    redirect_to @users_role.role
  end
end
