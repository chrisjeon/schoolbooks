class EventAttendancesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_event, if: -> { params[:event_id].present? }
  before_action :find_event_session, if: -> { params[:event_session_id].present? }
  before_action :find_event_attendance, only: :destroy

  def create
    @event_attendance = EventAttendance.new(event_attendance_params.merge(event: @event, event_session: @event_session, account: current_account))

    if @event_attendance.save
      flash[:success] = I18n.t('attendee_successfully_added')
    else
      flash[:error] = I18n.t('something_went_wrong')
    end

    redirect_to redirect_url
  end

  def destroy
    authorize @event_attendance
    @event_attendance.destroy
    flash[:success] = I18n.t('attendee_successfully_removed')
    redirect_to redirect_url
  end

  private

  def find_event
    @event = current_account.events.find(params[:event_id])
  end

  def find_event_session
    @event_session = current_account.event_sessions.find(params[:event_session_id])
    @event = @event_session.event if @event.nil?
  end

  def find_event_attendance
    @event_attendance = @event.event_attendances.find_by(id: params[:id], event_session: nil) if @event.present?
    @event_attendance = @event_session.event_attendances.find(params[:id]) if @event_session.present?
  end

  def event_attendance_params
    params.require(:event_attendance).permit(:user_id)
  end

  def redirect_url
    @event_attendance.event_session.present? ? event_event_session_path(@event, @event_session) : event_path(@event)
  end
end
