class PlanEnrollmentsController < ApplicationController
  before_action :authenticate_user!, :find_pricing_plan
  before_action :find_plan_enrollment, except: [:new, :create]

  def new
    @user = current_account.users.find(params[:user_id])
    @plan_enrollment = PlanEnrollment.new(account: current_account, user: @user)
    @plan_enrollment.build_start_end_time
    authorize @plan_enrollment
  end

  def create
    @plan_enrollment = PlanEnrollment.new(plan_enrollment_params.merge(account: current_account, pricing_plan: @pricing_plan))
    authorize @plan_enrollment
    start_date = @plan_enrollment.start_end_time.start_date
    end_date = @plan_enrollment.start_end_time.end_date
    @plan_enrollment.start_end_time.start_date = start_date.to_date.strftime('%m/%d/%Y')
    @plan_enrollment.start_end_time.end_date = end_date.to_date.strftime('%m/%d/%Y')
    @plan_enrollment.start_end_time.timeable = @plan_enrollment
    @plan_enrollment.start_end_time.account = current_account
    @plan_enrollment.price_currency = current_account.currency
    @plan_enrollment.currency = current_account.currency
    @plan_enrollment.price = if plan_enrollment_params[:price_cents].try { |pc| pc.include?('.') }
                               plan_enrollment_params[:price_cents].try(:to_f)
                             else
                               plan_enrollment_params[:price_cents].try(:to_i)
                             end

    if @plan_enrollment.save
      @plan_enrollment.user.update_attributes(active: true)
      flash[:success] = I18n.t('user_successfully_added_to_plan')
    else
      flash[:error] = I18n.t('something_went_wrong')
    end

    redirect_to params[:redirect_url] || @pricing_plan
  end

  def update
    authorize @plan_enrollment

    if @plan_enrollment.update_attributes(plan_enrollment_params)
      flash[:success] = I18n.t('plan_enrollment_updated_successfully')
    else
      flash[:error] = I18n.t('something_went_wrong')
    end
    redirect_to @pricing_plan
  end

  def destroy
    authorize @plan_enrollment
    @plan_enrollment.destroy
    flash[:success] = I18n.t('user_successfully_removed_from_plan')
    redirect_to @pricing_plan
  end

  private

  def plan_enrollment_params
    params.require(:plan_enrollment).permit(:user_id, :account_id, :pricing_plan_id,
      :active, :price_cents,
      start_end_time_attributes: [:start_date, :end_date, :timeable_id, :timeable_type])

  end

  def find_pricing_plan
    @pricing_plan = current_account.pricing_plans.find(params[:pricing_plan_id])
  end

  def find_plan_enrollment
    @plan_enrollment = @pricing_plan.plan_enrollments.find(params[:id])
  end
end
