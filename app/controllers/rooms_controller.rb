class RoomsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_room, only: [:show, :edit, :update, :destroy]

  def index
    @rooms = policy_scope(Room)
  end

  def new
    @room = Room.new(account: current_account)
    authorize @room
  end

  def show
    authorize @room
  end

  def edit
    authorize @room
  end

  def create
    @room = current_account.rooms.new(room_params)

    if @room.save
      flash[:success] = I18n.t('room_successfully_created')
      redirect_to @room
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    if @room.update_attributes(room_params)
      flash[:success] = I18n.t('room_successfully_updated')
      redirect_to @room
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @room
    @room.destroy
    flash[:success] = I18n.t('room_successfully_deleted')
    redirect_to rooms_path
  end

  private

  def find_room
    @room = current_account.rooms.find(params[:id])
  end

  def room_params
    params.require(:room).permit(:account_id, :name)
  end
end
