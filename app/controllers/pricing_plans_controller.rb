class PricingPlansController < ApplicationController
  before_action :authenticate_user!
  before_action :find_pricing_plan, only: [:edit, :show, :update, :destroy]

  def index
    @pricing_plans = policy_scope(PricingPlan)
  end

  def new
    @pricing_plan = PricingPlan.new(account: current_account)
    @pricing_plan.build_note
    authorize @pricing_plan
  end

  def edit
    authorize @pricing_plan
  end

  def show
    authorize @pricing_plan
  end

  def create
    @pricing_plan = PricingPlan.new(pricing_plan_params.merge(account: current_account))
    @pricing_plan.build_note if @pricing_plan.note.nil?
    authorize @pricing_plan
    @pricing_plan.price_currency = current_account.currency
    @pricing_plan.currency = current_account.currency
    @pricing_plan.price = if pricing_plan_params[:price_cents].try { |pc| pc.include?('.') }
                            pricing_plan_params[:price_cents].try(:to_f)
                          else
                            pricing_plan_params[:price_cents].try(:to_i)
                          end
    @pricing_plan.note.noteable = @pricing_plan

    if @pricing_plan.save
      flash[:success] = I18n.t('pricing_plan_successfully_created')
      redirect_to @pricing_plan
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    authorize @pricing_plan
    @pricing_plan.assign_attributes(pricing_plan_params)
    @pricing_plan.price_currency = current_account.currency
    @pricing_plan.currency = current_account.currency
    @pricing_plan.price = if pricing_plan_params[:price_cents].try { |pc| pc.include?('.') }
                            pricing_plan_params[:price_cents].try(:to_f)
                          else
                            pricing_plan_params[:price_cents].try(:to_i)
                          end

    if @pricing_plan.save
      flash[:success] = I18n.t('pricing_plan_successfully_updated')
      redirect_to @pricing_plan
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @pricing_plan
    @pricing_plan.destroy
    flash[:success] = I18n.t('pricing_plan_successfully_deleted')
    redirect_to pricing_plans_path
  end

  private

  def pricing_plan_params
    params.require(:pricing_plan).permit(:name, :price_cents, :frequency,
      :frequency_unit, :private_plan, :hours_per_week, :hours_per_day,
      note_attributes: [:id, :noteable, :noteable_id, :noteable_type, :body])
  end

  def find_pricing_plan
    @pricing_plan = current_account.pricing_plans.find(params[:id])
  end
end
