class LevelMembershipsController < ApplicationController
  before_action :authenticate_user!, :find_level

  def create
    @level_membership = LevelMembership.new(level_membership_params.merge(account: current_account, level: @level))
    authorize @level_membership

    if @level_membership.save
      flash[:success] = I18n.t('user_successfully_added_to_level')
    else
      flash[:error] = I18n.t('something_went_wrong')
    end
    redirect_to @level
  end

  def destroy
    @level_membership = @level.level_memberships.find(params[:id])
    authorize @level_membership
    @level_membership.destroy
    flash[:success] = I18n.t('user_successfully_removed_from_level')
    redirect_to @level
  end

  private

  def level_membership_params
    params.require(:level_membership).permit(:user_id)
  end

  def find_level
    @level = current_account.levels.find(params[:level_id])
  end
end
