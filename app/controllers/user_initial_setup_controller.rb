class UserInitialSetupController < ApplicationController
  layout 'user_initial_setup'

  before_action :authenticate_user!

  def index
    @user = current_user
  end
end
