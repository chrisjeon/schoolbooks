class ReportTeachersController < ApplicationController
  before_action :authenticate_user!

  def index
    redirect_to root_path unless current_user.admin?
    @teachers = current_account.users.teachers.active
    params[:start_on] ||= (Date.today - 30.days).strftime('%m/%d/%Y')
    params[:end_on] ||= (Date.today).strftime('%m/%d/%Y')
    @start_on = Date.strptime(params[:start_on].strip, '%m/%d/%Y')
    @end_on   = Date.strptime(params[:end_on].strip, '%m/%d/%Y')
  end
end
