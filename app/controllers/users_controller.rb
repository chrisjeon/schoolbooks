class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :find_user, only: [:show, :update, :destroy]

  def new
    authorize current_user, :create?
    @user = User.new
  end

  def index
    @search_term = params[:search_term]
    @users = current_account.
             users.
             filter(params.slice(:search_term)).
             page(params[:page])
  end

  def show
  end

  def create
    authorize current_user
    @user = User.new(user_params.merge(account: current_account))

    if @user.save
      flash.now[:success] = "#{I18n.t('user_successfully_created')} #{I18n.t('confirmation_email_has_been_sent_to_the_user')}"

      UserMailer.user_created_email(@user, params[:user][:password]).deliver_later

      @search_term = params[:search_term]
      @users = current_account.
               users.
               filter(params.slice(:search_term)).
               page(params[:page])
      render :index, change: :users
    else
      flash.now[:error] = I18n.t('something_went_wrong')
      render :new, change: :user
    end
  end

  def update
    authorize @user

    if user_params[:password].present?
      @user.update_attributes(user_params)
    else
      @user.update_without_password(user_params)
    end

    if @user.errors.none?
      adjust_user_roles(@user)
      flash.now[:success] = I18n.t('user_successfully_updated')
      render :show, change: :user
    else
      flash.now[:error] = I18n.t('something_went_wrong')
      render :show
    end
  end

  def destroy
    authorize @user
    @user.destroy
    @users = current_account.
             users.
             filter(params.slice(:search_term)).
             page(params[:page])
    render :index, change: :users
  end

  private

  def find_user
    @user = current_account.users.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :password,
      :password_confirmation, :about, :phone, :avatar, :set_teacher,
      :available_in_morning, :available_in_afternoon, :set_student, :active,
      :set_admin, :schedulable, :country, :gender, :passport_number, :birth_date,
      :emergency_contact_name, :emergency_contact_number)
  end

  def adjust_user_roles(user)
    valid_role_params.each do |role_id|
      role = Role.find(role_id)
      user.roles << role unless user.roles.include?(role)
    end
    invalid_role_params.each do |role_id|
      role = Role.find(role_id)
      user.roles.destroy(role)
    end
  end

  def valid_role_params
    params[:role].reject { |_key, value| value == '0' }
  end

  def invalid_role_params
    params[:role].reject { |_key, value| value == '1' }
  end
end
