class RegistrationForms::StudentRegistrationsController < ApplicationController
  layout 'student_registration'

  def new
    @account = Account.find(params[:account_id])
  end

  def create
    @account = Account.find(params[:account_id])

    @user = @account.users.new(user_params)
    @user.password = 'password'
    @user.password_confirmation = 'password'
    @user.active = false

    if @user.save
      @user.add_role :student
      registration_information = @user.build_registration_information
      registration_information.assign_attributes(registration_information_params)
      registration_information.account = @account
      registration_information.save
      terms_of_service = @account.terms_of_services.where(default: true).first
      TermsOfServiceAcceptance.create(user: @user,
                                      terms_of_service: terms_of_service,
                                      account: @account)
      flash[:success] = I18n.t('you_have_successfully_registered')
      redirect_to root_path
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email,
      :class_schedule_preference, :country, :phone, :gender, :passport_number,
      :birth_date, :emergency_contact_name, :emergency_contact_number)
  end

  def registration_information_params
    params.require(:registration_information).permit(:starting_date,
      :sign_up_period, :pricing_plan_id, :current_level_description,
      :how_did_you_hear_about_us, :why_did_you_choose_us, :anything_else,
      :housing_information, :coaching_sessions_per_week)
  end
end
