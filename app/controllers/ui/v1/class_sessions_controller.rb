module Ui
  module V1
    class ClassSessionsController < Ui::ApplicationController
      def create
        schedule_dates = ClassSchedule.find(params[:class_schedule_id]).start_end_time
        dates = (schedule_dates.start_on.to_date..schedule_dates.end_on.to_date)
        class_sessions = dates.each_with_object([]) do |date, memo|
          memo << ClassSession.create(session_params(date))
        end

        deal_with_result(class_sessions)
      end

      def update
        class_sessions = policy_scope(ClassSession)
          .find(params[:id])
          .with_siblings
          .each_with_object([]) do |class_session, memo|
            class_session.update(
              with_normalized_attendances(
                class_session.id,
                session_params(
                  class_session.start_end_time.start_on
                )
              )
            )

            memo << class_session
          end

        deal_with_result(class_sessions)
      end

      def show
        render(
          json: policy_scope(ClassSession).find(params[:id]),
          serializer: RoomSerializer::ClassSessionSerializer,
          key_transform: :camel_lower
        )
      end

      def destroy
        class_sessions = policy_scope(ClassSession).find(params[:id]).with_siblings.map(&:destroy)

        render(
          if class_sessions.all? { |cs| cs.destroyed? }
            { json: :ok }
          else
            { json: { errors: true } }
          end
        )
      end

      private

      def permited_params
        params.permit(
          :room_id, :class_schedule_id, :user_id, :account_id,
          class_attendances_attributes: [:user_id, :account_id, :class_schedule_id]
        )
      end

      def time_with_date(time, date)
        Time.zone.parse(time).change(day: date.day, year: date.year, month: date.month)
      end

      def session_params(date)
        class_session_params = permited_params.dup

        class_session_params[:start_end_time_attributes] = {
          end_on: time_with_date(params[:end_on], date),
          start_on: time_with_date(params[:start_on], date),
          account_id: params[:account_id]
        }

        class_session_params
      end

      def with_normalized_attendances(class_session_id, class_session_params)
        old_attendances = ClassAttendance.where(class_session_id: class_session_id)

        old_user_ids = old_attendances.pluck(:user_id)
        old_attributes = class_session_params[:class_attendances_attributes]

        new_attributes = old_attributes.select do |_k, v|
          !old_user_ids.include?(v[:user_id].to_i)
        end

        new_user_ids = old_attributes.to_h.map { |_key, attrs| attrs[:user_id].to_i }

        old_attendances.where.not(user_id: new_user_ids).ids.each do |id|
          new_attributes[id.to_s] = ActionController::Parameters.new({ id: id, _destroy: true }).permit!
        end

        class_session_params[:class_attendances_attributes] = new_attributes

        class_session_params
      end

      def deal_with_result(class_sessions)
        class_sessions.each { |cs| cs.check_students_presence if cs.persisted? }

        render(
          if class_sessions.none? { |cs| cs.errors.any? }
            {
              json: class_sessions.first,
              serializer: RoomSerializer::ClassSessionSerializer,
              key_transform: :camel_lower
            }
          else
            class_sessions.each { |cs| cs.destroy if cs.persisted? }

            { json: { errors: errors(class_sessions) } }
          end
        )
      end

      def errors(class_sessions)
        class_sessions
          .map { |cs| cs.errors.messages }
          .uniq
          .each_with_object({}) do |errors, memo|
            errors.each do |k, v|
              memo[k] ||= []

              memo[k].concat(v)
            end
          end
      end
    end
  end
end
