module Ui
  module V1
    class TodosController < Ui::ApplicationController
      def index
        render json: policy_scope(Todo).where(user_id: current_user.id, complete: false).order(created_at: :desc)
      end

      def create
        todo = current_user.todos.new(todo_params.merge(account: current_account))

        authorize todo

        if todo.save
          render json: todo
        else
          render json: { errors: todo.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def update
        todos = Todo.where(id: params[:ids])

        authorize todos

        todos.update(complete: true, completed_on: Time.zone.now)

        render json: :ok
      end

      private

      def todo_params
        params.require(:todo).permit(:body)
      end
    end
  end
end
