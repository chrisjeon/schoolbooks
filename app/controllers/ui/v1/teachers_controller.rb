module Ui
  module V1
    class TeachersController < Ui::ApplicationController
      def index
        render(
          json: policy_scope(User).with_role(:teacher).includes(:levels),
          each_serializer: TeacherSerializer,
          key_transform: :camel_lower
        )
      end
    end
  end
end
