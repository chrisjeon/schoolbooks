module Ui
  module V1
    class RoomsController < Ui::ApplicationController
      serialization_scope :class_schedule

      def index
        render(
          json: policy_scope(Room),
          key_transform: :camel_lower,
          include: [
            :class_sessions,
            'class_sessions.teacher.levels',
            'class_sessions.start_end_time',
            'class_sessions.students',
            'class_sessions.teacher',
            'class_sessions.room'
          ]
        )
      end

      private

      def class_schedule
        @_class_schedule ||= ClassSchedule.find(params[:class_schedule_id])
      end
    end
  end
end
