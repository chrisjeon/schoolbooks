module Ui
  module V1
    class ScheduleExportsController < Ui::ApplicationController
      def create
        authorize :schedule_export

        ExportAndSendScheduleJob.perform_later(
          ClassSchedule.find(params[:id])
        )

        head :ok
      end
    end
  end
end
