module Ui
  module V1
    class StudentsController < Ui::ApplicationController
      def index
        students = policy_scope(User).with_role(:student).includes(:levels, :pricing_plan)

        students = students.where('full_name ~* ?', params[:nameQuery]) unless params[:nameQuery].blank?
        students = students.where(levels: { id: params[:levelId] }) unless params[:levelId].to_i.zero?

        render(
          json: students,
          scope: class_schedule_id,
          scope_name: :class_schedule_id,
          key_transform: :camel_lower,
          each_serializer: StudentSerializer
        )
      end

      private

      def class_schedule_id
        params[:class_schedule_id]
      end
    end
  end
end
