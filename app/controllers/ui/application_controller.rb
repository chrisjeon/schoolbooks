module Ui
  class ApplicationController < ApplicationController
    before_action :authenticate_user!

    def user_not_authorized
      render json: { error: 'You are not authorized to perform this action.' }, status: :unauthorized
    end
  end
end
