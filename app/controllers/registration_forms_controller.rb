class RegistrationFormsController < ApplicationController
  before_action :authenticate_user!, :load_registration_form

  def show
    authorize @registration_form
  end

  def update
    authorize @registration_form

    if @registration_form.update_attributes(registration_form_params)
      flash.now[:success] = I18n.t('registration_form_successfully_updated')
      render :show, change: :registration_form
    else
      flash.now[:error] = I18n.t('something_went_wrong')
      render :show, change: :registration_form
    end
  end

  private

  def load_registration_form
    @registration_form = current_account.registration_form
  end

  def registration_form_params
    params.require(:registration_form).permit(:custom_form)
  end
end
