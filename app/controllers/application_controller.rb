class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :layout_by_authentication

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :set_paper_trail_whodunnit
  before_action :redirect_to_user_initial_setup, if: Proc.new {
    current_user && !current_user.account
  }
  before_action :redirect_to_account_subscriptions, if: Proc.new {
    current_user &&
      current_account &&
      current_account.account_subscription &&
      !current_account.subscription_plan.free? &&
      current_account.account_subscription.stripe_customer_token.blank?
  }

  around_action :set_time_zone, if: Proc.new { current_user && current_user.account }

  private

  def current_account
    @_current_account ||= current_user.account
  end
  helper_method :current_account

  def set_time_zone(&block)
    Time.use_zone(current_user.account.time_zone, &block)
  end

  def redirect_to_user_initial_setup
    return if controller_name != 'user_initial_setup'
    return if controller_name != 'assign_user_to_accounts'
    return if controller_name == 'accounts' && action_name == 'create'
    redirect_to user_initial_setup_index_path
  end

  def redirect_to_account_subscriptions
    return if controller_name.in?(%w(user_initial_setup assign_user_to_accounts account_subscriptions stripe_subscriptions))
    flash[:warning] = I18n.t('you_must_enter_in_your_credit_card_information_to_begin_your_14_day_trial')
    redirect_to account_subscription_path(current_account.account_subscription)
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit(:email, :first_name, :last_name, :password, :account_id)
    end
  end

  def user_not_authorized
    flash[:error] = 'You are not authorized to perform this action.'
    redirect_to(request.referrer || root_path)
  end

  def layout_by_authentication
    if user_signed_in?
      'application'
    else
      'landing'
    end
  end
end
