class IntegrationsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_integration, only: [:show, :edit, :update, :destroy]

  def index
    @integrations = policy_scope(Integration)
  end

  def show
    authorize @integration
  end

  def new
    @integration = current_account.integrations.new
    authorize @integration
  end

  def edit
    authorize @integration
  end

  def create
    @integration = current_account.integrations.new(integration_params)
    authorize @integration

    if @integration.save
      flash[:success] = I18n.t('integration_successfully_created')
      redirect_to integration_path(@integration)
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    if @integration.update_attributes(integration_params)
      flash[:success] = I18n.t('integration_successfully_updated')
      redirect_to integration_path(@integration)
    else
      flash[:error] = I18n.t('something_went_wrong')
    end
  end

  def destroy
    @integration.destroy
    flash[:success] = I18n.t('integration_successfully_deleted')
    redirect_to integrations_path
  end

  private

  def find_integration
    @integration = current_account.integrations.find(params[:id])
  end

  def integration_params
    params.require(:integration).permit(:type, :file)
  end
end
