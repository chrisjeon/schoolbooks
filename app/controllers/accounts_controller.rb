class AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_account, only: [:show, :update]

  def show
    authorize @account
  end

  def create
    @account = Account.new(account_params)
    @account.build_account_subscription if @account.account_subscription.nil?
    @account.account_subscription.account = @account

    if @account.save
      flash[:success] = I18n.t('account_successfully_created')
      current_user.update_attributes(account: @account) if current_user.account.nil?
      current_user.add_role(:admin) unless current_user.has_role?(:admin)
    else
      flash[:error] = I18n.t('something_went_wrong')
    end

    redirect_to root_path
  end

  def update
    authorize @account

    if @account.update_attributes(account_params)
      flash.now[:success] = I18n.t('account_successfully_updated')
    else
      flash.now[:error] = I18n.t('something_went_wrong')
    end

    render :show, change: :account
  end

  private

  def account_params
    params.require(:account).permit(:name, :time_zone, :country, :currency,
      :invoices_sent_on, :address, :address2, :city, :state, :zip_code, :phone,
      :payments_due_on, :has_registration_form, :code, :email,
      account_subscription_attributes: [:subscription_plan_id, :account_id])
  end

  def find_account
    @account = Account.find(params[:id])
  end
end
