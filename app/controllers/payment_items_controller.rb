class PaymentItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_payment_item, only: [:show, :edit, :update, :destroy]

  def index
    @payment_items = policy_scope(PaymentItem)
    @payment_items = @payment_items.includes(:user)
  end

  def new
    @payment_item = current_account.payment_items.new
    authorize(@payment_item)
    @payment_item.build_note
    @payment_item.itemable = MiscPaymentItem.new
  end

  def show
    authorize(@payment_item)
  end

  def edit
    authorize(@payment_item)
  end

  def create
    @payment_item = PaymentItem.new(payment_item_params.merge(account: current_account).reject { |k, _v| k == 'itemable_attributes' })
    authorize(@payment_item)
    @payment_item.build_note if @payment_item.note.nil?
    @payment_item.itemable = MiscPaymentItem.new(account: current_account, user: @payment_item.user) if @payment_item.itemable.nil?
    @payment_item.itemable.price_currency = current_account.currency
    @payment_item.itemable.currency = current_account.currency
    @payment_item.itemable.price = if payment_item_params[:itemable_attributes][:price_cents].try { |pc| pc.include?('.') }
                                     payment_item_params[:itemable_attributes][:price_cents].try(:to_f)
                                   else
                                     payment_item_params[:itemable_attributes][:price_cents].try(:to_i)
                                   end
    @payment_item.note.noteable = @payment_item
    @payment_item.due_date = params[:due_on]

    if @payment_item.save
      flash[:success] = I18n.t('payment_item_successfully_created')
      redirect_to @payment_item
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    authorize(@payment_item)
    @payment_item.assign_attributes(payment_item_params.reject { |k, _v| k == 'itemable_attributes' })
    @payment_item.itemable.price_currency = current_account.currency
    @payment_item.itemable.currency = current_account.currency
    @payment_item.itemable.price = if payment_item_params[:itemable_attributes][:price_cents].try { |pc| pc.include?('.') }
                                     payment_item_params[:itemable_attributes][:price_cents].try(:to_f)
                                   else
                                     payment_item_params[:itemable_attributes][:price_cents].try(:to_i)
                                   end
    @payment_item.due_date = params[:due_on]

    if @payment_item.save
      flash[:success] = I18n.t('payment_item_successfully_updated')
      redirect_to @payment_item
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :show
    end
  end

  def destroy
    authorize(@payment_item)
    @payment_item.destroy
    flash[:success] = I18n.t('payment_item_successfully_deleted')
    redirect_to payment_items_path
  end

  private

  def find_payment_item
    @payment_item = current_account.payment_items.find(params[:id])
  end

  def payment_item_params
    params.require(:payment_item).permit(:paid, :user_id, :due_on,
      itemable_attributes: [:price_cents],
      note_attributes: [:id, :noteable, :noteable_id, :noteable_type, :body])
  end
end
