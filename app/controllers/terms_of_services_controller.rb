class TermsOfServicesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_terms_of_service, only: [:show, :edit, :update, :destroy]

  def index
    @terms_of_services = policy_scope(TermsOfService)
  end

  def new
    @terms_of_service = current_account.terms_of_services.new
    authorize @terms_of_service
  end

  def show
    authorize @terms_of_service
  end

  def edit
    authorize @terms_of_service
  end

  def create
    @terms_of_service = current_account.terms_of_services.new(terms_of_service_params)

    if @terms_of_service.save
      flash[:success] = I18n.t('terms_of_service_successfully_created')
      redirect_to @terms_of_service
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :new
    end
  end

  def update
    if @terms_of_service.update_attributes(terms_of_service_params)
      flash[:success] = I18n.t('terms_of_service_successfully_updated')
      redirect_to @terms_of_service
    else
      flash[:error] = I18n.t('something_went_wrong')
      render :edit
    end
  end

  def destroy
    authorize @terms_of_service
    @terms_of_service.destroy
    redirect_to terms_of_services_path
  end

  private

  def find_terms_of_service
    @terms_of_service = current_account.terms_of_services.find(params[:id])
  end

  def terms_of_service_params
    params.require(:terms_of_service).permit(:name, :body, :default)
  end
end
