class HousingOccupanciesController < ApplicationController
  before_action :authenticate_user!, :find_housing
  before_action :find_housing_occupancy, except: :create

  def create
    @housing_occupancy = HousingOccupancy.new(housing_occupancy_params.merge(account: current_account, housing: @housing))
    authorize @housing_occupancy
    @housing_occupancy.build_start_end_time if @housing_occupancy.start_end_time.nil?
    start_date = @housing_occupancy.start_end_time.start_date
    end_date = @housing_occupancy.start_end_time.end_date
    if start_date.present? || end_date.present?
      @housing_occupancy.start_end_time.start_date = start_date.to_date.strftime('%m/%d/%Y')
      @housing_occupancy.start_end_time.end_date = end_date.to_date.strftime('%m/%d/%Y')
      @housing_occupancy.start_end_time.timeable = @housing_occupancy
      @housing_occupancy.start_end_time.account = current_account
    end
    @housing_occupancy.price_currency = current_account.currency
    @housing_occupancy.currency = current_account.currency
    @housing_occupancy.price = if housing_occupancy_params[:price_cents].try { |pc| pc.include?('.') }
                                 housing_occupancy_params[:price_cents].try(:to_f)
                               else
                                 housing_occupancy_params[:price_cents].try(:to_i)
                               end

    if @housing_occupancy.save
      flash[:success] = I18n.t('user_successfully_added_to_house')
    else
      flash[:error] = I18n.t('something_went_wrong')
    end
    redirect_to @housing
  end

  def update
    authorize @housing_occupancy

    if @housing_occupancy.update_attributes(housing_occupancy_params)
      flash[:success] = I18n.t('housing_occupancy_updated_successfully')
    else
      flash[:error] = I18n.t('something_went_wrong')
    end
    redirect_to @housing

  end

  def destroy
    authorize @housing_occupancy
    @housing_occupancy.destroy
    flash[:success] = I18n.t('user_successfully_removed_from_house')
    redirect_to @housing
  end

  private

  def housing_occupancy_params
    params.require(:housing_occupancy).permit(:user_id, :account_id, :price_cents,
      start_end_time_attributes: [:start_date, :end_date, :timeable_id, :timeable_type])
  end

  def find_housing
    @housing = current_account.housings.find(params[:housing_id])
  end

  def find_housing_occupancy
    @housing_occupancy = @housing.housing_occupancies.find(params[:id])
  end
end
