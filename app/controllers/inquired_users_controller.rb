class InquiredUsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = policy_scope(User).inquired(current_account.id)
    @search_term = params[:search_term]
    @users = @users.
             filter(params.slice(:search_term)).
             page(params[:page])
  end
end
