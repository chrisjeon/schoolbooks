class IntegrationProcessorsController < ApplicationController
  before_action :authenticate_user!

  def create
    @integration = current_account.integrations.find(params[:id])
    ProcessIntegrationsJob.perform_later(@integration)
    flash[:success] = I18n.t('integration_import_successfully_scheduled')
    redirect_to integration_path(@integration)
  end
end
