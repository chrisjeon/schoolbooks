class AddPrivatePlanBooleanFieldToPricingPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :pricing_plans, :private_plan, :boolean, default: false, null: false
  end
end
