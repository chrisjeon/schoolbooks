class AddAccountReferenceToEventAttendances < ActiveRecord::Migration[5.0]
  def change
    add_reference :event_attendances, :account, foreign_key: true
  end
end
