class CreateAvailableDays < ActiveRecord::Migration[5.0]
  def change
    create_table :available_days do |t|
      t.string :type
      t.datetime :start_on
      t.datetime :end_on
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
