class AddInvoicesSentOnToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :invoices_sent_on, :string

    Account.find_each do |account|
      account.update_attributes(invoices_sent_on: 'Everyday')
    end
  end
end
