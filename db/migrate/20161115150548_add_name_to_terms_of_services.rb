class AddNameToTermsOfServices < ActiveRecord::Migration[5.0]
  def change
    add_column :terms_of_services, :name, :string
  end
end
