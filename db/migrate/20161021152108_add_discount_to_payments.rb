class AddDiscountToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :discount, :float

    Payment.find_each(&:save)
  end
end
