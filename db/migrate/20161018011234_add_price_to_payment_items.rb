class AddPriceToPaymentItems < ActiveRecord::Migration[5.0]
  def change
    change_table :payment_items do |t|
      t.monetize :price
      t.column :currency, :string
    end

    PaymentItem.find_each(&:save)
  end
end
