class CreateMiscPaymentItems < ActiveRecord::Migration[5.0]
  def change
    create_table :misc_payment_items do |t|
      t.references :account, foreign_key: true
      t.references :user, foreign_key: true
      t.string :currency
      t.monetize :price

      t.timestamps
    end
  end
end
