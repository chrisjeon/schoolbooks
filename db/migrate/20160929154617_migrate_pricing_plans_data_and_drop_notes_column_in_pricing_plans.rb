class MigratePricingPlansDataAndDropNotesColumnInPricingPlans < ActiveRecord::Migration[5.0]
  def change
    PricingPlan.where.not(notes: nil).each do |pricing_plan|
      Note.create(noteable: pricing_plan, body: pricing_plan.notes)
    end

    remove_column :pricing_plans, :notes
  end
end
