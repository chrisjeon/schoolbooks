class CreateTermsOfServiceAcceptances < ActiveRecord::Migration[5.0]
  def change
    create_table :terms_of_service_acceptances do |t|
      t.references :account, foreign_key: true
      t.references :user, foreign_key: true
      t.references :terms_of_service, foreign_key: true

      t.timestamps
    end
  end
end
