class CreatePricingPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :pricing_plans do |t|
      t.references :account, foreign_key: true
      t.string :name
      t.monetize :price
      t.text :notes

      t.timestamps
    end
  end
end
