class AddAvailableInMorningAndAvailableInAfternoonToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      t.boolean :available_in_morning, default: true, null: false
      t.boolean :available_in_afternoon, default: true, null: false
    end
  end
end
