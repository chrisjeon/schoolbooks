class CreatePlanEnrollments < ActiveRecord::Migration[5.0]
  def change
    create_table :plan_enrollments do |t|
      t.references :account, foreign_key: true
      t.references :user, foreign_key: true
      t.references :pricing_plan, foreign_key: true
      t.boolean :active, default: true, null: false

      t.timestamps
    end
  end
end
