class AddHoursPerDayToPricingPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :pricing_plans, :hours_per_day, :integer

    PricingPlan.where.not(hours_per_week: nil).each do |pricing_plan|
      hours_per_week = pricing_plan.hours_per_week
      pricing_plan.update_attributes(hours_per_day: hours_per_week / 5)
    end
  end
end
