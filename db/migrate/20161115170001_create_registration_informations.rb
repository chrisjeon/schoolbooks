class CreateRegistrationInformations < ActiveRecord::Migration[5.0]
  def change
    create_table :registration_informations do |t|
      t.references :account, foreign_key: true
      t.references :user, foreign_key: true
      t.date :starting_date
      t.integer :sign_up_period
      t.references :pricing_plan, foreign_key: true
      t.text :current_level_description
      t.text :how_did_you_hear_about_us
      t.text :why_did_you_choose_us
      t.text :anything_else
      t.text :housing_information
      t.integer :coaching_sessions_per_week

      t.timestamps
    end
  end
end
