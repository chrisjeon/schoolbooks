class CreateHousingOccupancies < ActiveRecord::Migration[5.0]
  def change
    create_table :housing_occupancies do |t|
      t.references :user, foreign_key: true
      t.references :account, foreign_key: true
      t.references :housing, foreign_key: true
      t.boolean :active, default: true, null: false

      t.timestamps
    end
  end
end
