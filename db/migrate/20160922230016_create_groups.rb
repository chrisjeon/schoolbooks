class CreateGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :groups do |t|
      t.references :account, foreign_key: true
      t.boolean :active, default: true, null: false
      t.string :name

      t.timestamps
    end
  end
end
