class AddNotNullContraintToAvailableDaysAvailable < ActiveRecord::Migration[5.0]
  def change
    change_column :available_days, :available, :boolean, null: false
  end
end
