class DropStartOnEndOnColumnAndMigrateDataForEventSessions < ActiveRecord::Migration[5.0]
  def change
    EventSession.find_each do |event_session|
      StartEndTime.create(start_on: event_session.start_on,
                          end_on: event_session.end_on,
                          account: event_session.account,
                          timeable: event_session)
    end

    change_table :event_sessions do |t|
      t.remove :start_on, :end_on
    end
  end
end
