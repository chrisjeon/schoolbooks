class CreateRegistrationForms < ActiveRecord::Migration[5.0]
  def change
    create_table :registration_forms do |t|
      t.references :account, foreign_key: true
      t.boolean :custom_form, null: false, default: false

      t.timestamps
    end
  end
end
