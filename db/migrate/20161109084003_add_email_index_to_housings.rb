class AddEmailIndexToHousings < ActiveRecord::Migration[5.0]
  def change
    add_index :housings, :email, unique: true
  end
end
