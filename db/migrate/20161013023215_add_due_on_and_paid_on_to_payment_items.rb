class AddDueOnAndPaidOnToPaymentItems < ActiveRecord::Migration[5.0]
  def change
    add_column :payment_items, :due_on, :datetime
    add_column :payment_items, :paid_on, :datetime
  end
end
