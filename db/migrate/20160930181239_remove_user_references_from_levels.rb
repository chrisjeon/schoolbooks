class RemoveUserReferencesFromLevels < ActiveRecord::Migration[5.0]
  def change
    remove_reference :levels, :user, foreign_key: true, index: true
  end
end
