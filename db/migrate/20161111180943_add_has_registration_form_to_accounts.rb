class AddHasRegistrationFormToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :has_registration_form, :boolean, null: false, default: false
  end
end
