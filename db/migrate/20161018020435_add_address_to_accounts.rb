class AddAddressToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :address, :string
    add_column :accounts, :address2, :string
    add_column :accounts, :city, :string
    add_column :accounts, :state, :string
    add_column :accounts, :zip_code, :string
    add_column :accounts, :phone, :string
  end
end
