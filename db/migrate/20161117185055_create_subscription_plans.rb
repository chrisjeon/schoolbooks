class CreateSubscriptionPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :subscription_plans do |t|
      t.string :name
      t.monetize :price
      t.string :stripe_plan_id
      t.string :currency

      t.timestamps
    end
  end
end
