class AddPriceAndCurrencyToHousingOccupancies < ActiveRecord::Migration[5.0]
  def change
    change_table :housing_occupancies do |t|
      t.monetize :price
      t.column :currency, :string
    end
  end
end
