class AddDateOfCompletionToTodos < ActiveRecord::Migration[5.0]
  def change
    add_column :todos, :completed_on, :datetime
  end
end
