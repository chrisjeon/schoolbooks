class AddPriceAndCurrencyToPayments < ActiveRecord::Migration[5.0]
  def change
    change_table :payments do |t|
      t.monetize :subtotal
      t.column :currency, :string
    end

    Payment.find_each(&:save)
  end
end
