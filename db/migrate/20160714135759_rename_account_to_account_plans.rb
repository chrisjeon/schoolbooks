class RenameAccountToAccountPlans < ActiveRecord::Migration[5.0]
  def change
    # remove_foreign_key :users, :accounts
    # remove_index :users, :account_id
    # remove_column :users, :account_id, :integer
    remove_reference :users, :account, index: true, foreign_key: true
    rename_table :accounts, :account_plans
    add_reference :users, :account_plan, foreign_key: true
  end
end
