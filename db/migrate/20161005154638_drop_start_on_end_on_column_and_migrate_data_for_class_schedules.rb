class DropStartOnEndOnColumnAndMigrateDataForClassSchedules < ActiveRecord::Migration[5.0]
  def change
    ClassSchedule.find_each do |class_schedule|
      StartEndTime.create(start_on: class_schedule.start_on,
                          end_on: class_schedule.end_on,
                          account: class_schedule.account,
                          timeable: class_schedule)
    end

    change_table :class_schedules do |t|
      t.remove :start_on, :end_on
      t.column :name, :string
    end

    ClassSchedule.find_each do |class_schedule|
      class_schedule.name = "#{class_schedule.start_end_time.start_on.in_time_zone(class_schedule.account.time_zone).strftime('%-m/%-d/%Y').strip} - #{class_schedule.start_end_time.end_on.in_time_zone(class_schedule.account.time_zone).strftime('%-m/%-d/%Y').strip}"
      class_schedule.save
    end
  end
end
