class AddTotalToPayments < ActiveRecord::Migration[5.0]
  def change
    change_table :payments do |t|
      t.monetize :total
    end

    Payment.find_each(&:save)
  end
end
