class RenameAccountPlansToAccounts < ActiveRecord::Migration[5.0]
  def change
    # remove_foreign_key :users, :account_plans
    # remove_index :users, :account_plan_id
    # remove_column :users, :account_plan_id, :integer
    remove_reference :users, :account_plan, index: true, foreign_key: true
    rename_table :account_plans, :accounts
    add_reference :users, :account, foreign_key: true
  end
end
