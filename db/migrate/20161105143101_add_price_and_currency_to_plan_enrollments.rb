class AddPriceAndCurrencyToPlanEnrollments < ActiveRecord::Migration[5.0]
  def change
    change_table :plan_enrollments do |t|
      t.monetize :price
      t.column :currency, :string
    end
  end
end
