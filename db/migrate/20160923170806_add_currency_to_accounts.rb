class AddCurrencyToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :currency, :string
  end
end
