class AddCodeToPricingPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :pricing_plans, :code, :string
  end
end
