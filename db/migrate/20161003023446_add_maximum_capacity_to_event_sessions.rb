class AddMaximumCapacityToEventSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :event_sessions, :maximum_capacity, :integer
  end
end
