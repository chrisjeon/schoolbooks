class AddFrequencyToPricingPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :pricing_plans, :frequency, :integer
    add_column :pricing_plans, :frequency_unit, :string
  end
end
