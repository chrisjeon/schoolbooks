class ChangePriceCentsColumnFromIntToBigInt < ActiveRecord::Migration[5.0]
  def change
    change_column :pricing_plans, :price_cents, :bigint
  end
end
