class CreateClassAttendances < ActiveRecord::Migration[5.0]
  def change
    create_table :class_attendances do |t|
      t.references :account, foreign_key: true
      t.references :class_schedule, foreign_key: true
      t.references :class_session, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
