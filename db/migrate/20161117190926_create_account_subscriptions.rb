class CreateAccountSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :account_subscriptions do |t|
      t.references :subscription_plan, foreign_key: true
      t.references :account, foreign_key: true
      t.string :stripe_customer_token
      t.string :aasm_state

      t.timestamps
    end
  end
end
