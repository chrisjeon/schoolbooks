class AddFullNameToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :full_name, :string

    User.find_each do |user|
      user.update_attributes(full_name: "#{user.first_name} #{user.last_name}")
    end
  end
end
