class ModifyClassSessions < ActiveRecord::Migration[5.0]
  def change
    change_table :class_sessions do |t|
      t.remove_references :student, index: true
      t.remove_references :teacher, index: true
      t.references :user, index: true, foreign_key: true
    end
  end
end
