class RemoveActiveColumnFromHousingOccupancies < ActiveRecord::Migration[5.0]
  def change
    remove_column :housing_occupancies, :active
  end
end
