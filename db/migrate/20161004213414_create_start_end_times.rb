class CreateStartEndTimes < ActiveRecord::Migration[5.0]
  def change
    create_table :start_end_times do |t|
      t.references :account, foreign_key: true
      t.references :timeable, polymorphic: true, index: true
      t.datetime :start_on
      t.datetime :end_on

      t.timestamps
    end
  end
end
