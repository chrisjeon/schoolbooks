class DropStartOnEndOnColumnAndMigrateDataForEvents < ActiveRecord::Migration[5.0]
  def change
    Event.find_each do |event|
      StartEndTime.create(start_on: event.start_on,
                          end_on: event.end_on,
                          account: event.account,
                          timeable: event)
    end

    change_table :events do |t|
      t.remove :start_on, :end_on
    end
  end
end
