class AddHoursPerWeekToPricingPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :pricing_plans, :hours_per_week, :integer
  end
end
