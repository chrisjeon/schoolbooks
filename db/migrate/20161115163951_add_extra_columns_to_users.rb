class AddExtraColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :country, :string
    add_column :users, :gender, :string
    add_column :users, :passport_number, :string
    add_column :users, :birth_date, :date
    add_column :users, :emergency_contact_name, :string
    add_column :users, :emergency_contact_number, :string
  end
end
