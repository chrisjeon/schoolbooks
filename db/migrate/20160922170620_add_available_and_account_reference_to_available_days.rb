class AddAvailableAndAccountReferenceToAvailableDays < ActiveRecord::Migration[5.0]
  def change
    add_column :available_days, :available, :boolean, default: false
    add_reference :available_days, :account, foreign_key: true
  end
end
