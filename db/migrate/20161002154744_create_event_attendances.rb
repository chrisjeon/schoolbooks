class CreateEventAttendances < ActiveRecord::Migration[5.0]
  def change
    create_table :event_attendances do |t|
      t.references :event, foreign_key: true
      t.references :event_session, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :paid, default: false, null: false

      t.timestamps
    end
  end
end
