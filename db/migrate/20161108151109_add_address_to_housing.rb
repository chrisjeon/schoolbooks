class AddAddressToHousing < ActiveRecord::Migration[5.0]
  def change
    add_column :housings, :address, :text
  end
end
