class DropActiveColumnFromPlanEnrollments < ActiveRecord::Migration[5.0]
  def change
    remove_column :plan_enrollments, :active
  end
end
