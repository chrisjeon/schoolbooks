class AddNullFalseConstraintToRolesDefault < ActiveRecord::Migration[5.0]
  def change
    change_column :roles, :default, :boolean, default: false, null: false
  end
end
