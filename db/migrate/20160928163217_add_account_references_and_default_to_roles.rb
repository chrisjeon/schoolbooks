class AddAccountReferencesAndDefaultToRoles < ActiveRecord::Migration[5.0]
  def change
    add_reference :roles, :account, foreign_key: true
    add_column :roles, :default, :boolean, default: false
    change_column :roles, :name, :string, null: false
  end
end
