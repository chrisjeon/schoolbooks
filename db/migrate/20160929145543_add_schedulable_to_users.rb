class AddSchedulableToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :schedulable, :boolean, default: true, null: false
  end
end
