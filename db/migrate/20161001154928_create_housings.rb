class CreateHousings < ActiveRecord::Migration[5.0]
  def change
    create_table :housings do |t|
      t.references :account, foreign_key: true
      t.string :owner_name
      t.string :email
      t.string :phone
      t.text :description
      t.text :additional_services_and_costs
      t.integer :maximum_occupancy
      t.monetize :monthly_cost
      t.monetize :weekly_cost
      t.boolean :confirmed_host, default: true, null: false
      t.string :url
      t.string :currency

      t.timestamps
    end
  end
end
