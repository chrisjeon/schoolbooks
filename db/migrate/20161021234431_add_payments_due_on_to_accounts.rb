class AddPaymentsDueOnToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :payments_due_on, :string

    Account.find_each(&:save)
  end
end
