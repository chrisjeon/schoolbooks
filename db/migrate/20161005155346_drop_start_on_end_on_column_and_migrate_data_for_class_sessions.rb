class DropStartOnEndOnColumnAndMigrateDataForClassSessions < ActiveRecord::Migration[5.0]
  def change
    ClassSession.find_each do |class_session|
      StartEndTime.create(start_on: class_session.start_on,
                          end_on: class_session.end_on,
                          account: class_session.account,
                          timeable: class_session)
    end

    change_table :class_sessions do |t|
      t.remove :start_on, :end_on
    end
  end
end
