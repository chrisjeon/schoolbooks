class RemoveNullFalseConstraintOnNameFromRoles < ActiveRecord::Migration[5.0]
  def change
    change_column :roles, :name, :string, null: true
  end
end
