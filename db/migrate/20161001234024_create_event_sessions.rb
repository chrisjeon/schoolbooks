class CreateEventSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :event_sessions do |t|
      t.references :account, foreign_key: true
      t.references :event, foreign_key: true
      t.datetime :start_on
      t.datetime :end_on

      t.timestamps
    end
  end
end
