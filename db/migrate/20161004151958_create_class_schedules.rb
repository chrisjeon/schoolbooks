class CreateClassSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :class_schedules do |t|
      t.references :account, foreign_key: true
      t.datetime :start_on
      t.datetime :end_on
      t.integer :maximum_students_per_class

      t.timestamps
    end
  end
end
