class CreateClassSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :class_sessions do |t|
      t.references :account, foreign_key: true
      t.references :class_schedule, foreign_key: true
      t.datetime :start_on
      t.datetime :end_on
      t.references :student
      t.references :teacher
      t.references :room, foreign_key: true

      t.timestamps
    end
  end
end
