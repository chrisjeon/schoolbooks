class AddStripeBillingEndDateToAccountSubscriptions < ActiveRecord::Migration[5.0]
  def change
    add_column :account_subscriptions, :stripe_billing_end_date, :string
  end
end
