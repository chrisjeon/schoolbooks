class RenameGroupsToLevels < ActiveRecord::Migration[5.0]
  def change
    rename_table :groups, :levels
    rename_table :group_memberships, :level_memberships
    rename_column :level_memberships, :group_id, :level_id
  end 
end
