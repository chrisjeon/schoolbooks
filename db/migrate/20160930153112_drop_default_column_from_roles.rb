class DropDefaultColumnFromRoles < ActiveRecord::Migration[5.0]
  def change
    remove_column :roles, :default
  end
end
