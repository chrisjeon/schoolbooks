class AddEmailToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :email, :string

    Account.where(email: nil).each do |account|
      default_email = account.users.with_role(:admin).order(:id).first.email
      account.update_attribute(:email, default_email)
    end
  end
end
