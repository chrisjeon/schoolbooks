class CreateTodos < ActiveRecord::Migration[5.0]
  def change
    create_table :todos do |t|
      t.references :account, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :requester_id
      t.index :requester_id
      t.text :body
      t.boolean :complete, default: false, null: false

      t.timestamps
    end
  end
end
