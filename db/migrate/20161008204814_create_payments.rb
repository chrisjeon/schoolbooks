class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.references :account, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :paid, default: false, null: false
      t.datetime :due_on
      t.datetime :paid_on

      t.timestamps
    end
  end
end
