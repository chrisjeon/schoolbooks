class CreateIntegrations < ActiveRecord::Migration[5.0]
  def change
    create_table :integrations do |t|
      t.references :account, foreign_key: true
      t.string :type
      t.attachment :file

      t.timestamps
    end
  end
end
