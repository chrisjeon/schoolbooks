class CreatePaymentItems < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_items do |t|
      t.references :itemable, polymorphic: true, index: true
      t.references :account, foreign_key: true
      t.references :user, foreign_key: true
      t.references :payment, foreign_key: true
      t.boolean :paid, default: false, null: false

      t.timestamps
    end
  end
end
