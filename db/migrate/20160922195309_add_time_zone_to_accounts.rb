class AddTimeZoneToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :time_zone, :string, default: 'UTC'
  end
end
