class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.references :account, foreign_key: true
      t.string :name
      t.integer :maximum_capacity
      t.monetize :price
      t.string :currency
      t.text :description
      t.datetime :start_on
      t.datetime :end_on
      t.boolean :has_sessions, default: false, null: false

      t.timestamps
    end
  end
end
