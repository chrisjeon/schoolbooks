class CreateTermsOfServices < ActiveRecord::Migration[5.0]
  def change
    create_table :terms_of_services do |t|
      t.references :account, foreign_key: true
      t.text :body
      t.boolean :default, null: false, default: false

      t.timestamps
    end
  end
end
