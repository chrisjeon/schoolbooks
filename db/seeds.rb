# Create Subscription Plans
SubscriptionPlan.create(name: 'Free',
                        price_cents: 0,
                        currency: 'USD',
                        stripe_plan_id: 'schoolbooks-free')

SubscriptionPlan.create(name: 'Micro',
                        price_cents: 4900,
                        currency: 'USD',
                        stripe_plan_id: 'schoolbooks-micro')

SubscriptionPlan.create(name: 'Pro',
                        price_cents: 7900,
                        currency: 'USD',
                        stripe_plan_id: 'schoolbooks-pro')

SubscriptionPlan.create(name: 'Premium',
                        price_cents: 9900,
                        currency: 'USD',
                        stripe_plan_id: 'schoolbooks-premium')

SubscriptionPlan.create(name: 'God Mode',
                        price_cents: 0,
                        currency: 'USD',
                        stripe_plan_id: 'schoolbooks-god-mode')

# Create Account
account = Account.create(email: 'erica@colombiaimmersion.com', name: 'Colombia Immersion', time_zone: 'EST', invoices_sent_on: 'Sunday')
micro_subscription_plan = SubscriptionPlan.find_by(name: 'Micro')
AccountSubscription.create(account: account, subscription_plan: micro_subscription_plan)

# Create User
students = []
students << User.new(email: 'chris0374@gmail.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Chris',
                     last_name: 'Jeon')

50.times do
  students << User.new(email: Faker::Internet.email,
                       password: 'password',
                       password_confirmation: 'password',
                       first_name: Faker::Name.first_name,
                       last_name: Faker::Name.last_name)
end

students.each do |student|
  student.add_role :student
  student.save
end

teachers = []
teachers << User.new(email: 'julio@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Julio',
                     last_name: 'Ponce')

teachers << User.new(email: 'erica@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Erica',
                     last_name: 'Borgardijn')

teachers << User.new(email: 'andres@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Andrés',
                     last_name: 'Celis')

teachers << User.new(email: 'inaki@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Iñaki',
                     last_name: 'Pérez')

teachers << User.new(email: 'andrey@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Andrey',
                     last_name: 'Barrera')

teachers << User.new(email: 'avalon@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Avalon',
                     last_name: 'Bauman')

teachers << User.new(email: 'pipe@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Pipe',
                     last_name: 'Vergara')

teachers << User.new(email: 'camilo@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Camilo',
                     last_name: 'Martínez')

teachers << User.new(email: 'merly@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Merly',
                     last_name: 'Davianny')

teachers << User.new(email: 'daniel@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Daniel',
                     last_name: 'Zapata')

teachers << User.new(email: 'sebastian@colombiaimmersion.com',
                     password: 'password',
                     password_confirmation: 'password',
                     first_name: 'Sebastian',
                     last_name: 'Perez')

teachers.each(&:save)

teachers.each do |teacher|
  %w(teacher admin employee).each { |role_name| teacher.add_role role_name.to_sym }
end

account.users << students + teachers

User.with_role(:student).update_all(temporary: true)

intensive_classes      = PricingPlan.new(name: 'Intensive Classes')
afternoon_classes      = PricingPlan.new(name: 'Afternoon Classes')
private_classes        = PricingPlan.new(name: 'Private Classes')
full_immersion_package = PricingPlan.new(name: 'Full Immersion Package')

intensive_classes.price = 550000
afternoon_classes.price = 300000
private_classes.price = 275000
full_immersion_package.price = 2700000

intensive_classes.frequency = 1
intensive_classes.frequency_unit = 'week'

afternoon_classes.frequency = 1
afternoon_classes.frequency_unit = 'week'

private_classes.frequency = 5
private_classes.frequency_unit = 'hour'

full_immersion_package.frequency = 4
full_immersion_package.frequency_unit = 'week'

[intensive_classes, afternoon_classes, private_classes, full_immersion_package].each do |package|
  package.price_currency = 'COP'
  package.currency = 'COP'
  package.account = account
end

[intensive_classes, afternoon_classes, private_classes, full_immersion_package].each(&:save)
