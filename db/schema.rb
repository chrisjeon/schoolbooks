# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161118181337) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_subscriptions", force: :cascade do |t|
    t.integer  "subscription_plan_id"
    t.integer  "account_id"
    t.string   "stripe_customer_token"
    t.string   "aasm_state"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "stripe_billing_end_date"
    t.index ["account_id"], name: "index_account_subscriptions_on_account_id", using: :btree
    t.index ["subscription_plan_id"], name: "index_account_subscriptions_on_subscription_plan_id", using: :btree
  end

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "time_zone",             default: "UTC"
    t.string   "country"
    t.string   "currency"
    t.string   "invoices_sent_on"
    t.string   "address"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "phone"
    t.string   "payments_due_on"
    t.boolean  "has_registration_form", default: false, null: false
    t.string   "code"
    t.string   "email"
  end

  create_table "class_attendances", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "class_schedule_id"
    t.integer  "class_session_id"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["account_id"], name: "index_class_attendances_on_account_id", using: :btree
    t.index ["class_schedule_id"], name: "index_class_attendances_on_class_schedule_id", using: :btree
    t.index ["class_session_id"], name: "index_class_attendances_on_class_session_id", using: :btree
    t.index ["user_id"], name: "index_class_attendances_on_user_id", using: :btree
  end

  create_table "class_schedules", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "maximum_students_per_class"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "name"
    t.index ["account_id"], name: "index_class_schedules_on_account_id", using: :btree
  end

  create_table "class_sessions", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "class_schedule_id"
    t.integer  "room_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "user_id"
    t.index ["account_id"], name: "index_class_sessions_on_account_id", using: :btree
    t.index ["class_schedule_id"], name: "index_class_sessions_on_class_schedule_id", using: :btree
    t.index ["room_id"], name: "index_class_sessions_on_room_id", using: :btree
    t.index ["user_id"], name: "index_class_sessions_on_user_id", using: :btree
  end

  create_table "event_attendances", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "event_session_id"
    t.integer  "user_id"
    t.boolean  "paid",             default: false, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "account_id"
    t.index ["account_id"], name: "index_event_attendances_on_account_id", using: :btree
    t.index ["event_id"], name: "index_event_attendances_on_event_id", using: :btree
    t.index ["event_session_id"], name: "index_event_attendances_on_event_session_id", using: :btree
    t.index ["user_id"], name: "index_event_attendances_on_user_id", using: :btree
  end

  create_table "event_sessions", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "event_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "maximum_capacity"
    t.index ["account_id"], name: "index_event_sessions_on_account_id", using: :btree
    t.index ["event_id"], name: "index_event_sessions_on_event_id", using: :btree
  end

  create_table "events", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "name"
    t.integer  "maximum_capacity"
    t.integer  "price_cents",      default: 0,     null: false
    t.string   "price_currency",   default: "USD", null: false
    t.string   "currency"
    t.text     "description"
    t.boolean  "has_sessions",     default: false, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["account_id"], name: "index_events_on_account_id", using: :btree
  end

  create_table "housing_occupancies", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "account_id"
    t.integer  "housing_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
    t.string   "currency"
    t.index ["account_id"], name: "index_housing_occupancies_on_account_id", using: :btree
    t.index ["housing_id"], name: "index_housing_occupancies_on_housing_id", using: :btree
    t.index ["user_id"], name: "index_housing_occupancies_on_user_id", using: :btree
  end

  create_table "housings", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "owner_name"
    t.string   "email"
    t.string   "phone"
    t.text     "description"
    t.text     "additional_services_and_costs"
    t.integer  "maximum_occupancy"
    t.integer  "monthly_cost_cents",            default: 0,     null: false
    t.string   "monthly_cost_currency",         default: "USD", null: false
    t.integer  "weekly_cost_cents",             default: 0,     null: false
    t.string   "weekly_cost_currency",          default: "USD", null: false
    t.boolean  "confirmed_host",                default: true,  null: false
    t.string   "url"
    t.string   "currency"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.text     "address"
    t.index ["account_id"], name: "index_housings_on_account_id", using: :btree
    t.index ["email"], name: "index_housings_on_email", unique: true, using: :btree
  end

  create_table "integrations", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "type"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["account_id"], name: "index_integrations_on_account_id", using: :btree
  end

  create_table "level_memberships", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.integer  "level_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_level_memberships_on_account_id", using: :btree
    t.index ["level_id"], name: "index_level_memberships_on_level_id", using: :btree
    t.index ["user_id"], name: "index_level_memberships_on_user_id", using: :btree
  end

  create_table "levels", force: :cascade do |t|
    t.integer  "account_id"
    t.boolean  "active",     default: true, null: false
    t.string   "name"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["account_id"], name: "index_levels_on_account_id", using: :btree
  end

  create_table "misc_payment_items", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.string   "currency"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["account_id"], name: "index_misc_payment_items_on_account_id", using: :btree
    t.index ["user_id"], name: "index_misc_payment_items_on_user_id", using: :btree
  end

  create_table "notes", force: :cascade do |t|
    t.string   "noteable_type"
    t.integer  "noteable_id"
    t.text     "body"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["noteable_type", "noteable_id"], name: "index_notes_on_noteable_type_and_noteable_id", using: :btree
  end

  create_table "payment_items", force: :cascade do |t|
    t.string   "itemable_type"
    t.integer  "itemable_id"
    t.integer  "account_id"
    t.integer  "user_id"
    t.integer  "payment_id"
    t.boolean  "paid",           default: false, null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "due_on"
    t.datetime "paid_on"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
    t.string   "currency"
    t.index ["account_id"], name: "index_payment_items_on_account_id", using: :btree
    t.index ["itemable_type", "itemable_id"], name: "index_payment_items_on_itemable_type_and_itemable_id", using: :btree
    t.index ["payment_id"], name: "index_payment_items_on_payment_id", using: :btree
    t.index ["user_id"], name: "index_payment_items_on_user_id", using: :btree
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.boolean  "paid",              default: false, null: false
    t.datetime "due_on"
    t.datetime "paid_on"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "subtotal_cents",    default: 0,     null: false
    t.string   "subtotal_currency", default: "USD", null: false
    t.string   "currency"
    t.float    "discount"
    t.integer  "total_cents",       default: 0,     null: false
    t.string   "total_currency",    default: "USD", null: false
    t.string   "payment_method"
    t.index ["account_id"], name: "index_payments_on_account_id", using: :btree
    t.index ["user_id"], name: "index_payments_on_user_id", using: :btree
  end

  create_table "plan_enrollments", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.integer  "pricing_plan_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "price_cents",     default: 0,     null: false
    t.string   "price_currency",  default: "USD", null: false
    t.string   "currency"
    t.index ["account_id"], name: "index_plan_enrollments_on_account_id", using: :btree
    t.index ["pricing_plan_id"], name: "index_plan_enrollments_on_pricing_plan_id", using: :btree
    t.index ["user_id"], name: "index_plan_enrollments_on_user_id", using: :btree
  end

  create_table "pricing_plans", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "name"
    t.bigint   "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "frequency"
    t.string   "frequency_unit"
    t.string   "currency"
    t.boolean  "private_plan",   default: false, null: false
    t.integer  "hours_per_week"
    t.integer  "hours_per_day"
    t.string   "code"
    t.index ["account_id"], name: "index_pricing_plans_on_account_id", using: :btree
  end

  create_table "registration_forms", force: :cascade do |t|
    t.integer  "account_id"
    t.boolean  "custom_form", default: false, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["account_id"], name: "index_registration_forms_on_account_id", using: :btree
  end

  create_table "registration_informations", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.date     "starting_date"
    t.integer  "sign_up_period"
    t.integer  "pricing_plan_id"
    t.text     "current_level_description"
    t.text     "how_did_you_hear_about_us"
    t.text     "why_did_you_choose_us"
    t.text     "anything_else"
    t.text     "housing_information"
    t.integer  "coaching_sessions_per_week"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["account_id"], name: "index_registration_informations_on_account_id", using: :btree
    t.index ["pricing_plan_id"], name: "index_registration_informations_on_pricing_plan_id", using: :btree
    t.index ["user_id"], name: "index_registration_informations_on_user_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "account_id"
    t.index ["account_id"], name: "index_roles_on_account_id", using: :btree
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id", using: :btree
  end

  create_table "rooms", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_rooms_on_account_id", using: :btree
  end

  create_table "start_end_times", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "timeable_type"
    t.integer  "timeable_id"
    t.datetime "start_on"
    t.datetime "end_on"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["account_id"], name: "index_start_end_times_on_account_id", using: :btree
    t.index ["timeable_type", "timeable_id"], name: "index_start_end_times_on_timeable_type_and_timeable_id", using: :btree
  end

  create_table "subscription_plans", force: :cascade do |t|
    t.string   "name"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
    t.string   "stripe_plan_id"
    t.string   "currency"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "terms_of_service_acceptances", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.integer  "terms_of_service_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["account_id"], name: "index_terms_of_service_acceptances_on_account_id", using: :btree
    t.index ["terms_of_service_id"], name: "index_terms_of_service_acceptances_on_terms_of_service_id", using: :btree
    t.index ["user_id"], name: "index_terms_of_service_acceptances_on_user_id", using: :btree
  end

  create_table "terms_of_services", force: :cascade do |t|
    t.integer  "account_id"
    t.text     "body"
    t.boolean  "default",    default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "name"
    t.index ["account_id"], name: "index_terms_of_services_on_account_id", using: :btree
  end

  create_table "todos", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.integer  "requester_id"
    t.text     "body"
    t.boolean  "complete",     default: false, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.datetime "completed_on"
    t.index ["account_id"], name: "index_todos_on_account_id", using: :btree
    t.index ["requester_id"], name: "index_todos_on_requester_id", using: :btree
    t.index ["user_id"], name: "index_todos_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                    default: "",    null: false
    t.string   "encrypted_password",       default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "account_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "full_name"
    t.text     "about"
    t.string   "phone"
    t.boolean  "temporary",                default: false, null: false
    t.boolean  "schedulable",              default: true,  null: false
    t.boolean  "available_in_morning",     default: true,  null: false
    t.boolean  "available_in_afternoon",   default: true,  null: false
    t.boolean  "active",                   default: true,  null: false
    t.string   "country"
    t.string   "gender"
    t.string   "passport_number"
    t.date     "birth_date"
    t.string   "emergency_contact_name"
    t.string   "emergency_contact_number"
    t.index ["account_id"], name: "index_users_on_account_id", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id", using: :btree
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
    t.index ["user_id"], name: "index_users_roles_on_user_id", using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  end

  add_foreign_key "account_subscriptions", "accounts"
  add_foreign_key "account_subscriptions", "subscription_plans"
  add_foreign_key "class_attendances", "accounts"
  add_foreign_key "class_attendances", "class_schedules"
  add_foreign_key "class_attendances", "class_sessions"
  add_foreign_key "class_attendances", "users"
  add_foreign_key "class_schedules", "accounts"
  add_foreign_key "class_sessions", "accounts"
  add_foreign_key "class_sessions", "class_schedules"
  add_foreign_key "class_sessions", "rooms"
  add_foreign_key "class_sessions", "users"
  add_foreign_key "event_attendances", "accounts"
  add_foreign_key "event_attendances", "event_sessions"
  add_foreign_key "event_attendances", "events"
  add_foreign_key "event_attendances", "users"
  add_foreign_key "event_sessions", "accounts"
  add_foreign_key "event_sessions", "events"
  add_foreign_key "events", "accounts"
  add_foreign_key "housing_occupancies", "accounts"
  add_foreign_key "housing_occupancies", "housings"
  add_foreign_key "housing_occupancies", "users"
  add_foreign_key "housings", "accounts"
  add_foreign_key "integrations", "accounts"
  add_foreign_key "level_memberships", "accounts"
  add_foreign_key "level_memberships", "levels"
  add_foreign_key "level_memberships", "users"
  add_foreign_key "levels", "accounts"
  add_foreign_key "misc_payment_items", "accounts"
  add_foreign_key "misc_payment_items", "users"
  add_foreign_key "payment_items", "accounts"
  add_foreign_key "payment_items", "payments"
  add_foreign_key "payment_items", "users"
  add_foreign_key "payments", "accounts"
  add_foreign_key "payments", "users"
  add_foreign_key "plan_enrollments", "accounts"
  add_foreign_key "plan_enrollments", "pricing_plans"
  add_foreign_key "plan_enrollments", "users"
  add_foreign_key "pricing_plans", "accounts"
  add_foreign_key "registration_forms", "accounts"
  add_foreign_key "registration_informations", "accounts"
  add_foreign_key "registration_informations", "pricing_plans"
  add_foreign_key "registration_informations", "users"
  add_foreign_key "roles", "accounts"
  add_foreign_key "rooms", "accounts"
  add_foreign_key "start_end_times", "accounts"
  add_foreign_key "terms_of_service_acceptances", "accounts"
  add_foreign_key "terms_of_service_acceptances", "terms_of_services"
  add_foreign_key "terms_of_service_acceptances", "users"
  add_foreign_key "terms_of_services", "accounts"
  add_foreign_key "todos", "accounts"
  add_foreign_key "todos", "users"
  add_foreign_key "users", "accounts"
end
