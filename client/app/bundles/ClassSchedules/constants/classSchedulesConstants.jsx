import mirrorCreator from 'mirror-creator';

const actionTypes = mirrorCreator([
  'NEW_FORM',
  'GET_ROOMS',
  'EDIT_FORM',
  'CANCEL_FORM',
  'TIME_SELECTED',
  'SELECT_SESSION',
  'FILTER_STUDENTS',
  'SESSION_ERRORED',
  'PERSON_SELECTION',
  'SESSION_SUBMITTED',
  'SESSION_IS_DELETED',
  'TOGGLE_STUDENTS_SORTING',
]);

export default actionTypes;
