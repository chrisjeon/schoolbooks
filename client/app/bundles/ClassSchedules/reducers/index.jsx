import classSchedulesReducer from './classSchedulesReducer';
import { $$initialState as $$classSchedulesState } from './classSchedulesReducer';

export default {
  $$classSchedulesStore: classSchedulesReducer,
};

export const initialStates = {
  $$classSchedulesState,
};
