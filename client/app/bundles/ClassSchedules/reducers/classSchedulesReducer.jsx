import Immutable from 'immutable';

import prunedSet from '../helpers/prunedSet';
import actionTypes from '../constants/classSchedulesConstants';
import sessionsWithBlanks from '../helpers/sessionsWithBlanks';

export const $$initialState = Immutable.fromJS({
  rooms: [],
  selectedSessionPath: '',
  sessionFormVisible: false,
});

const $$initialClassSessionForm = Immutable.fromJS({
  sortedField: '',
  sortedDirection: false,
  studentsFilterLevelId: 0,
  studentsFilterNameQuery: '',
});

function withErrorsHtml(user) {
  let newErrors = new Set();

  const adder = e => newErrors.add(e);

  user.clientErrors.forEach(adder);
  user.backendErrors.forEach(adder);

  return {...user, errorsHtml: [...newErrors].join('<br>')};
}

const user = {
  errorsHtml: '',
  clientErrors: new Set(),
  backendErrors: new Set(),
}

function availabilityLable(moring, afternoon) {
  if (moring && afternoon) return('Full Day');
  if (moring) return('Morning');
  if (afternoon) return('Afternoon');

  return('Not Available');
};

function parsedStudents(payload, selectedIds = []) {
  return payload.map(student => {
    const levels = student.levels.map(level => level.name);

    return({
      ...user,
      ...student,
      levels,
      selected: selectedIds.indexOf(student.id) >= 0,
      pricingPlan: student.pricingPlan && student.pricingPlan.name || '',
      joinedLevels: levels.sort().join(', '),
      availability: availabilityLable(student.availableInMorning, student.availableInAfternoon),
    });
  });
};

function parsedTeachers(payload, selectedId = 0) {
  return payload.map(teacher => ({
    ...user,
    ...teacher,
    levels: teacher.levels.map(level => level.name),
    selected: teacher.id === selectedId,
    availability: availabilityLable(teacher.availableInMorning, teacher.availableInAfternoon),
  }));
};

export default function classSchedulesReducer($$state = $$initialState, action) {
  const { id, value, data, type, kind, endOn, roomId, startOn, personType } = action;

  switch (type) {
    case actionTypes.NEW_FORM: {
      var studentLevels = {};

      data.students.users.forEach(
        student => student.levels.forEach(
          level => studentLevels[level.id] = level.name
        )
      )

      const $$newClassSessionForm = $$initialClassSessionForm.withMutations((map) => {
        map.set('endOn', endOn);
        map.set('roomId', roomId);
        map.set('startOn', startOn);
        map.set('students', parsedStudents(data.students.users));
        map.set('teachers', parsedTeachers(data.teachers.users));
        map.set('studentLevels', studentLevels);
      });

      return $$state.withMutations((map) => {
        map.set('classSessionForm', $$newClassSessionForm);
        map.set('sessionFormVisible', true);
      });
    }

    case actionTypes.EDIT_FORM: {
      var studentLevels = {};

      data.students.users.forEach(
        student => student.levels.forEach(
          level => studentLevels[level.id] = level.name
        )
      )

      const $$newClassSessionForm = $$initialClassSessionForm.withMutations((map) => {
        map.set('endOn', data.session.endOn);
        map.set('roomId', data.session.roomId);
        map.set('startOn', data.session.startOn);
        map.set('students', parsedStudents(data.students.users, data.session.studentIds));
        map.set('teachers', parsedTeachers(data.teachers.users, data.session.teacherId));
        map.set('studentLevels', studentLevels);
        map.set('edit?', true);
      });

      return $$state.withMutations((map) => {
        map.set('classSessionForm', $$newClassSessionForm);
        map.set('sessionFormVisible', true);
      });
    }

    case actionTypes.CANCEL_FORM: {
      return $$state.set('sessionFormVisible', !$$state.get('sessionFormVisible'));
    }

    case actionTypes.SELECT_SESSION: {
      return $$state.set('selectedSessionPath', data.path);
    }

    case actionTypes.FILTER_STUDENTS: {
      return $$state.set(
        'classSessionForm',
        $$state.get('classSessionForm').withMutations((map) => {
          map.set('students', parsedStudents(data.students.users));
          map.set('sortedField', '');
          map.set('studentsFilterLevelId', data.levelId);
          map.set('studentsFilterNameQuery', data.nameQuery);
        })
      );
    }

    case actionTypes.TOGGLE_STUDENTS_SORTING: {
      return $$state.set(
        'classSessionForm',
        $$state.get('classSessionForm').withMutations((map) => {
          const direction = data.field === map.get('sortedField') ? !map.get('sortedDirection') : true;

          map.set('sortedField', data.field);
          map.set('sortedDirection', direction);
          map.set(
            'students',
            map
              .get('students')
              .sort((a, b) => {
                if(a[data.field] < b[data.field]) return direction ? -1 : 1;
                if(a[data.field] > b[data.field]) return direction ? 1 : -1;

                return 0;
              })
          );
        })
      );
    }

    case actionTypes.GET_ROOMS: {
      return $$state.set(
        'rooms',
        Immutable.fromJS(
          data.map(
            (room) => ({
              ...room,
              classSessions: sessionsWithBlanks(room.classSessions)
            })
          )
        )
      );
    }

    case actionTypes.SESSION_SUBMITTED: {
      const roomId = $$state.getIn(['classSessionForm', 'roomId']);

      return $$state.withMutations((map) => {
        map.set('sessionFormVisible', false);
        map.set(
          'rooms',
          Immutable.fromJS(
            map
              .get('rooms')
              .toJS()
              .map((room) => {
                if (room.id !== roomId) return room;

                return ({
                  ...room,
                  classSessions: sessionsWithBlanks(
                    room
                      .classSessions
                      .filter(session => session.presence && session.path !== data.path)
                      .concat(data)
                      .sort((a, b) => {
                        if(a.startOn < b.startOn) return -1;
                        if(a.startOn > b.startOn) return 1;
                        return 0;
                      })
                  ),
                })
              })
          ),
        )
      });
    }

    case actionTypes.SESSION_IS_DELETED: {
      return $$state.withMutations((map) => {
        map.set('selectedSessionPath', '');
        map.set(
          'rooms',
          Immutable.fromJS(
            map
              .get('rooms')
              .toJS()
              .map((room) => ({
                ...room,
                classSessions: sessionsWithBlanks(
                  room
                    .classSessions
                    .filter(session => session.presence && session.path !== data.path)
                ),
              }))
          ),
        )
      });
    }

    case actionTypes.TIME_SELECTED: {
      const $$classSessionForm = $$state.get('classSessionForm');

      return $$state.set('classSessionForm', $$classSessionForm.set(kind, value));
    }

    case actionTypes.SESSION_ERRORED: {
      return $$state.set(
        'classSessionForm',
        $$state.get('classSessionForm').withMutations((map) => {
          var studentErrorsObject = {};
          const studentErrors = data.errors['class_attendances.user'];

          if (studentErrors) studentErrors
            .forEach(e => {
              if (e.type === 'availability') data.errors.studentsAvailability = true;

              studentErrorsObject[e.student_id] = (studentErrorsObject[e.student_id] || new Set()).add(e.text);
            });

          map.set('errors', data.errors);

          map.set(
            'students',
            map.get('students').map(student =>
              withErrorsHtml(
                studentErrorsObject[student.id] && studentErrorsObject[student.id].size
                  ? {...student, backendErrors: studentErrorsObject[student.id]}
                  : {...student, backendErrors: new Set()}
              )
            )
          );

          if (data.errors.teacher) {
            map.set(
              'teachers',
              map.get('teachers').map(teacher =>
                withErrorsHtml(
                  teacher.selected
                    ? {...teacher, backendErrors: new Set(data.errors.teacher)}
                    : {...teacher, backendErrors: new Set()}
                )
              )
            );
          }
        })
      );
    }

    case actionTypes.PERSON_SELECTION: {
      return $$state.set(
        'classSessionForm',
        $$state.get('classSessionForm').withMutations((map) => {
          if (personType === 'students') {
            map.set(
              'students',
              map.get('students').map(
                student => student.id === id ? {...student, selected: value} : student
              )
            );
          } else {
            map.set(
              'teachers',
              map.get('teachers').map(
                teacher => {
                  if (teacher.id === id) {
                    var mapper;
                    var levelMismatch = 'This student should not be assigned to this teacher';

                    if (value && teacher.levels.length) {
                      mapper = student =>
                        withErrorsHtml(
                          !teacher.levels.filter(level => student.levels.indexOf(level) >= 0).length ||
                          !student.levels.length
                            ? {...student, clientErrors: new Set(student.clientErrors).add(levelMismatch)}
                            : {...student, clientErrors: prunedSet(student.clientErrors, levelMismatch)}
                        );
                    } else {
                      mapper = student => withErrorsHtml(
                        {...student, clientErrors: prunedSet(student.clientErrors, levelMismatch)}
                      );
                    }

                    map.set('students', map.get('students').map(mapper));

                    return {...teacher, selected: value}
                  } else {
                    return {...teacher, selected: false}
                  }
                }
              )
            );
          }
        })
      );
    }

    default:
      return $$state;
  }
}
