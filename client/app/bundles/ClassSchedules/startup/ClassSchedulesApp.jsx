import React from 'react';
import ReactOnRails from 'react-on-rails';
import { Provider } from 'react-redux';

import createStore from '../store/classSchedulesStore';
import ClassSchedules from '../containers/ClassSchedules';

const ClassSchedulesApp = (props, _railsContext) => {
  const store = createStore(props);
  const reactComponent = (
    <Provider store={store}>
      <ClassSchedules />
    </Provider>
  );
  return reactComponent;
};

ReactOnRails.register({ ClassSchedulesApp });
