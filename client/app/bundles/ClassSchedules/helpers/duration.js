function toMinutes(hhmm) {
  const splitted = hhmm.split(':');

  return splitted[0]*60 + + splitted[1];
}

export default function duration(start, end) {
  return toMinutes(end) - toMinutes(start);
}
