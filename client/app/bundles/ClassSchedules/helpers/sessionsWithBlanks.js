export default function sessionsWithBlanks(classSessions) {
  var startTime = '09:00';

  const endOfTheDay = '19:00'

  // add full blank session if there are no sessions
  if (!classSessions.length) {
    return [{endOn: endOfTheDay, startOn: startTime, presence: false}];
  }

  var newClassSessions = [];

  // add sessions with blanks in front
  classSessions.forEach((session) => {
    if (session.startOn !== startTime) {
      const endTime = session.startOn;

      newClassSessions.push({endOn: endTime, startOn: startTime, presence: false});

      startTime = endTime;
    }

    newClassSessions.push(session);

    startTime = session.endOn;
  });

  // add last blank session if last end time is before end of the day
  const lastEndTime = newClassSessions[newClassSessions.length - 1].endOn;

  if (lastEndTime !== endOfTheDay) {
    newClassSessions.push({endOn: endOfTheDay, startOn: lastEndTime, presence: false});
  }

  return newClassSessions;
}
