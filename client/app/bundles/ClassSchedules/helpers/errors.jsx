import React from 'react';

export default function errors(array) {
  return array.map((error, i) => <div key={i} className="error">{error}</div>);
}
