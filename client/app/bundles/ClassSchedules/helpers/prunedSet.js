export default function prunedSet(oldSet, element) {
  let newSet = new Set(oldSet);

  newSet.delete(element);

  return newSet;
}
