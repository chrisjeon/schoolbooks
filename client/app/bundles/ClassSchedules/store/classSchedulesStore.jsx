import { compose, createStore, applyMiddleware, combineReducers } from 'redux';

// https://github.com/gaearon/redux-thunk and http://redux.js.org/docs/advanced/AsyncActions.html
import thunkMiddleware from 'redux-thunk';

import reducers from '../reducers';
import { initialStates } from '../reducers';

export default props => {
  const { $$classSchedulesState } = initialStates;

  const {
    accountId,
    roomsPath,
    exportPath,
    rangeLabel,
    studentsPath,
    teachersPath,
    newSessionPath,
    rangeShortLabel,
    classScheduleId,
  } = props;

  // Redux expects to initialize the store using an Object, not an Immutable.Map
  const initialState = {
    $$classSchedulesStore: $$classSchedulesState.merge({
      accountId,
      roomsPath,
      exportPath,
      rangeLabel,
      studentsPath,
      teachersPath,
      newSessionPath,
      rangeShortLabel,
      classScheduleId,
    }),
  };

  const reducer = combineReducers(reducers);
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(reducer, initialState, composeEnhancers(
    applyMiddleware(thunkMiddleware)
  ));

  return store;
};
