import actionTypes from '../constants/classSchedulesConstants';

import { deserializeClassSession } from '../serializers/classSessionSerializer';
import { errorNotification } from '../../../helpers/notifications';

function showWizardErrors(errors) {
  const title = 'Validation error';
  const timoutStep = 250;

  const endOnErrors = errors['start_end_time.end_on'];
  const startOnErrors = errors['start_end_time.start_on'];
  const classAttendancesUserErrors = errors['class_attendances.user'];

  var currentTimeout = 0 - timoutStep;

  if (classAttendancesUserErrors) {
    classAttendancesUserErrors.forEach(error =>
      setTimeout(() => errorNotification(title, error.text), currentTimeout += timoutStep)
    );
  };

  if (endOnErrors) {
    endOnErrors.forEach(error =>
      setTimeout(() => errorNotification(title, 'End date ' + error), currentTimeout += timoutStep)
    );
  };

  if (startOnErrors) {
    startOnErrors.forEach(error =>
      setTimeout(() => errorNotification(title, 'Start date ' + error), currentTimeout += timoutStep)
    );
  };

  ['room', 'teacher', 'students'].forEach(errorType => {
    if (errors[errorType]) {
      errors[errorType].forEach(error =>
        setTimeout(() => errorNotification(title, error), currentTimeout += timoutStep)
      );
    };
  });
}

export function cancelForm() {
  return {
    type: actionTypes.CANCEL_FORM,
  };
}

export function selectSession(path) {
  return {
    data: { path },
    type: actionTypes.SELECT_SESSION,
  };
}

export function toggleStudentsSorting(field) {
  return {
    data: { field },
    type: actionTypes.TOGGLE_STUDENTS_SORTING,
  };
}

export function selectTime(kind, value) {
  return {
    kind,
    value,
    type: actionTypes.TIME_SELECTED,
  };
}

export function deleteSession(selectedSessionPath) {
  return dispatch =>
    $.ajax({
      url: selectedSessionPath,
      method: 'DELETE',
    }).done((data) => {
      if (!data.errors) {
        dispatch({
          type: actionTypes.SESSION_IS_DELETED,
          data: { path: selectedSessionPath },
        });

        swal('Deleted!', 'The class session has been deleted.', 'success');
      }
    });
}

export function personSelection(personType, id, value) {
  return {
    id,
    value,
    personType,
    type: actionTypes.PERSON_SELECTION,
  };
}

export function filterStudents(studentsPath, levelId, nameQuery) {
  return dispatch =>
    $.get(
      studentsPath + '&' + $.param({levelId, nameQuery})
    ).done(students => {
      dispatch({
        type: actionTypes.FILTER_STUDENTS,
        data: {
          levelId: Number(levelId),
          students,
          nameQuery,
        },
      });
    });
}

export function newForm(studentsPath, teachersPath, roomId, startOn, endOn) {
  return dispatch =>
    $.when(
      $.get(studentsPath),
      $.get(teachersPath),
    ).done((studentsResponce, teachersResponce) => {
      dispatch({
        endOn,
        roomId,
        startOn,
        type: actionTypes.NEW_FORM,
        data: {
          students: studentsResponce[0],
          teachers: teachersResponce[0],
        },
      });
    });
}

export function editForm(studentsPath, teachersPath, sessionPath) {
  return dispatch =>
    $.when(
      $.get(studentsPath),
      $.get(teachersPath),
      $.get(sessionPath),
    ).done((studentsResponce, teachersResponce, sessionResponce) => {
      dispatch({
        type: actionTypes.EDIT_FORM,
        data: {
          session: deserializeClassSession(sessionResponce[0].classSession),
          students: studentsResponce[0],
          teachers: teachersResponce[0],
        },
      });
    });
}

export function submitForm(accountId, newSessionPath, classScheduleId, $$classSessionForm, selectedSessionPath) {
  const students = $$classSessionForm.get('students').filter(student => student.selected);
  const selectedTeacher = $$classSessionForm.get('teachers').find(teacher => teacher.selected);

  const endpoint = $$classSessionForm.get('edit?')
    ? { url: selectedSessionPath, method: 'PUT' }
    : { url: newSessionPath, method: 'POST' }

  return dispatch =>
    $.ajax({
      url: endpoint.url,
      method: endpoint.method,
      data: {
        end_on: $$classSessionForm.get('endOn'),
        room_id: $$classSessionForm.get('roomId'),
        user_id: selectedTeacher && selectedTeacher.id,
        start_on: $$classSessionForm.get('startOn'),
        account_id: accountId,
        class_schedule_id: classScheduleId,
        class_attendances_attributes: students.map(student => ({
          user_id: student.id,
          account_id: accountId,
          class_schedule_id: classScheduleId,
        })),
      },
    }).done(data => {
      if (data.errors) {
        dispatch({
          data,
          type: actionTypes.SESSION_ERRORED,
        });

        showWizardErrors(data.errors);
      } else {
        dispatch({
          type: actionTypes.SESSION_SUBMITTED,
          data: deserializeClassSession(data.classSession),
        });
      }
    });
}

export function getRooms(roomsPath) {
  return dispatch =>
    $.get(
      roomsPath
    ).done(payload => {
      var data = payload.rooms.map(room => ({
        ...room,
        classSessions: room.classSessions.map((session) => deserializeClassSession(session))
      }));

      dispatch({
        data,
        type: actionTypes.GET_ROOMS,
      });
    });
}

