import React, { PropTypes } from 'react';

export default class SessionConfirmator extends React.Component {
  static propTypes = {
    cancelForm: PropTypes.func.isRequired,
    submitClassSession: PropTypes.func.isRequired,
  };

  render() {
    return (
      <div className="panel panel-default panel-confirm">
        <div className="panel-body text-center p-t-0">
          <p>Schedule this class?</p>

          <p>
            <a
              onClick={this.props.submitClassSession}
              className="btn btn-lg btn-success text-uppercase w-lg"
            >
              Confirm
            </a>
          </p>

          <p>
            <a onClick={this.props.cancelForm}>Cancel</a>
          </p>
        </div>
      </div>
    );
  }
}
