import React, { PropTypes } from 'react';

import PersonToRemove from './PersonToRemove';

const SessionPeopleRemover = ({ students, teachers, personSelection }) => {
  const personToRemove = (person, key, peopleType) =>
    <PersonToRemove
      key={key}
      fullName={person.fullName}
      errorsHtml={person.errorsHtml}
      removePerson={() => personSelection(peopleType, person.id, false)}
    />

  const studentsToRemove = students.map((person, key) => personToRemove(person, key, 'students'));
  const teachersToRemove = teachers.map((person, key) => personToRemove(person, key, 'teachers'));

  return (
    <div className="panel panel-default panel-selected-students">
      <div className="panel-body p-t-0">
        <div className="remove-students">
          <h4>Selected students</h4>
          <div className="remove-students-content">
            {studentsToRemove}
          </div>
        </div>

        <div className="remove-teachers">
          <h4>Selected teachers</h4>
          <div className="remove-teachers-content">
            {teachersToRemove}
          </div>
        </div>
      </div>
    </div>
  );
}

SessionPeopleRemover.propTypes = {
  students: PropTypes.array.isRequired,
  teachers: PropTypes.array.isRequired,
  personSelection: PropTypes.func.isRequired,
};

export default SessionPeopleRemover;
