import React, { PropTypes } from 'react';

const StudentToSelect = ({
  id,
  levels,
  selected,
  fullName,
  hoursLeft,
  errorsHtml,
  pricingPlan,
  availability,
  joinedLevels,
  personSelection,
}) => {
  const toggleStudent = (event) => personSelection('students', id, event.target.checked);

  return (
    <tr data-original-title={errorsHtml} data-toggle="tooltip">
      <td>
        <label className="cr-styled">
          <input type="checkbox" checked={selected} onChange={toggleStudent} />
          <i className="fa" />
        </label>
      </td>
      <td className={errorsHtml.length ? 'error' : ''}>
        {fullName}
      </td>
      <td>{joinedLevels}</td>
      <td>{pricingPlan}</td>
      <td>{availability}</td>
      <td>{hoursLeft} hours</td>
    </tr>
  );
};

StudentToSelect.propTypes = {
  id: PropTypes.number.isRequired,
  levels: PropTypes.array.isRequired,
  selected: PropTypes.bool.isRequired,
  fullName: PropTypes.string.isRequired,
  hoursLeft: PropTypes.number.isRequired,
  errorsHtml: PropTypes.string.isRequired,
  pricingPlan: PropTypes.string,
  joinedLevels: PropTypes.string.isRequired,
  availability: PropTypes.string.isRequired,
  personSelection: PropTypes.func.isRequired,
};

export default StudentToSelect;
