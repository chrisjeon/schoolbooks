import React, { PropTypes } from 'react';

const TeacherToSelect = ({ id, levels, selected, fullName, errorsHtml, availability, personSelection }) => {
  const levelsColumn = levels.length ? 'Levels: ' + levels.join(', ') : '';
  const toggleTeacher = (event) => personSelection('teachers', id, event.target.checked);

  return (
    <tr data-original-title={errorsHtml} data-toggle="tooltip">
      <td>
        <label className="cr-styled">
          <input type="checkbox" checked={selected} onChange={toggleTeacher} />
          <i className="fa" />
        </label>
      </td>
      <td className={errorsHtml.length ? 'error' : ''}>{fullName}</td>
      <td>{levelsColumn}</td>
      <td>{availability}</td>
    </tr>
  );
}

TeacherToSelect.propTypes = {
  id: PropTypes.number.isRequired,
  levels: PropTypes.array.isRequired,
  selected: PropTypes.bool.isRequired,
  fullName: PropTypes.string.isRequired,
  errorsHtml: PropTypes.string.isRequired,
  availability: PropTypes.string.isRequired,
  personSelection: PropTypes.func.isRequired,
};

export default TeacherToSelect;
