import React, { PropTypes } from 'react';
import {Tooltip} from 'pui-react-tooltip';
import {OverlayTrigger} from 'pui-react-overlay-trigger';

import duration from '../helpers/duration';

const ClassSesison = ({ path, endOn, startOn, teacherName, studentNames, selectSession, teacherLevels, selectedSessionPath }) => {
  const students = studentNames.map((name, i) => <li key={i}>{name}</li>);
  const className = 'event-wrapper overlay-trigger' + (path === selectedSessionPath ? ' active' : '');

  return(
    <OverlayTrigger placement="right" overlay={<Tooltip > <ul className="event-body-tooltip">{students}</ul></Tooltip>}>
      <div className={className} tabIndex="0" onClick={() => selectSession(path)}>
        <div className={'event minutes-' + duration(startOn, endOn)}>
          <div className="event-header">
            <div className="name">
              {teacherName}
            </div>

            <div className="level">
              level {teacherLevels.join(', ')}
            </div>
          </div>

          <ul className="event-body">
            {students}
          </ul>
        </div>
      </div>
    </OverlayTrigger>
  );
};

ClassSesison.propTypes = {
  path: PropTypes.string.isRequired,
  endOn: PropTypes.string.isRequired,
  startOn: PropTypes.string.isRequired,
  teacherName: PropTypes.string.isRequired,
  studentNames: PropTypes.array.isRequired,
  selectSession: PropTypes.func.isRequired,
  teacherLevels: PropTypes.array.isRequired,
  selectedSessionPath: PropTypes.string.isRequired,
};

export default ClassSesison;
