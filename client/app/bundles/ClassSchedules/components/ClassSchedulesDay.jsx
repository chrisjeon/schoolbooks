import React, { PropTypes } from 'react';

import Immutable from 'immutable';

import ClassRoom from './ClassRoom';
import ScheduleRuler from './ScheduleRuler';

class ClassSchedulesDay extends React.Component {
  static propTypes = {
    $$rooms: PropTypes.instanceOf(Immutable.List).isRequired,
    exportPath: PropTypes.string.isRequired,
    rangeLabel: PropTypes.string.isRequired,
    selectSession: PropTypes.func.isRequired,
    editTheSession: PropTypes.func.isRequired,
    deleteTheSession: PropTypes.func.isRequired,
    newFormWithPathes: PropTypes.func.isRequired,
    selectedSessionPath: PropTypes.string.isRequired,
  };

  componentDidMount() {
    window.initClassSchedulesExportListener();
  };

  render() {
    const {
      $$rooms,
      exportPath,
      rangeLabel,
      selectSession,
      editTheSession,
      deleteTheSession,
      newFormWithPathes,
      selectedSessionPath,
    } = this.props;

    const classRooms = $$rooms.toJS().map(
      (room, i) => <ClassRoom {...{...room, key: i, selectSession, newFormWithPathes, selectedSessionPath}} />
    );

    const actionButtons = selectedSessionPath
      ? [
          <div className="btn-group" key="Delete">
            <button className="btn btn-info" onClick={deleteTheSession}>Delete</button>
          </div>,
          <div className="btn-group" key="Edit">
            <button className="btn btn-danger" onClick={editTheSession}>Edit</button>
          </div>
        ]
      : false

    return (
      <div>
        <div className="page-title">
          <h3 className="title">Show schedule</h3>
        </div>

        <div className="row">
          <div className="col-xs-12">
            <div className="panel panel-default">
              <div className="panel-body">
                <div className="row">
                  <div className="col-xs-12 clearfix m-b-15">
                    <h4 className="pull-left text-center">
                      {rangeLabel}
                    </h4>

                    <div className="btn-toolbar pull-right" role="toolbar">
                      <div className="btn-group">
                        <button className="btn btn-success export-class-schedule" data-path={exportPath}>Export</button>
                      </div>

                      {actionButtons}
                    </div>
                  </div>

                  <div className="col-xs-12">
                    <div className="schedule-table">
                      <div className="schedule-table-body">
                        <ScheduleRuler />

                        <div className="schedule-table-content">
                          <div className="schedule-table-content-body">
                            {classRooms}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default ClassSchedulesDay;
