import React, { PropTypes } from 'react';

import duration from '../helpers/duration';

const EmptySession = ({ endOn, startOn, newFormWithRoomId }) => {
  const showNewForm = () => newFormWithRoomId(startOn, endOn);

  return(
    <div className="event-wrapper" onClick={showNewForm} >
      <div className={'add-event minutes-' + duration(startOn, endOn)}>
        <i className="ion-plus"></i>
      </div>
    </div>
  );
};

EmptySession.propTypes = {
  endOn: PropTypes.string.isRequired,
  startOn: PropTypes.string.isRequired,
  newFormWithRoomId: PropTypes.func.isRequired,
};

export default EmptySession;
