import React, { PropTypes } from 'react';

import errors from '../helpers/errors';

export default class TimeSelector extends React.Component {
  static propTypes = {
    selectTime: PropTypes.func.isRequired,
    roomErrors: PropTypes.array.isRequired,
  };

  componentDidMount() {
    const options = {
      min: '9:00',
      max: '19:00',
      format: 'HH:i',
    };

    $('#start-picker').pickatime({
      ...options,
      onSet: (e) => this.props.selectTime('startOn', this.minutestToHours(e.select)),
    });

    $('#end-picker').pickatime({
      ...options,
      onSet: (e) => this.props.selectTime('endOn', this.minutestToHours(e.select)),
    });
  }

  minutestToHours(minutes) {
    return(Math.trunc(minutes / 60) + ':' + minutes % 60);
  }

  render() {
    return (
      <div className="panel panel-default time-schedule-block">
        <div className="panel-body text-center p-t-0">
          <p className="p-b-10">Schedule this class?</p>
          <div className="row">
            <div className="col-md-6">
              <div className="input-group m-b-15">
                <div className="bootstrap-timepicker">
                  <input
                    id="start-picker"
                    type="text"
                    className="form-control"
                    defaultValue={this.props.startOn}
                  />
                </div>

                <span className="input-group-addon">
                  <i className="ion-clock" />
                </span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="input-group m-b-15">
                <div className="bootstrap-timepicker">
                  <input
                    id="end-picker"
                    type="text"
                    className="form-control"
                    defaultValue={this.props.endOn}
                  />
                </div>

                <span className="input-group-addon">
                  <i className="ion-clock" />
                </span>
              </div>
            </div>

            <div className="col-md-12">
              <div className="error-wrapper">
                {errors(this.props.roomErrors)}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
