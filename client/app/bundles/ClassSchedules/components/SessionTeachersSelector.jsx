import React, { PropTypes } from 'react';

import errors from '../helpers/errors';
import TeacherToSelect from './TeacherToSelect';

export default class SessionTeachersSelector extends React.Component {
  static propTypes = {
    teachers: PropTypes.array.isRequired,
    teacherErrors: PropTypes.array.isRequired,
    personSelection: PropTypes.func.isRequired,
    timeLocationLabel: PropTypes.string.isRequired,
  };

  render() {
    const { personSelection, teacherErrors, timeLocationLabel } = this.props;

    const teacherToSelect = this.props.teachers.map(
      teacher => <TeacherToSelect {...{...teacher, personSelection, key: teacher.id}} />
    );

    return (
      <div className="panel panel-default teacher-select-block">
        <div className="panel-body p-t-0">
          <h4 className="m-b-15">
            Select the teacher {timeLocationLabel}
          </h4>
          <div className="error-wrapper">
            {errors(teacherErrors)}
          </div>
          <div className="table-select-person-wrapper">
            <table className="table table-select-person teacher">
              <tbody>
                {teacherToSelect}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
