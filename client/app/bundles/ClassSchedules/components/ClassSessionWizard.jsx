import React, { PropTypes } from 'react';
import { Map } from 'immutable';

import TimeSelector from './TimeSelector';
import SessionConfirmator from './SessionConfirmator';
import SessionPeopleRemover from './SessionPeopleRemover';
import SessionStudentsSelector from './SessionStudentsSelector';
import SessionTeachersSelector from './SessionTeachersSelector';

export default class ClassSessionWizard extends React.Component {
  static propTypes = {
    roomName: PropTypes.string.isRequired,
    cancelForm: PropTypes.func.isRequired,
    selectTime: PropTypes.func.isRequired,
    rangeShortLabel: PropTypes.string.isRequired,
    personSelection: PropTypes.func.isRequired,
    $$classSessionForm: PropTypes.instanceOf(Map).isRequired,
    submitClassSession: PropTypes.func.isRequired,
    toggleStudentsSorting: PropTypes.func.isRequired,
    filterStudentsWithPath: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.initTooltips();
  }

  componentDidUpdate() {
    this.initTooltips();
  }

  initTooltips() {
    $('[data-toggle="tooltip"]').tooltip({
      html: true,
      placement: 'auto',
    });
  }

  render() {
    const {
      roomName,
      selectTime,
      cancelForm,
      personSelection,
      rangeShortLabel,
      $$classSessionForm,
      submitClassSession,
      toggleStudentsSorting,
      filterStudentsWithPath,
    } = this.props;

    const endOn = $$classSessionForm.get('endOn');
    const startOn = $$classSessionForm.get('startOn');
    const students = $$classSessionForm.get('students');
    const teachers = $$classSessionForm.get('teachers');
    const sortedField = $$classSessionForm.get('sortedField');
    const studentLevels = $$classSessionForm.get('studentLevels');
    const sortedDirection = $$classSessionForm.get('sortedDirection');
    const studentsFilterLevelId = $$classSessionForm.get('studentsFilterLevelId');
    const studentsFilterNameQuery = $$classSessionForm.get('studentsFilterNameQuery');

    const errors = $$classSessionForm.get('errors');
    const roomErrors = errors && errors.room || [];
    const teacherErrors = errors && errors.teacher || [];

    var studentsErrors = errors && errors.students || [];

    if (errors && errors.studentsAvailability) {
      studentsErrors = [...(new Set(studentsErrors).add('Some students are not available for given time'))]
    }

    const selectedStudents = students.filter(student => student.selected);
    const selectedTeachers = teachers.filter(student => student.selected);

    const timeLocationLabel = 'for ' + rangeShortLabel + ' in ' + roomName +'.';

    return (
      <div>
        <div className="page-title">
          <h3 className="title">Show schedule <small>Match Students to a Teacher</small></h3>
        </div>

        <div className="row">
          <div className="col-md-8">
            <TimeSelector {...{endOn, startOn, roomErrors, selectTime}} />

            <SessionStudentsSelector {
              ...{
                students,
                sortedField,
                studentLevels,
                studentsErrors,
                sortedDirection,
                personSelection,
                timeLocationLabel,
                toggleStudentsSorting,
                studentsFilterLevelId,
                filterStudentsWithPath,
                studentsFilterNameQuery,
              }
            } />

            <SessionTeachersSelector {...{teachers, teacherErrors, personSelection, timeLocationLabel}} />
          </div>

          <div className="col-md-4">
            <SessionPeopleRemover {...{students: selectedStudents, teachers: selectedTeachers, personSelection}} />
            <SessionConfirmator {...{cancelForm, submitClassSession}} />
          </div>
        </div>
      </div>
    );
  }
}
