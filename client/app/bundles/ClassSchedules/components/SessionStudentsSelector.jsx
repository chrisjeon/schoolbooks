import React, { PropTypes } from 'react';

import errors from '../helpers/errors';
import StudentToSelect from './StudentToSelect';
import SortableTableHeaderColumn from './SortableTableHeaderColumn';

export default class SessionStudentsSelector extends React.Component {
  static propTypes = {
    students: PropTypes.array.isRequired,
    sortedField: PropTypes.string.isRequired,
    studentLevels: PropTypes.object.isRequired,
    studentsErrors: PropTypes.array.isRequired,
    sortedDirection: PropTypes.bool.isRequired,
    personSelection: PropTypes.func.isRequired,
    timeLocationLabel: PropTypes.string.isRequired,
    toggleStudentsSorting: PropTypes.func.isRequired,
    studentsFilterLevelId: PropTypes.number.isRequired,
    filterStudentsWithPath: PropTypes.func.isRequired,
    studentsFilterNameQuery: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      delayTimer: null,
    };

    this.filterStudentsByQuery = this.filterStudentsByQuery.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return(
      this.state.timestamp === nextState.timestamp ||
      this.props.sortedField !== nextProps.sortedField ||
      this.props.sortedDirection !== nextProps.sortedDirection
    );
  }

  filterStudentsByQuery(query) {
    clearTimeout(this.state.delayTimer);

    const {filterStudentsWithPath, studentsFilterLevelId} = this.props;
    this.setState({
      timestamp: Date.now(),
      delayTimer: setTimeout(function() {
        filterStudentsWithPath(studentsFilterLevelId, query);
      }, 1000),
    });
  }

  render() {
    const {
      sortedField,
      studentLevels,
      sortedDirection,
      personSelection,
      timeLocationLabel,
      toggleStudentsSorting,
      studentsFilterLevelId,
      filterStudentsWithPath,
      studentsFilterNameQuery,
    } = this.props;

    const studentsToSelect = this.props.students.map(
      student => <StudentToSelect {...{...student, personSelection, key: student.id}} />
    );

    const optionsForLevels = Object.keys(studentLevels).map(
      key =>
        <li key={key}>
          <a onClick={() => filterStudentsWithPath(key, studentsFilterNameQuery)}>{studentLevels[key]}</a>
        </li>
    );

    const levelFilterLabel = studentsFilterLevelId ? studentsFilterLevelId : 'Select a level...'

    return (
      <div className="panel panel-default">
        <div className="panel-body p-t-0">
          <h4 className="m-b-15">
            Select the students {timeLocationLabel}
          </h4>
        </div>

        <div className="filter-block-select-student m-b-15">
          <div>
            <input
              type="text"
              onChange={(e) => this.filterStudentsByQuery(e.target.value)}
              className="form-control filter-style"
              placeholder="Filter by name"
              defaultValue={studentsFilterNameQuery}
            />
          </div>

          <div className="dropdown">
            <button
              className="btn btn-default dropdown-toggle filter-style"
              type="button"
              id="dropdownMenu"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true"
            >
              {levelFilterLabel}
              <span className="caret"></span>
            </button>
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu">
              <li><a onClick={() => filterStudentsWithPath(0, studentsFilterNameQuery)}>&zwnj;</a></li>

              {optionsForLevels}
            </ul>
          </div>
        </div>

        {errors(this.props.studentsErrors)}

        <div className="table-select-person-wrapper">
          <table className="table table-select-person">
            <thead>
              <tr>
                <th></th>
                <th>
                  <SortableTableHeaderColumn {
                    ...{
                      sortedField,
                      sortedDirection,
                      toggleStudentsSorting,
                      label: 'Name',
                      field: 'fullName',
                    }
                  } />
                </th>
                <th>
                  <SortableTableHeaderColumn {
                    ...{
                      sortedField,
                      sortedDirection,
                      toggleStudentsSorting,
                      label: 'Levels',
                      field: 'joinedLevels',
                    }
                  } />
                </th>
                <th>
                  <SortableTableHeaderColumn {
                    ...{
                      sortedField,
                      sortedDirection,
                      toggleStudentsSorting,
                      label: 'Pricing Plan',
                      field: 'pricingPlan',
                    }
                  } />
                </th>
                <th>
                  <SortableTableHeaderColumn {
                    ...{
                      sortedField,
                      sortedDirection,
                      toggleStudentsSorting,
                      label: 'Availability',
                      field: 'availability',
                    }
                  } />
                </th>
                <th>
                  <SortableTableHeaderColumn {
                    ...{
                      sortedField,
                      sortedDirection,
                      toggleStudentsSorting,
                      label: 'To Assign',
                      field: 'hoursLeft',
                    }
                  } />
                </th>
              </tr>
            </thead>
            <tbody>

            {studentsToSelect}
          </tbody>
          </table>
        </div>
      </div>
    );
  }
}
