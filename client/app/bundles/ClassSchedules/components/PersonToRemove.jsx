import React, { PropTypes } from 'react';

const PersonToRemove = ({ fullName, errorsHtml, removePerson }) => (
  <p>
    <span
      className={errorsHtml.length ? 'error' : ''}
      data-toggle="tooltip"
      data-original-title={errorsHtml}
    >
      {fullName}
    </span>
    <a
      onClick={removePerson}
      className="text-uppercase pull-right small-light-link"
    >
      remove
    </a>
  </p>
);

PersonToRemove.propTypes = {
  fullName: PropTypes.string.isRequired,
  errorsHtml: PropTypes.string.isRequired,
  removePerson: PropTypes.func.isRequired,
};

export default PersonToRemove;

