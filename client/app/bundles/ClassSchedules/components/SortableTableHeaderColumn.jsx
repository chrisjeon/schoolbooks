import React, { PropTypes } from 'react';

const SortableTableHeaderColumn = ({ label, field, sortedField, sortedDirection, toggleStudentsSorting }) => (
  <div className="th-wrapper" onClick={() => toggleStudentsSorting(field)}>
    <span>
      {label}
    </span>

    <div className="buttons" >
      <div className={'button-up' + (field === sortedField && !sortedDirection ? ' active' : '')}>
        <i className="ion-arrow-up-b"></i>
      </div>

      <div className={'button-down' + (field === sortedField && sortedDirection ? ' active' : '')}>
        <i className="ion-arrow-down-b"></i>
      </div>
    </div>
  </div>
);

SortableTableHeaderColumn.propTypes = {
  label: PropTypes.string.isRequired,
  field: PropTypes.string.isRequired,
  sortedField: PropTypes.string.isRequired,
  sortedDirection: PropTypes.bool.isRequired,
  toggleStudentsSorting: PropTypes.func.isRequired,
};

export default SortableTableHeaderColumn;
