import React from 'react';

const ScheduleRuler = () => (
  <div className="schedule-table-rule">
    <div><span>9:00</span></div>
    <div><span>9:30</span></div>
    <div><span>10:00</span></div>
    <div><span>10:30</span></div>
    <div><span>11:00</span></div>
    <div><span>11:30</span></div>
    <div><span>12:00</span></div>
    <div><span>12:30</span></div>
    <div><span>13:00</span></div>
    <div><span>13:30</span></div>
    <div><span>14:00</span></div>
    <div><span>14:30</span></div>
    <div><span>15:00</span></div>
    <div><span>15:30</span></div>
    <div><span>16:00</span></div>
    <div><span>16:30</span></div>
    <div><span>17:00</span></div>
    <div><span>17:30</span></div>
    <div><span>18:00</span></div>
    <div><span>18:30</span></div>
    <div><span>19:00</span></div>
  </div>
);

export default ScheduleRuler;
