import React, { PropTypes } from 'react';

import ClassSession from './ClassSession';
import EmptySession from './EmptySession';

const ClassRoom = ({ id, name, classSessions, selectSession, newFormWithPathes, selectedSessionPath }) => {
  const newFormWithRoomId = (startOn, endOn) => newFormWithPathes(id, startOn, endOn);

  const sessions = classSessions.map(
    (session, key) => session.presence
      ? <ClassSession {...{...session, key, selectSession, selectedSessionPath}} />
      : <EmptySession {...{...session, key, newFormWithRoomId}} />
  );

  return(
    <div className="column">
      <div className="column-header">
        <div className="name">{ name }</div>
      </div>

      {sessions}
    </div>
  );
};

ClassRoom.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  classSessions: PropTypes.array.isRequired,
  selectSession: PropTypes.func.isRequired,
  newFormWithPathes: PropTypes.func.isRequired,
  selectedSessionPath: PropTypes.string.isRequired,
};

export default ClassRoom;
