import React, { PropTypes } from 'react';
import ClassSchedulesDay from '../components/ClassSchedulesDay';
import ClassSessionWizard from '../components/ClassSessionWizard';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Immutable from 'immutable';
import * as classSchedulesActionCreators from '../actions/classSchedulesActionCreators';

function select(state) {
  // Which part of the Redux global state does our component want to receive as props?
  // Note the use of `$$` to prefix the property name because the value is of type Immutable.js
  return { $$classSchedulesStore: state.$$classSchedulesStore };
}

class ClassSchedules extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,

    // We prefix all property and variable names pointing to Immutable.js objects with '$$'.
    $$classSchedulesStore: PropTypes.instanceOf(Immutable.Map).isRequired,
  };

  componentWillMount() {
    this.props.dispatch(
      classSchedulesActionCreators.getRooms(
        this.props.$$classSchedulesStore.get(
          'roomsPath'
        )
      )
    );
  }

  newFormWithPathes(roomId, startOn, endOn) {
    return this.props.dispatch(
      classSchedulesActionCreators.newForm(
        this.props.$$classSchedulesStore.get('studentsPath'),
        this.props.$$classSchedulesStore.get('teachersPath'),
        roomId,
        startOn,
        endOn
      )
    );
  }

  editTheSession() {
    return this.props.dispatch(
      classSchedulesActionCreators.editForm(
        this.props.$$classSchedulesStore.get('studentsPath'),
        this.props.$$classSchedulesStore.get('teachersPath'),
        this.props.$$classSchedulesStore.get('selectedSessionPath')
      )
    );
  }

  deleteTheSession() {
    const { dispatch, $$classSchedulesStore } = this.props;

    swal({
      text: 'You will not be able to recover this class session and attendances!',
      type: 'warning',
      title: 'Are you sure?',
      closeOnConfirm: false,
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      confirmButtonColor: '#DD6B55',
    }, function() {
      dispatch(
        classSchedulesActionCreators.deleteSession(
          $$classSchedulesStore.get('selectedSessionPath')
        )
      );
    });
  }

  filterStudentsWithPath(levelId, nameQuery) {
    return this.props.dispatch(
      classSchedulesActionCreators.filterStudents(
        this.props.$$classSchedulesStore.get('studentsPath'),
        levelId,
        nameQuery
      )
    );
  }

  submitClassSession() {
    return this.props.dispatch(
      classSchedulesActionCreators.submitForm(
        this.props.$$classSchedulesStore.get('accountId'),
        this.props.$$classSchedulesStore.get('newSessionPath'),
        this.props.$$classSchedulesStore.get('classScheduleId'),
        this.props.$$classSchedulesStore.get('classSessionForm'),
        this.props.$$classSchedulesStore.get('selectedSessionPath')
      )
    );
  }

  render() {
    const { dispatch, $$classSchedulesStore } = this.props;
    const actions = bindActionCreators(classSchedulesActionCreators, dispatch);
    const { selectTime, cancelForm, selectSession, toggleStudentsSorting, personSelection } = actions;

    const $$rooms = $$classSchedulesStore.get('rooms');
    const exportPath = $$classSchedulesStore.get('exportPath');
    const rangeLabel = $$classSchedulesStore.get('rangeLabel');
    const rangeShortLabel = $$classSchedulesStore.get('rangeShortLabel');
    const $$classSessionForm = $$classSchedulesStore.get('classSessionForm');
    const selectedSessionPath = $$classSchedulesStore.get('selectedSessionPath');

    const editTheSession = this.editTheSession.bind(this);
    const deleteTheSession = this.deleteTheSession.bind(this);
    const newFormWithPathes = this.newFormWithPathes.bind(this);
    const submitClassSession = this.submitClassSession.bind(this);
    const filterStudentsWithPath = this.filterStudentsWithPath.bind(this);

    const room = $$rooms.toJS().find(room => room.id === ($$classSessionForm && $$classSessionForm.get('roomId')));
    const roomName = room && room.name;

    return (
      $$classSchedulesStore.get('sessionFormVisible')
        ? <ClassSessionWizard {
            ...{
              roomName,
              cancelForm,
              selectTime,
              rangeShortLabel,
              personSelection,
              $$classSessionForm,
              submitClassSession,
              toggleStudentsSorting,
              filterStudentsWithPath,
            }
          } />
        : <ClassSchedulesDay {
            ...{
              $$rooms,
              exportPath,
              rangeLabel,
              selectSession,
              editTheSession,
              deleteTheSession,
              newFormWithPathes,
              selectedSessionPath,
            }
          } />
    );
  }
};

// Don't forget to actually use connect!
// Note that we don't export ClassSchedules, but the redux "connected" version of it.
// See https://github.com/reactjs/react-redux/blob/master/docs/api.md#examples
export default connect(select)(ClassSchedules);
