export const deserializeClassSession = (session) => ({
  path: session.path,
  endOn: session.startEndTime.endOn,
  roomId: session.roomId,
  startOn: session.startEndTime.startOn,
  presence: true,
  teacherId: session.teacher.id,
  studentIds: session.students.map(student => student.id).sort(),
  teacherName: session.teacher.fullName,
  studentNames: session.students.map(student => student.fullName).sort(),
  teacherLevels: session.teacher.levels && session.teacher.levels.map(level => level.name).sort() || [],
});
