export const deserializeTodo = todo => ({
  id: todo.id,
  body: todo.body,
  complete: todo.complete,
  selected: false,
});
