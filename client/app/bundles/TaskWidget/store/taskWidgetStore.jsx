import { compose, createStore, applyMiddleware, combineReducers } from 'redux';

// https://github.com/gaearon/redux-thunk and http://redux.js.org/docs/advanced/AsyncActions.html
import thunkMiddleware from 'redux-thunk';

import reducers from '../reducers';
import { initialStates } from '../reducers';

export default props => {
  const { taskWidgetState } = initialStates;

  const {
    todosPath,
  } = props;

  const initialState = {
    taskWidgetStore: {
      ...taskWidgetState,
      todosPath,
    }
  };

  const reducer = combineReducers(reducers);
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(reducer, initialState, composeEnhancers(
    applyMiddleware(thunkMiddleware)
  ));

  return store;
};
