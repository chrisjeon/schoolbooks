import mirrorCreator from 'mirror-creator';

const actionTypes = mirrorCreator([
  'GOT_TODOS',
  'HIDE_WIDGET',
  'TODO_CREATED',
  'TODO_SELECTED',
  'LOADER_ENABLED',
  'LOADER_DISABLED',
  'NEW_ITEM_TEXT_CHANGED',
  'ARCHIVE_SELECTED_TODOS',
]);

export default actionTypes;
