import actionTypes from '../constants/taskWidgetConstants';

import { errorNotification } from '../../../helpers/notifications';
import { deserializeTodo } from '../serializers/todoSerializer';

export function enableLoader() {
  return {
    type: actionTypes.LOADER_ENABLED,
  }
}

export function disableLoader() {
  return {
    type: actionTypes.LOADER_DISABLED,
  }
}

export function getTodos(path) {
  return dispatch => {
    dispatch(enableLoader());

    $.get(
      path
    ).done(response => {
      dispatch({
        data: { todos: response.todos.map(deserializeTodo) },
        type: actionTypes.GOT_TODOS,
      });
    }).always(
      () => dispatch(disableLoader())
    );
  }
}

export function updateNewItemText(newText) {
  return {
    data: { newText },
    type: actionTypes.NEW_ITEM_TEXT_CHANGED
  }
}

export function createNewItem(url, body) {
  return dispatch => {
    dispatch(enableLoader());

    return $.ajax({
      url,
      method: 'POST',
      data: { todo: { body } },
    }).done(response => {
      dispatch({
        data: { todo: deserializeTodo(response.todo) },
        type: actionTypes.TODO_CREATED,
      });
    }).fail(xhr => {
      xhr.responseJSON.errors.forEach(error =>
        errorNotification('Validation error', error)
      );
    }).always(() =>
      dispatch(disableLoader())
    );
  };
}

export function selectItem(id, value) {
  return {
    data: { id, value },
    type: actionTypes.TODO_SELECTED
  }
}

export function archiveSelectedItems(url, ids) {
  return dispatch => {
    dispatch(enableLoader());

    return $.ajax({
      url,
      method: 'PATCH',
      data: { ids },
    }).done(() => {
      dispatch({
        type: actionTypes.ARCHIVE_SELECTED_TODOS,
      });
    }).always(() =>
      dispatch(disableLoader())
    );
  };
}

export function hideWidget() {
  return {
    type: actionTypes.HIDE_WIDGET,
  }
}
