import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as taskWidgetActionCreators from '../actions/taskWidgetActionCreators';

import TodoItem from '../components/TodoItem';
import PortletLoading from '../components/PortletLoading';

function select(state) {
  // Which part of the Redux global state does our component want to receive as props?
  return { taskWidgetStore: state.taskWidgetStore };
}

class TaskWidget extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    taskWidgetStore: PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.dispatch(
      taskWidgetActionCreators.getTodos(
        this.props.taskWidgetStore.todosPath
      )
    );
  }

  componentDidMount() {
    this.initNiceScroll();
  }

  componentDidUpdate() {
    this.initNiceScroll();
  }

  initNiceScroll() {
    $('.nicescroll').niceScroll({
      cursorcolor: '#9d9ea5',
      cursorborderradius: '0px',
    });
  }

  createItem() {
    const { todosPath, newItemText } = this.props.taskWidgetStore;

    if (newItemText === '') return;

    this.props.dispatch(
      taskWidgetActionCreators.createNewItem(
        todosPath,
        newItemText,
      )
    );
  }

  render() {
    const { dispatch, taskWidgetStore } = this.props;
    const actions = bindActionCreators(taskWidgetActionCreators, dispatch);

    const { hidden, todosPath, newItemText, loaderEnabled } = taskWidgetStore;

    const todos = taskWidgetStore.todos.map((todo, key) =>
      <TodoItem {...{ ...todo, key, selectThisItem: (e) => actions.selectItem(todo.id, e.target.checked) }}/>
    );

    const selectedTodos = taskWidgetStore.todos.filter(todo => todo.selected);
    const totalTodosCount = taskWidgetStore.todos.length;
    const selectedTodoIds = selectedTodos.map(todo => todo.id);
    const selectedTodosCount = selectedTodos.length;

    return (
      <div className={'portlet' + (hidden ? ' hidden' : '')}>
        <div className="portlet-heading">
          <h3 className="portlet-title text-dark text-uppercase">todo</h3>

          <div className="portlet-widgets">
            <a onClick={() => actions.getTodos(todosPath)}><i className="ion-refresh" /></a>

            <span className="divider" />

            <a data-toggle="collapse" data-parent="#accordion1" href="#todo-panel" aria-expanded="true">
              <i className="ion-minus-round" />
            </a>

            <span className="divider" />

            <a onClick={() => actions.hideWidget()}><i className="ion-close-round" /></a>
          </div>

          <div className="clearfix" />
        </div>

        <div id="todo-panel" className="panel-collapse collapse in">
          <div className="portlet-body todoapp">
            <div className="row">
              <div className="col-sm-6">
                <h4>
                  {selectedTodosCount} of {totalTodosCount} remaining
                </h4>
              </div>

              <div className="col-sm-6">
                <a
                  onClick={() => actions.archiveSelectedItems(todosPath, selectedTodoIds)}
                  className="pull-right btn btn-primary btn-sm"
                >
                  Archive
                </a>
              </div>
            </div>

            <ul className="list-group nicescroll todo-list" style={{'maxHeight': '275px'}} >
              {todos}
            </ul>

            <div className="row m-t-20">
              <div className="col-sm-9">
                <input
                  type="text"
                  value={newItemText}
                  onChange={(e) => actions.updateNewItemText(e.target.value)}
                  className="form-control"
                  placeholder="Add new todo"
                />
              </div>

              <div className="col-sm-3 todo-send">
                <input
                  type="submit"
                  value="Add"
                  className="btn-info btn-block btn"
                  onClick={() => this.createItem()}
                />
              </div>
            </div>
          </div>
        </div>

        <PortletLoading enabled={loaderEnabled} />
      </div>
    );
  }
};

// Don't forget to actually use connect!
// Note that we don't export TaskWidget, but the redux "connected" version of it.
// See https://github.com/reactjs/react-redux/blob/master/docs/api.md#examples
export default connect(select)(TaskWidget);

