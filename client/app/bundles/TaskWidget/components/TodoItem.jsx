import React, { PropTypes } from 'react';

const TodoItem = ({ body, selected, selectThisItem }) => (
  <li className="list-group-item">
    <label className="cr-styled">
      <input type="checkbox" checked={selected} onChange={selectThisItem} />
      <i className="fa" />
    </label>

    <span>{body}</span>
  </li>
);

TodoItem.propTypes = {
  body: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  selectThisItem: PropTypes.func.isRequired,
};

export default TodoItem;
