import React, { PropTypes } from 'react';

export default class PortletLoading extends React.Component {
  static propTypes = {
    enabled: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      shown: false,
    };
  }

  componentWillMount(nextProps) {
    this.showLater();
  }

  componentWillUpdate(nextProps) {
    if (this.props.enabled !== nextProps.enabled) this.showLater();
  }

  showLater() {
    const component = this;

    component.setState({ shown: false });

    setTimeout(
      () => component.setState({ shown: true }),
      400
    );
  }

  render() {
    return(
      this.props.enabled && this.state.shown ?
        <div className="panel-disabled">
          <div className="loader-1" />
        </div>
      :
        false
    );
  }
};
