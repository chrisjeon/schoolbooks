import actionTypes from '../constants/taskWidgetConstants';

export const initialState = {
  todos: [],
  hidden: false,
  newItemText: '',
  loaderEnabled: true,
};

export default function taskWidgetReducer(state = initialState, action) {
  const { data, type } = action;

  switch (type) {
    case actionTypes.GOT_TODOS: {
      return {
        ...state,
        todos: data.todos,
      };
    }

    case actionTypes.NEW_ITEM_TEXT_CHANGED: {
      return {
        ...state,
        newItemText: data.newText,
      };
    }

    case actionTypes.TODO_CREATED: {
      return {
        ...state,
        todos: [
          data.todo,
          ...state.todos,
        ],
        newItemText: '',
      };
    }

    case actionTypes.TODO_SELECTED: {
      return {
        ...state,
        todos: state.todos.map(todo => todo.id === data.id ? { ...todo, selected: data.value } : todo),
      };
    }

    case actionTypes.ARCHIVE_SELECTED_TODOS: {
      return {
        ...state,
        todos: state.todos.filter(todo => !todo.selected),
      };
    }

   case actionTypes.LOADER_ENABLED: {
      return {
        ...state,
        loaderEnabled: true,
      };
    }

    case actionTypes.LOADER_DISABLED: {
      return {
        ...state,
        loaderEnabled: false,
      };
    }

    case actionTypes.HIDE_WIDGET: {
      return {
        ...state,
        hidden: true,
      };
    }

    default:
      return state;
  }
}
