import taskWidgetReducer from './taskWidgetReducer';
import { initialState as taskWidgetState } from './taskWidgetReducer';

export default {
  taskWidgetStore: taskWidgetReducer,
};

export const initialStates = {
  taskWidgetState,
};
