import React from 'react';
import ReactOnRails from 'react-on-rails';
import { Provider } from 'react-redux';

import TaskWidget from '../containers/TaskWidget';
import createStore from '../store/taskWidgetStore';

const TaskWidgetApp = (props, _railsContext) => {
  const store = createStore(props);
  const reactComponent = (
    <Provider store={store}>
      <TaskWidget />
    </Provider>
  );
  return reactComponent;
};

ReactOnRails.register({ TaskWidgetApp });
