export function errorNotification(title, text) {
  $.notify({
    text,
    title,
    image: "<i class='fa fa-exclamation'></i>",
  }, {
    style: 'metro',
    autoHide: true,
    className: 'error',
    clickToHide: true,
    showDuration: 150,
    hideDuration: 150,
    autoHideDelay: 20000,
    showAnimation: 'slideDown',
    hideAnimation: 'slideUp',
    globalPosition: 'top right',
  });
}
