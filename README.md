[ ![Codeship Status for chrisjeon/schoolbooks](https://codeship.com/projects/0453fb90-7f5f-0134-5755-32387a709fb7/status?branch=master)](https://codeship.com/projects/181914)

# SchoolBooks

This is the initial version of the SIS for Colombia Immersion

Setup Instructions
=================

Works best with Ruby 2.3.1 and PostgreSQL 9.x.

Dependencies:
* Rails 5
* Ruby 2.3.1
* PostgreSQL 9.x.
* Redis
* Node (tested with 6.7.0)

Install:

0. Clone and `bundle install && npm install`
0. `rails db:seed` to load initial data
0. `gem install foreman` to install foreman
0. `foreman start` to start up server and all dependencies (redis, sidekiq, etc.)
