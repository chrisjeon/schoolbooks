Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations' }
  root to: 'home#index'

  resources :accounts, only: [:show, :create, :update]

  resources :account_subscriptions, only: [:show, :update]

  resources :users do
    resources :todos
  end
  post '/users_create' => 'users#create'

  resources :inquired_users, only: [:index, :update]

  resources :available_days

  resources :levels do
    resources :level_memberships, only: [:create, :destroy, :update]
  end

  resources :pricing_plans do
    resources :plan_enrollments, only: [:new, :create, :destroy, :update]
  end

  resources :rooms

  resources :roles

  resources :users_roles, only: [:create, :destroy]

  resources :housings do
    resources :housing_occupancies, only: [:create, :destroy, :update]
  end

  resources :events do
    resources :event_sessions
    resources :event_attendances
  end

  resources :event_sessions do
    resources :event_attendances
  end

  resources :class_schedules

  resources :start_end_times

  resources :payments

  resources :payment_items

  namespace :payments do
    resources :payment_items, only: :destroy
  end
  get 'export_payments' => 'payments/csv_exporters#index', default: { format: :csv }

  resources :integrations

  resources :integration_processors, only: :create

  resources :report_teachers, only: :index

  resources :registration_forms, only: [:show, :update]

  namespace :registration_forms do
    resources :student_registrations, only: [:new, :create]
  end

  resources :terms_of_services

  resources :user_initial_setup, only: :index

  namespace :user_initial_setup do
    resources :assign_user_to_accounts, only: :create
  end

  # Integration sample files downloads routes
  get 'integration_user_csv' => 'integrations_sample_file_downloads#download_user_csv'
  get 'integration_housing_csv' => 'integrations_sample_file_downloads#download_housing_csv'
  get 'integration_housing_occupancy_csv' => 'integrations_sample_file_downloads#download_housing_occupancy_csv'
  get 'integration_plan_enrollment_csv' => 'integrations_sample_file_downloads#download_plan_enrollment_csv'

  namespace :ui, defaults: { format: 'json' } do
    namespace :v1 do
      resources :rooms, only: :index
      resources :students, only: :index
      resources :teachers, only: :index
      resources :class_sessions, only: %i(create show update destroy)
      resources :schedule_exports, only: :create

      resources :todos, only: %i(create index) do
        patch :update, on: :collection
      end
    end
  end

  # Stripe subscription actions
  post 'subscribe' => 'stripe_subscriptions#subscribe'
  post 'resume_subscription' => 'stripe_subscriptions#resume_subscription'
  post 'pause_subscription' => 'stripe_subscriptions#pause_subscription'
  post 'add_a_new_card' => 'stripe_subscriptions#add_a_new_card'
  delete 'delete_card' => 'stripe_subscriptions#delete_card'
  put 'make_default_card' => 'stripe_subscriptions#make_default_card'
end
