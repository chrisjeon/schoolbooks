every :day, at: '12:00am' do
  runner 'EnqueuePaymentItemsCreatorsJob.perform_later'
end

every :day, at: '3:00am' do
  runner 'EnqueueCreateAndSendInvoicesJob.perform_later'
end
