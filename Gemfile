source 'https://rubygems.org'

ruby '2.3.1'

gem 'aasm'
gem 'active_model_serializers', '~> 0.10.0'
gem 'appsignal'
gem 'awesome_print'
gem 'country_select'
gem 'devise'
gem 'devise-async'
gem 'dotenv-rails'
gem 'email_validator'
gem 'faker'
gem 'fast_blank'
gem 'font-awesome-rails'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'kaminari'
gem 'mailgun-ruby', require: 'mailgun'
gem 'mini_racer', platforms: :ruby
gem 'money-rails'
gem 'paperclip', '~> 5.0.0'
gem 'paper_trail'
gem 'pg', '~> 0.18'
gem 'pickadate-rails'
# gem 'puma', '~> 3.0' Maybe reenable it in the future but use unicorn for now
gem 'pry'
gem 'pundit'
gem 'rails', '~> 5.0.1'
gem 'react_on_rails', '~> 6'
gem 'rolify'
gem 'sass-rails', '~> 5.0'
gem 'stripe'
gem 'sidekiq'
gem 'turbolinks', '~> 5'
gem 'unicorn', '~> 4.8.3' # For app server
gem 'uglifier', '>= 1.3.0'
gem 'whenever', require: false

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'factory_girl_rails'
end

group :development do
  gem 'bullet'
  gem 'capistrano', '~> 3.4.0'
  gem 'capistrano-bundler'
  gem 'capistrano-cookbook', require: false
  gem 'capistrano-npm'
  gem 'capistrano-nvm', require: false
  gem 'capistrano-rails', '~> 1.1.0'
  gem 'capistrano-rbenv', '~> 2.0'
  gem 'capistrano-sidekiq'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'poltergeist'
  gem 'connection_pool'
  gem 'minitest-reporters'
  gem 'mocha'
  gem 'shoulda', '~> 3.5'
  gem 'shoulda-matchers', '~> 2.0'
  gem 'timecop'
  gem 'whenever-test'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
