require 'test_helper'

class ClassSchedulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    3.times { create(:class_schedule, account: @account) }
    get class_schedules_url
    assert_response :success
  end

  test 'GET new' do
    get new_class_schedule_url
    assert_response :success
  end

  test 'GET show' do
    class_schedule = create(:class_schedule, account: @account)
    get class_schedule_url(class_schedule)
    assert_response :success
  end

  test 'GET edit' do
    class_schedule = create(:class_schedule, account: @account)
    get edit_class_schedule_url(class_schedule)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('ClassSchedule.count', +1) do
      post class_schedules_url, params: {
        class_schedule: {
          maximum_students_per_class: 30,
          start_end_time_attributes: {
            start_date: '11/7/2016',
            end_date: '11/11/2016'
          }
        }
      }
    end
  end

  test 'PUT update' do
    class_schedule = create(:class_schedule, account: @account)
    start_end_time = create(:start_end_time, account: @account, timeable: class_schedule)
    put class_schedule_url(class_schedule), params: {
      class_schedule: {
        maximum_students_per_class: 30
      }
    }
    assert_response :redirect
    assert_equal 30, ClassSchedule.find(class_schedule.id).maximum_students_per_class
  end

  test 'DELETE destroy' do
    class_schedule = create(:class_schedule, account: @account)
    assert_difference('ClassSchedule.count', -1) do
      delete class_schedule_url(class_schedule)
    end
    assert_response :redirect
  end
end
