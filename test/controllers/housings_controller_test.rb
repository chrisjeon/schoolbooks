require 'test_helper'

class HousingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    3.times { create(:housing, account: @account) }
    get housings_url
    assert_response :success
  end

  test 'GET new' do
    get new_housing_url
    assert_response :success
  end

  test 'GET show' do
    housing = create(:housing, account: @account)
    get housing_url(housing)
    assert_response :success
  end

  test 'GET edit' do
    housing = create(:housing, account: @account)
    get edit_housing_url(housing)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('Housing.count', +1) do
      post housings_url,
           params: {
             housing: {
               owner_name: Faker::Name.name,
               email: Faker::Internet.email,
               phone: Faker::PhoneNumber.phone_number,
               description: Faker::Lorem.paragraph,
               additional_services_and_costs: Faker::Lorem.paragraph,
               maximum_occupancy: 4,
               confirmed_host: true,
               url: Faker::Internet.url,
               monthly_cost_cents: 4444,
               weekly_cost_cents: 5555
             }
           }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    housing = create(:housing, account: @account)
    put housing_url(housing), params: { housing: { owner_name: 'Pie' } }
    assert_equal 'Pie', housing.reload.owner_name
    assert_response :redirect
  end

  test 'DELETE destroy' do
    temp_housing = create(:housing, account: @account)
    assert_difference('Housing.count', -1) do
      delete housing_url(temp_housing)
    end
    assert_response :redirect
  end
end
