require 'test_helper'

class IntegrationsSampleFileDownloadsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET download_user_csv' do
    get integration_user_csv_url
    assert_response :success
  end

  test 'GET download_housing_csv' do
    get integration_housing_csv_url
    assert_response :success
  end

  test 'GET download_housing_occupancy_csv' do
    get integration_housing_occupancy_csv_url
    assert_response :success
  end

  test 'GET download_plan_enrollment_csv' do
    get integration_plan_enrollment_csv_url
    assert_response :success
  end
end
