require 'test_helper'

class TodosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @requester = create(:user, account: @account)
    @user.add_role :admin
    @requester.add_role :teacher
    sign_in @user
  end

  test 'GET new' do
    get new_user_todo_url(@user)
    assert_response :success
  end

  test 'GET show' do
    todo = create(:todo, account: @account, user: @user)
    get user_todo_url(@user, todo)
    assert_response :success
  end

  test 'GET edit' do
    todo = create(:todo, account: @account, user: @user)
    get edit_user_todo_url(@user, todo)
    assert_response :success
  end

  test 'GET index' do
    5.times { create(:todo, account: @account, user: @user) }
    get user_todos_url(@user)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('Todo.count', +1) do
      post user_todos_url(@user),
           params: {
             todo: {
               body: 'asodijfaosidfj'
             }
           }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    todo = create(:todo, account: @account, user: @user)
    put user_todo_url(@user, todo), params: { todo: { body: 'blah' } }
    assert_response :redirect
  end

  test 'DELETE destroy' do
    todo = create(:todo, account: @account, user: @user)
    assert_difference('Todo.count', -1) do
      delete user_todo_url(@user, todo)
    end
    assert_response :redirect
  end
end
