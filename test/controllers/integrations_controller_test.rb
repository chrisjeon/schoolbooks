require 'test_helper'

class IntegrationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    5.times { create(:integration_user, account: @account) }
    get integrations_url
    assert_response :success
  end

  test 'GET show' do
    user_integration = create(:integration_user, account: @account)
    get integration_url(user_integration)
    assert_response :success
  end

  test 'GET new' do
    get new_integration_url
    assert_response :success
  end

  test 'GET edit' do
    user_integration = create(:integration_user, account: @account)
    get edit_integration_url(user_integration)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('IntegrationUser.count', +1) do
      post integrations_url, params: {
        integration: {
          type: 'IntegrationUser',
          file: fixture_file_upload(
            Rails.root.join('test', 'fixtures', 'files', 'user_integration_sample.csv'),
            'text/csv'
          )
        }
      }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    user_integration = create(:integration_user, account: @account)
    put integration_url(user_integration), params: {
      integration: {
        file: fixture_file_upload(
          Rails.root.join('test', 'fixtures', 'files', 'user_integration_sample.csv'),
          'text/csv'
        )
      }
    }
    assert_response :redirect
  end

  test 'DELETE destroy' do
    user_integration = create(:integration_user, account: @account)
    assert_difference('IntegrationUser.count', -1) do
      delete integration_url(user_integration)
    end
    assert_response :redirect
  end
end
