require 'test_helper'

class PaymentItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    get payment_items_url
    assert_response :success
  end

  test 'GET new' do
    get new_payment_item_url
    assert_response :success
  end

  test 'GET show' do
    misc_payment_item = create(:misc_payment_item, account: @account, user: @user)
    payment_item = create(:payment_item,
                          account: @account,
                          user: @user,
                          itemable: misc_payment_item)
    get payment_item_url(payment_item)
    assert_response :success
  end

  test 'GET edit' do
    misc_payment_item = create(:misc_payment_item, account: @account, user: @user)
    payment_item = create(:payment_item,
                          account: @account,
                          user: @user,
                          itemable: misc_payment_item)
    get edit_payment_item_url(payment_item)
    assert_response :success
  end

  test 'POST create' do
    @account.update_attributes(currency: 'COP')
    assert_difference('PaymentItem.count', +1) do
      post payment_items_url,
        params: {
          payment_item: {
            user_id: @user.id,
            itemable_attributes: {
              price_cents: 1234
            }
          }
        }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    misc_payment_item = create(:misc_payment_item, account: @account, user: @user)
    payment_item = create(:payment_item,
                          account: @account,
                          user: @user,
                          itemable: misc_payment_item)
    put payment_item_url(payment_item), params: {
      payment_item: {
        itemable_attributes: {
          price_cents: 1234
        }
      }
    }
    assert_response :redirect
  end

  test 'DELETE destroy' do
    misc_payment_item = create(:misc_payment_item, account: @account, user: @user)
    payment_item = create(:payment_item,
                          account: @account,
                          user: @user,
                          itemable: misc_payment_item)
    assert_difference('PaymentItem.count', -1) do
      delete payment_item_url(payment_item)
    end
    assert_response :redirect
  end
end
