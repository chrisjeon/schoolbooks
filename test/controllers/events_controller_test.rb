require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    3.times { create(:event, account: @account) }
    get events_url
    assert_response :success
  end

  test 'GET new' do
    get new_event_url
    assert_response :success
  end

  test 'GET show' do
    event = create(:event, account: @account)
    get event_url(event)
    assert_response :success
  end

  test 'GET edit' do
    event = create(:event, account: @account)
    get edit_event_url(event)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('Event.count', +1) do
      post events_url,
        params: {
          event: {
            name: 'Hello New Event',
            maximum_capacity: 4,
            has_sessions: true,
            price_cents: 3333,
            description: 'The best event ever',
            start_end_time_attributes: {
              start_date: '11/2/2016',
              start_time: '11:33 AM',
              end_date: '11/2/2016',
              end_time: '4:44 PM'
            }
          },
        }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    event = create(:event, account: @account)
    create(:start_end_time, account: @account, timeable: event)
    put event_url(event), params: { event: { name: 'blah' } }
    assert_response :redirect
    assert_equal 'blah', Event.find(event.id).name
  end

  test 'DELETE destroy' do
    event = create(:event, account: @account)
    assert_difference('Event.count', -1) do
      delete event_url(event)
    end
    assert_response :redirect
  end
end
