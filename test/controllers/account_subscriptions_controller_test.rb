require 'test_helper'

class AccountSubscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @subscription_plan = create(:micro_plan)
    @account_subscription = create(:account_subscription,
                                               subscription_plan: @subscription_plan,
                                               account: @account)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET show' do
    get account_subscription_url(@account_subscription)
    assert_response :success
  end
end
