require 'test_helper'

class PricingPlansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @pricing_plan = create(:pricing_plan, account: @account)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    get pricing_plans_url
    assert_response :success
  end

  test 'GET new' do
    get new_pricing_plan_url
    assert_response :success
  end

  test 'GET edit' do
    get edit_pricing_plan_url(@pricing_plan)
    assert_response :success
  end

  test 'GET show' do
    get pricing_plan_url(@pricing_plan)
    assert_response :success
  end

  test 'POST create valid' do
    assert_difference('PricingPlan.count', +1) do
      post pricing_plans_url,
           params: {
             pricing_plan: {
               name: 'Immersion',
               frequency: 1,
               frequency_unit: 'week',
               price: 1234
             }
           }
    end
    assert_response :redirect
  end

  test 'POST create invalid' do
    assert_no_difference('PricingPlan.count') do
      post pricing_plans_url,
           params: {
             pricing_plan: {
               name: ''
             }
           }
    end
    assert_response :success
  end

  test 'PUT update' do
    put pricing_plan_url(@pricing_plan), params: { pricing_plan: { name: 'Chris' } }
    assert_response :redirect
  end

  test 'DELETE destroy' do
    assert_difference('PricingPlan.count', -1) do
      delete pricing_plan_url(@pricing_plan)
    end
    assert_response :redirect
  end
end
