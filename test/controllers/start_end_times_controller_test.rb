require 'test_helper'

class StartEndTimesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @pricing_plan = create(:pricing_plan, account: @account)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET new' do
    get new_start_end_time_url(timeable_type: 'PricingPlan', timeable_id: @pricing_plan.id)
    assert_response :success
  end

  test 'GET edit' do
    start_end_time = create(:start_end_time, account: @account, timeable: @pricing_plan)
    get edit_start_end_time_url(start_end_time, timeable_type: 'PricingPlan', timeable_id: @pricing_plan.id)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('StartEndTime.count', +1) do
      post start_end_times_url, params: {
        timeable_type: 'PricingPlan',
        timeable_id: @pricing_plan.id,
        start_end_time: {
          start_date: '11/2/1988',
          end_date: '11/3/1988'
        }
      }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    start_end_time = create(:start_end_time, account: @account, timeable: @pricing_plan)
    put start_end_time_url(start_end_time), params: {
      timeable_type: 'PricingPlan',
      timeable_id: @pricing_plan.id,
      start_end_time: {
        start_date: '11/2/1988',
        end_date: '11/3/1988'
      }
    }
    assert_response :redirect
  end

  test 'DELETE destroy' do
    start_end_time = create(:start_end_time, account: @account, timeable: @pricing_plan)
    assert_difference('StartEndTime.count', -1) do
      delete start_end_time_url(start_end_time, timeable_type: 'PricingPlan', timeable_id: @pricing_plan.id)
    end
    assert_response :redirect
  end
end
