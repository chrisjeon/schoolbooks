require 'test_helper'

class PaymentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    get payments_url
    assert_response :success
  end

  test 'GET show' do
    payment = create(:payment, account: @account, user: @user)
    misc_payment_item = create(:misc_payment_item, account: @account, user: @user)
    payment_item = create(:payment_item,
                          account: @account,
                          user: @user,
                          itemable: misc_payment_item,
                          payment: payment)
    get payment_url(payment)
    assert_response :success
  end

  test 'GET edit' do
    payment = create(:payment, account: @account, user: @user)
    misc_payment_item = create(:misc_payment_item, account: @account, user: @user)
    payment_item = create(:payment_item,
                          account: @account,
                          user: @user,
                          itemable: misc_payment_item,
                          payment: payment)
    get edit_payment_url(payment)
    assert_response :success
  end

  test 'PUT update' do
    payment = create(:payment, account: @account, user: @user)

    misc_payment_item = create(:misc_payment_item, account: @account, user: @user)
    payment_item = create(:payment_item,
                          account: @account,
                          user: @user,
                          itemable: misc_payment_item,
                          payment: payment)
    put payment_url(payment), params: { payment: { paid: true, payment_method: 'cash' } }
    assert_response :redirect
    assert_equal 'cash', payment.reload.payment_method
    assert payment.reload.paid?
  end
end
