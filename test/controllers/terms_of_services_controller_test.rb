require 'test_helper'

class TermsOfServiceControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    3.times { create(:terms_of_service, account: @account) }
    get terms_of_services_url
    assert_response :success
  end

  test 'GET new' do
    get new_terms_of_service_url
    assert_response :success
  end

  test 'GET show' do
    terms_of_service = create(:terms_of_service, account: @account)
    get terms_of_service_url(terms_of_service)
    assert_response :success
  end

  test 'GET edit' do
    terms_of_service = create(:terms_of_service, account: @account)
    get edit_terms_of_service_url(terms_of_service)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('TermsOfService.count', +1) do
      post terms_of_services_url, params: {
        terms_of_service: {
          name: 'Hi',
          body: 'yay'
        }
      }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    terms_of_service = create(:terms_of_service, name: 'bah', account: @account)
    put terms_of_service_url(terms_of_service), params: {
      terms_of_service: {
        name: 'Boo'
      }
    }
    assert_equal 'Boo', terms_of_service.reload.name
    assert_response :redirect
  end

  test 'DELETE destroy' do
    terms_of_service = create(:terms_of_service, account: @account)
    assert_difference('TermsOfService.count', -1) do
      delete terms_of_service_url(terms_of_service)
    end
    assert_response :redirect
  end
end
