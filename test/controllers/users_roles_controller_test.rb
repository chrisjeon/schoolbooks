require 'test_helper'

class UsersRolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    @custom_role = Role.create(name: Faker::Lorem.word, account: @account)
    sign_in @user
  end

  test 'POST create' do
    assert_difference('UsersRole.count', +1) do
      post users_roles_url,
           params: {
             user_id: @user.id,
             role_id: @custom_role.id
           }
    end
  end

  test 'DELETE destroy' do
    @user.roles << @custom_role
    users_role = UsersRole.find_by(user: @user, role: @custom_role)

    assert_difference('UsersRole.count', -1) do
      delete users_role_path(users_role)
    end
    assert_response :redirect
  end
end
