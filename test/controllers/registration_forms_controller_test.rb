require 'test_helper'

class RegistrationFormsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET show' do
    registration_form = create(:registration_form, account: @account)
    get registration_form_url(registration_form)
    assert_response :success
  end

  test 'PUT update' do
    registration_form = create(:registration_form, account: @account)
    put registration_form_url(registration_form), params: {
      registration_form: {
        custom_form: true
      }
    }
    assert_response :success
    assert registration_form.reload.custom_form?
  end
end
