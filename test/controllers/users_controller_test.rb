require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    get users_url
    assert_response :success
  end

  test 'POST create valid' do
    perform_enqueued_jobs do
      assert_difference('User.count', +1) do
        post users_create_url,
             params: {
               user: {
                 email: 'john.smith@example.com',
                 first_name: 'John',
                 last_name: 'Smith',
                 password: 'password',
                 password_confirmation: 'password'
               }
             }
      end

      assert_response :success
    end
  end

  test 'POST create valid (enqueuing jobs)' do
    assert_enqueued_jobs 1 do
      post users_create_url,
           params: {
             user: {
               email: 'john.smith@example.com',
               first_name: 'John',
               last_name: 'Smith',
               password: 'password',
               password_confirmation: 'password'
             }
           }
    end
  end

  test 'POST create invalid' do
    assert_no_difference('User.count') do
      post users_create_url,
           params: {
             user: {
               first_name: 'John',
               last_name: 'Smith',
               password: 'password',
               password_confirmation: 'password'
             }
           }
    end
    assert_response :success
  end

  test 'PUT update' do
    put user_url(@user), params: { user: { first_name: 'Chris'}, role: { Role.first.id.to_s => '1' } }
    assert_response :success
  end

  test 'DELETE destroy authorized' do
    user = create(:user, account: @account)
    assert_difference('User.count', -1) do
      delete user_path(user)
    end
    assert_response :success
  end

  test 'DELETE destroy unauthorized' do
    user = create(:user, account: @account)
    @user.remove_role :admin
    assert_no_difference('User.count') do
      delete user_path(user)
    end
    assert_response :redirect
  end
end
