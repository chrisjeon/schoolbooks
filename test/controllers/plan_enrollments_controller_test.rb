require 'test_helper'

class PlanEnrollmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @pricing_plan = create(:pricing_plan, account: @account)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET new' do
    get new_pricing_plan_plan_enrollment_path(@pricing_plan), params: {
      user_id: @user.id
    }
    assert_response :success
  end

  test 'POST create' do
    assert_difference('PlanEnrollment.count', +1) do
      post pricing_plan_plan_enrollments_url(@pricing_plan),
           params: {
             plan_enrollment: {
               user_id: @user.id,
               start_end_time_attributes: {
                 start_date: '2016-11-21',
                 end_date: '2016-11-27'
               }
             }
           }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    plan_enrollment = create(:plan_enrollment,
                             account: @account,
                             user: @user,
                             pricing_plan: @pricing_plan)
    create(:start_end_time, account: @account, timeable: plan_enrollment)
    put pricing_plan_plan_enrollment_url(@pricing_plan, plan_enrollment),
        params: {
          plan_enrollment: {
            start_end_time_attributes: {
              start_date: '2016-10-22'
            }
          }
        }
    assert plan_enrollment.start_end_time.present?
    assert_response :redirect
  end

  test 'DELETE destroy' do
    plan_enrollment = create(:plan_enrollment,
                             account: @account,
                             user: @user,
                             pricing_plan: @pricing_plan)
    assert_difference('PlanEnrollment.count', -1) do
      delete pricing_plan_plan_enrollment_url(@pricing_plan, plan_enrollment)
    end
    assert_response :redirect
  end
end
