require 'test_helper'

class HousingOccupanciesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    @housing = create(:housing, account: @account)
    sign_in @user
  end

  test 'POST create' do
    assert_difference('HousingOccupancy.count', +1) do
      post housing_housing_occupancies_url(@housing),
           params: {
             housing_occupancy: {
               user_id: @user.id,
               start_end_time_attributes: {
                 start_date: '2016-11-21',
                 end_date: '2016-11-27'
               }
             }
           }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    housing_occupancy = create(:housing_occupancy,
                               housing: @housing,
                               account: @account)
    create(:start_end_time, account: @account, timeable: housing_occupancy)
    put housing_housing_occupancy_url(@housing, housing_occupancy),
        params: {
          housing_occupancy: {
            start_end_time_attributes: {
              start_date: '2016-10-22'
            }
          }
        }
    assert housing_occupancy.start_end_time.present?
    assert_response :redirect

  end

  test 'DELETE destroy' do
    housing_occupancy = create(:housing_occupancy,
                               housing: @housing,
                               account: @account)
    assert_difference('HousingOccupancy.count', -1) do
      delete housing_housing_occupancy_url(@housing, housing_occupancy)
    end
    assert_response :redirect
  end
end
