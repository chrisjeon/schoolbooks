require 'test_helper'

class LevelMembershipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @level = create(:level, account: @account)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'POST create' do
    assert_difference('LevelMembership.count', +1) do
      post level_level_memberships_url(@level),
           params: {
             level_membership: {
               user_id: @user.id
             }
           }
    end
    assert_response :redirect

  end

  test 'DELETE destroy' do
    level_membership = create(:level_membership,
                              account: @account,
                              user: @user,
                              level: @level)
    assert_difference('LevelMembership.count', -1) do
      delete level_level_membership_url(@level, level_membership)
    end
    assert_response :redirect
  end
end
