require 'test_helper'

class EventSessionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @event = create(:event, account: @account)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET new' do
    get new_event_event_session_url(@event)
    assert_response :success
  end

  test 'GET edit' do
    event_session = create(:event_session, account: @account, event: @event)
    get edit_event_event_session_url(@event, event_session)
    assert_response :success
  end

  test 'GET show' do
    event_session = create(:event_session, account: @account, event: @event)
    get event_event_session_url(@event, event_session)
    assert_response :success
  end

  test 'POST create' do
    assert_difference('EventSession.count', +1) do
      post event_event_sessions_url(@event),
        params: {
          event_session: {
            maximum_capacity: 4,
            start_end_time_attributes: {
              start_date: '11/2/2016',
              start_time: '11:33 AM',
              end_date: '11/2/2016',
              end_time: '4:44 PM'
            }
          }
        }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    event_session = create(:event_session, account: @account, event: @event)
    start_end_time = create(:start_end_time, account: @account, timeable: event_session)
    put event_event_session_url(@event, event_session),
      params: {
        event_session: {
          maximum_capacity: 5
        }
      }
    assert_response :redirect
  end

  test 'DELETE destroy' do
    event_session = create(:event_session, account: @account, event: @event)
    assert_difference('EventSession.count', -1) do
      delete event_event_session_url(@event, event_session)
    end
    assert_response :redirect
  end
end
