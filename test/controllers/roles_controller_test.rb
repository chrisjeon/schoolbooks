require 'test_helper'

class RolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    %w(student admin teacher coach employee).each do |name|
      Role.create(name: name)
    end
    @role = Role.first
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @unauthorized_user = create(:user, account: @account)
    @user.roles << Role.find_by(name: 'admin')
    sign_in @user
  end

  test 'GET new' do
    get roles_url
    assert_response :success
  end

  test 'GET show' do
    get role_url(@role)
    assert_response :success
  end

  test 'GET edit' do
    custom_role = Role.create(name: Faker::Lorem.word, account: @account)
    get edit_role_url(custom_role)
    assert_response :success
  end

  test 'GET index' do
    get roles_url
    assert_response :success
  end

  test 'POST create' do
    assert_difference('Role.count', +1) do
      post roles_url, params: { role: { name: 'coach' } }
    end
    assert_response :redirect
  end

  test 'PUT update' do
    new_role = Role.create(name: Faker::Lorem.word, account: @account)
    put role_url(new_role), params: { role: { name: 'sweet' } }
    assert_response :redirect
  end

  test 'DELETE destroy role' do
    new_role = Role.create(name: Faker::Lorem.word, account: @account)
    assert_difference('Role.count', -1) do
      delete role_path(new_role)
    end
    assert_response :redirect
  end
end
