require 'test_helper'

class IntegrationProcessorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @integration = create(:integration_user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'POST create responds with redirect' do
    post integration_processors_url(id: @integration)
    assert_response :redirect
  end

  test 'POST create enqueues ProcessIntegrationsJob' do
    assert_enqueued_with(job: ProcessIntegrationsJob) do
      post integration_processors_url(id: @integration)
    end
  end
end
