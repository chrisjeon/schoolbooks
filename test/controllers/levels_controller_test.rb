require 'test_helper'

class LevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    get levels_url
    assert_response :success
  end

  test 'GET new' do
    get new_level_url
    assert_response :success
  end

  test 'GET show' do
    level = create(:level, account: @account)
    get level_url(level)
    assert_response :success
  end

  test 'POST create valid' do
    assert_difference('Level.count', +1) do
      post levels_url,
           params: {
             level: {
               name: 'New Level Name',
             }
           }
    end
    assert_response :redirect
  end

  test 'POST create invalid' do
    assert_no_difference('Level.count') do
      post levels_url,
           params: {
             level: {
               name: ''
             }
           }
    end
    assert_response :success
  end
end
