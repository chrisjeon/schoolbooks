require 'test_helper'

class UserInitialSetupControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = create(:user, account: nil)
    sign_in @user
  end

  test 'GET index' do
    get user_initial_setup_index_url
    assert_response :success
  end
end
