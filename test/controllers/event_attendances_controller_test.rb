require 'test_helper'

class EventAttendancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @event = create(:event, account: @account)
    @event_session = create(:event_session, account: @account, event: @event)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'POST create' do
    assert_difference('EventAttendance.count', +1) do
      post event_event_attendances_url(@event), params: {
        event_attendance: {
          user_id: @user.id
        }
      }
    end
    assert_response :redirect
  end

  test 'DELETE destroy' do
    event_attendance = create(:event_attendance, event: @event, user: @user, event_session: nil, account: @account)

    assert_difference('EventAttendance.count', -1) do
      delete event_event_attendance_url(@event, event_attendance)
    end
    assert_response :redirect
  end
end
