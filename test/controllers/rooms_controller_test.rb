require 'test_helper'

class RoomsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET index' do
    5.times { create(:room, account: @account) }
    get rooms_url
    assert_response :success
  end

  test 'GET new' do
    get new_room_url
    assert_response :success
  end

  test 'GET show' do
    room = create(:room, account: @account)
    get room_url(room)
    assert_response :success
  end

  test 'GET edit' do
    room = create(:room, account: @account)
    get edit_room_url(room)
    assert_response :success
  end

  test 'POST create valid' do
    assert_difference('Room.count', +1) do
      post rooms_url,
           params: {
             room: {
               name: 'Luz',
             }
           }
    end
    assert_response :redirect
  end

  test 'POST create invalid' do
    assert_no_difference('Room.count') do
      post rooms_url,
           params: {
             room: {
               name: '',
             }
           }
    end
    assert_response :success
  end

  test 'PUT update' do
    room = create(:room, account: @account)
    put room_url(room), params: { room: { name: 'yoyoyo'} }
    assert_response :redirect
  end

  test 'DELETE destroy' do
    room = create(:room, account: @account)
    assert_difference('Room.count', -1) do
      delete room_url(room)
    end
    assert_response :redirect
  end
end
