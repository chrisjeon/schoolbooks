require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET show' do
    get account_url(@account)
    assert_response :success
  end

  test 'POST create' do
    micro_plan = create(:micro_plan)

    assert_difference('Account.count', +1) do
      post accounts_url, params: {
        account: {
          name: 'Bunny Warriors',
          email: 'bunny@warriors.com',
          invoices_sent_on: 'Everyday',
          payments_due_on: 'Sunday',
          account_subscription_attributes: {
            subscription_plan_id: micro_plan.id
          }
        }
      }
    end
  end

  test 'PUT update' do
    put account_url(@account), params: { account: { name: 'New name' } }
    assert_response :success
    assert_equal 'New name', @account.reload.name
  end
end
