require 'test_helper'

class Ui::V1::ScheduleExportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'unauthorized' do
    @user.remove_role :admin

    ExportAndSendScheduleJob.expects(:perform_later).never

    post ui_v1_schedule_exports_url, params: { id: 1 }

    assert_response :unauthorized
  end

  test 'schedule not found' do
    ExportAndSendScheduleJob.expects(:perform_later).never

    assert_raises ActiveRecord::RecordNotFound do
      post ui_v1_schedule_exports_url, params: { id: 1 }
    end
  end

  test 'async job scheduled' do
    class_schedule = create(:class_schedule, account: @account)

    ExportAndSendScheduleJob.expects(:perform_later).with(class_schedule)

    post ui_v1_schedule_exports_url, params: { id: class_schedule.id }

    assert_response :ok
  end
end
