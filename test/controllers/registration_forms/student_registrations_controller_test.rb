require 'test_helper'

class RegistrationForms::StudentRegistrationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
  end

  test 'GET new' do
    get new_registration_forms_student_registration_url, params: { account_id: @account.id }
    assert_response :success
  end

  test 'POST create' do
    pricing_plan = create(:pricing_plan, account: @account)
    assert_difference('User.count', +1) do
      post registration_forms_student_registrations_url, params: {
        account_id: @account.id,
        user: {
          first_name: 'asdf',
          last_name: 'fdas',
          email: 'asdf+example@example.com',
          class_schedule_preference: 'both',
          country: 'US',
          phone: '',
          gender: 'male',
          passport_number: '',
          birth_date: '1988-11-2',
          emergency_contact_name: 'asdf',
          emergency_contact_number: '123-123-1111'
        },
        registration_information: {
          starting_date: '2016-11-22',
          sign_up_period: 1,
          pricing_plan_id: pricing_plan.id,
          current_level_description: 'asdfa',
          how_did_you_hear_about_us: 'adsfasdf',
          why_did_you_choose_us: 'asdfasdf',
          anything_else: 'asdf',
          housing_information: 'asdf',
          coaching_sessions_per_week: 1
        }
      }
    end
    assert_response :redirect
  end
end
