require 'test_helper'

class Payments::CsvExportersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @user = create(:user, account: @account)
    @user.add_role :admin
    sign_in @user
    5.times do
      create(:misc_payment_item, account: @account)
    end
    @account.misc_payment_items.each do |misc_payment_item|
      create(:payment_item, account: @account, itemable: misc_payment_item)
    end
    @account.payment_items.update_all(created_at: Date.today - 3.days)
  end

  test 'GET index' do
    get export_payments_url(start_date: Date.today - 30.days, end_date: Date.today), xhr: true, as: :csv
    assert_response :success
    assert_equal 'text/csv', response.content_type

    csv = CSV.parse(response.body)
    assert csv
    assert_equal 6, csv.size
  end
end
