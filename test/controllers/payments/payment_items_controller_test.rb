require 'test_helper'

class Payments::PaymentItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @student = create(:user, account: @account)
    @admin   = create(:user, account: @account)
    @student.add_role :student
    @admin.add_role :admin
    sign_in @admin
    @misc_payment_items = create(:misc_payment_item, account: @account, user: @student)
    @payment = create(:payment, account: @account, user: @student)
    @payment_item = create(:payment_item,
                           account: @account,
                           itemable: @misc_payment_items,
                           user: @student)
  end

  test 'DELETE destroy' do
    @payment_item.update_attributes(payment: @payment)
    delete payments_payment_item_url(@payment_item)
    assert_nil @payment_item.reload.payment
    assert_equal 0, @payment.reload.subtotal_cents
  end
end
