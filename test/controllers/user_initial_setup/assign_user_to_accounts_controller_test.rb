require 'test_helper'

class UserInitialSetup::AssignUserToAccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account = create(:account, code: 'abc123')
    @user = create(:user, account: nil)
    sign_in @user
  end

  test 'POST create' do
    post user_initial_setup_assign_user_to_accounts_path(code: 'abc123')
    assert_response :redirect
    assert_equal @account, @user.reload.account
  end
end
