require 'test_helper'

class PaymentItemsCreatorServiceTest < ActiveSupport::TestCase
  setup do
    @account = create(:account, payments_due_on: 'Tuesday')
    @user    = create(:user, account: @account)
    @user.add_role :student

    @event        = create(:event, account: @account)
    @housing      = create(:housing, account: @account)
    @pricing_plan = create(:pricing_plan, account: @account)

    @event_attendance = create(:event_attendance, event: @event,
                                           user: @user,
                                           account: @account)
    @housing_occupancy = create(:housing_occupancy,
                                housing: @housing,
                                account: @account,
                                user: @user,
                                price: 1234)
    @plan_enrollment = create(:plan_enrollment,
                              account: @account,
                              user: @user,
                              pricing_plan: @pricing_plan,
                              price: 1234)
    @misc_payment_item = create(:misc_payment_item, user: @user, account: @account)

    @pic_service = PaymentItemsCreatorService.new(@account)
  end

  test '#process!' do
    Time.zone = @account.time_zone
    mock_time = Time.zone.strptime('10/21/2016', '%m/%d/%Y').to_date
    Timecop.freeze(mock_time) do
      @pic_service.process!
      assert_equal 4, @account.payment_items.count
      assert @account.payment_items.pluck(:price_cents).exclude?(0)
      assert_equal 1, @account.payment_items.pluck(:due_on).map { |due_on| due_on.strftime('%A') }.uniq.count
      assert_equal 'Tuesday', @account.payment_items.pluck(:due_on).map { |due_on| due_on.strftime('%A') }.uniq.first
    end
  end
end
