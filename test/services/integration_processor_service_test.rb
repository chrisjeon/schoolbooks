require 'test_helper'

class IntegrationProcessorServiceTest < ActiveSupport::TestCase
  setup do
    @account = create(:account)
  end

  context 'IntegrationUser' do
    setup do
      @integration_user = create(:integration_user, account: @account)
      @integration_processor_service = IntegrationProcessorService.new(@integration_user)
    end

    should '#process! for IntegrationUser' do
      assert_difference('User.count', +4) do
        @integration_processor_service.process!
      end
    end

    should '#process! running twice keeps user count to 4' do
      @integration_processor_service.process!
      @integration_processor_service.process!
      assert_equal 4, @account.users.count
    end

    should '#process! all boolean values are saved properly' do
      @integration_processor_service.process!

      @account.users.each do |user|
        assert user.schedulable?
        assert user.available_in_morning?
        assert !user.available_in_afternoon?
        assert user.active?
      end
    end
  end

  context 'IntegrationHousing' do
    setup do
      @integration_housing = create(:integration_housing, account: @account)
      @integration_processor_service = IntegrationProcessorService.new(@integration_housing)
    end

    should '#process! for IntegrationHousing' do
      assert_difference('Housing.count', +7) do
        @integration_processor_service.process!
      end
    end

    should '#process! running twice keeps Housing count to 7' do
      @integration_processor_service.process!

      assert_difference('Housing.count', 0) do
        @integration_processor_service.process!
      end
    end

    should '#process! updates existing Housing' do
      email = 'lina_zapata@example.com'
      housing = create :housing, account: @account, email: email

      assert housing.persisted?

      @integration_processor_service.process!

      refute_equal housing.owner_name, Housing.find_by(email: email).owner_name
    end
  end

  context 'IntegrationHousingOccupancy' do
    setup do
      integration_user = create(:integration_user, account: @account)
      IntegrationProcessorService.new(integration_user).process!

      integration_housing = create(:integration_housing, account: @account)
      IntegrationProcessorService.new(integration_housing).process!

      @integration_housing_occupancy = create(:integration_housing_occupancy, account: @account)
      @integration_processor_service = IntegrationProcessorService.new(@integration_housing_occupancy)
    end

    should '#process! for IntegrationHousing' do
      assert_difference('HousingOccupancy.count', +3) do
        @integration_processor_service.process!
      end
    end

    should '#process! running twice keeps Housing count to 3' do
      @integration_processor_service.process!

      assert_difference('HousingOccupancy.count', 0) do
        @integration_processor_service.process!
      end
    end
  end

  context 'IntegrationPlanEnrollment' do
    setup do
      %w(INTENSIVE123 PASSIVE123 TEMPORARY123 PRIVATE123).each do |pricing_plan_code|
        create(:pricing_plan, account: @account, code: pricing_plan_code)
      end
      ['pepe+example@gmail.com', 'pepe+example1@gmail.com', 'pepe+example2@gmail.com', 'pepe+example3@gmail.com'].each do |user_email|
        create(:user, account: @account, email: user_email)
      end

      @integration_plan_enrollment = create(:integration_plan_enrollment, account: @account)
      @integration_processor_service = IntegrationProcessorService.new(@integration_plan_enrollment)
    end

    should '#process! for IntegrationPlanEnrollment' do
      assert_difference('PlanEnrollment.count', +4) do
        @integration_processor_service.process!
      end
    end

    should '#process! running twice keeps PlanEnrollment count to 4' do
      @integration_processor_service.process!
      @integration_processor_service.process!
      assert_equal 4, @account.plan_enrollments.count
    end

    should '#process! saves start_on and end_dates properly' do
      @integration_processor_service.process!
      @account.plan_enrollments.each do |plan_enrollment|
        assert plan_enrollment.start_end_time.present?
        assert plan_enrollment.start_end_time.start_on.present?
        assert plan_enrollment.start_end_time.end_on.present?
      end
    end

    should '#process! save the prices properly' do
      @integration_processor_service.process!
      pricing_plan = PricingPlan.find_by(code: 'INTENSIVE123', account: @account)
      user = User.find_by(email: 'pepe+example@gmail.com')
      plan_enrollment = PlanEnrollment.find_by(pricing_plan: pricing_plan, account: @account, user: user)
      price_object = Money.new(30000, @account.currency)
      assert_equal price_object, plan_enrollment.price
    end
  end
end
