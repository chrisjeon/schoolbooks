require 'test_helper'

class PaymentsCreatorServiceTest < ActiveSupport::TestCase
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @users   =  5.times.collect { create(:user, account: @account) }
    @users.each { |user| user.add_role :student }

    @event   = create(:event, account: @account)
    @housing = create(:housing, account: @account)
    @pricing_plan   = create(:pricing_plan, account: @account)

    @users.each do |user|
      create(:event_attendance, event: @event, user: user, account: @account)
      create(:housing_occupancy, housing: @housing, account: @account, user: user)
      create(:plan_enrollment, account: @account, user: user, pricing_plan: @pricing_plan)
      create(:misc_payment_item, user: user, account: @account)
    end

    PaymentItemsCreatorService.new(@account).process!

    @payments_creator_service = PaymentsCreatorService.new(@account)
  end

  test '#process!' do
    perform_enqueued_jobs do
      @payments_creator_service.process!
      assert_equal 5, @account.payments.count
      assert @account.payments.pluck(:subtotal_cents).exclude?(0)
      assert @account.payments.pluck(:subtotal_cents).exclude?(nil)
    end
  end
end
