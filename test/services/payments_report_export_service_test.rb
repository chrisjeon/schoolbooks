require 'test_helper'

class PaymentsReportExportServiceTest < ActiveSupport::TestCase
  setup do
    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    5.times do
      create(:misc_payment_item, account: @account)
    end
    @account.misc_payment_items.each do |misc_payment_item|
      create(:payment_item, account: @account, itemable: misc_payment_item)
    end
    @account.payment_items.update_all(created_at: Date.today - 2.days)
    @payments_report_export_service = PaymentsReportExportService.new(@account)
  end

  test 'PaymentItemCsvMap Struct' do
    payment_item = @account.payment_items.last
    payment_item_csv_map = PaymentsReportExportService::PaymentItemCsvMap.new(payment_item)
    assert_equal payment_item.user.full_name, payment_item_csv_map.user_name
    assert_equal payment_item.user.id, payment_item_csv_map.user_id
    assert_equal payment_item.itemable_type, payment_item_csv_map.type
    assert_equal payment_item.paid?, payment_item_csv_map.paid
    assert_equal 'N/A', payment_item_csv_map.paid_on
    assert_equal payment_item.payment.payment_method, payment_item_csv_map.paid_with
  end

  test '#process!' do
    csv_file = CSV.parse(@payments_report_export_service.process!)
    assert_equal 6, csv_file.size
  end
end
