FactoryGirl.define do
  factory :subscription_plan do
    currency 'USD'

    factory :micro_plan do
      name 'Micro'
      price_cents 4900
      stripe_plan_id 'schoolbooks-micro'
    end

    factory :pro_plan do
      name 'Pro'
      price_cents 7900
      stripe_plan_id 'schoolbooks-pro'
    end

    factory :premium_plan do
      name 'Premium'
      price_cents 9900
      stripe_plan_id 'schoolbooks-premium'
    end
  end
end
