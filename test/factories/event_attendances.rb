FactoryGirl.define do
  factory :event_attendance do
    user
    event
    event_session
    account
    paid false
  end
end
