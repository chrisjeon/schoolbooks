FactoryGirl.define do
  factory :integration_user do
    account
    file do
      fixture_file_upload(
        Rails.root.join('test', 'fixtures', 'files', 'user_integration_sample.csv'),
        'text/csv'
      )
    end
  end
end
