FactoryGirl.define do
  factory :registration_information do
    account
    user
    starting_date { Date.today + (1..60).to_a.sample.days }
    sign_up_period { (1..10).to_a.sample }
    pricing_plan
    current_level_description { Faker::Lorem.paragraph }
    how_did_you_hear_about_us { Faker::Lorem.paragraph }
    why_did_you_choose_us { Faker::Lorem.paragraph }
    anything_else { Faker::Lorem.paragraph }
    housing_information { Faker::Lorem.paragraph }
    coaching { Faker::Lorem.paragraph }
  end
end
