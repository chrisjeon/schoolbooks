FactoryGirl.define do
  factory :housing_occupancy do
    user
    account
    housing
    price (100..1000).to_a.sample
  end
end
