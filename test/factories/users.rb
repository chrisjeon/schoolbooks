FactoryGirl.define do
  factory :user do
    account
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    schedulable { [true, false].sample }
    country { Faker::Address.country_code }
    phone { Faker::PhoneNumber.cell_phone }
    gender { ['male', 'female'].sample }
    passport_number { Faker::Code.imei }
    birth_date { Date.today - (18..35).to_a.sample }
    emergency_contact_name { Faker::Name.name }
    emergency_contact_number { Faker::PhoneNumber.cell_phone }
  end
end
