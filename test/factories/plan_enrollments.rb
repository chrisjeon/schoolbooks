FactoryGirl.define do
  factory :plan_enrollment do
    account
    user
    pricing_plan
    price (100..1000).to_a.sample
  end
end
