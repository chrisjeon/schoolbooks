FactoryGirl.define do
  factory :integration_housing_occupancy do
    account
    file do
      fixture_file_upload(
        Rails.root.join('test/fixtures/files/housing_occupancy_integration_sample.csv'),
        'text/csv'
      )
    end
  end
end
