FactoryGirl.define do
  factory :housing do
    account
    owner_name { Faker::Name.name }
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.phone_number }
    description { Faker::Lorem.paragraph }
    additional_services_and_costs { Faker::Lorem.paragraph }
    maximum_occupancy { (1..4).to_a.sample }
    monthly_cost { (1..1000).to_a.sample }
    weekly_cost { (1..1000).to_a.sample }
    confirmed_host true
    url { Faker::Internet.url }
    currency 'USD'
  end
end
