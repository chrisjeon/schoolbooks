FactoryGirl.define do
  factory :account_subscription do
    subscription_plan
    account
    stripe_customer_token { Faker::Code.ean }
    aasm_state 'trial'
  end
end
