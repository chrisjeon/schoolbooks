FactoryGirl.define do
  factory :pricing_plan do
    account
    name { Faker::Lorem.word }
    code { Faker::Code.isbn }
    price { (1..1000).to_a.sample }
    frequency { (1..7).to_a.sample }
    frequency_unit { %w(day week).sample }
  end
end
