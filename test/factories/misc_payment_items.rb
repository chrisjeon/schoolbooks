FactoryGirl.define do
  factory :misc_payment_item do
    account
    user
    price { (1..1000).to_a.sample }
  end
end
