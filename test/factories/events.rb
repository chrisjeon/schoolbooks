FactoryGirl.define do
  factory :event do
    account
    name { Faker::Lorem.word }
    maximum_capacity { (1..15).to_a.sample }
    price { (1..1000).to_a.sample }
    currency 'USD'
    description { Faker::Lorem.paragraph }
    has_sessions false
  end
end
