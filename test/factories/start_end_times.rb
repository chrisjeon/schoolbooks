FactoryGirl.define do
  factory :start_end_time do
    account
    start_on { DateTime.now }
    end_on { DateTime.now + 3.hours }
  end
end
