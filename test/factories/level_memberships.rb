FactoryGirl.define do
  factory :level_membership do
    account
    user
    level
  end
end
