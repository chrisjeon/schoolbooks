FactoryGirl.define do
  factory :class_attendance do
    account
    class_schedule
    class_session
    user
  end
end
