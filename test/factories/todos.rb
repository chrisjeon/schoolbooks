FactoryGirl.define do
  factory :todo do
    account
    user
    body { Faker::Lorem.paragraph }
    complete false
  end
end
