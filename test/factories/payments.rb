FactoryGirl.define do
  factory :payment do
    account
    user
    paid false
    due_on { DateTime.now + 5.days }
    paid_on { DateTime.now + 5.days }
    discount 0
  end
end
