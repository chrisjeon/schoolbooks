FactoryGirl.define do
  factory :note do
    noteable
    body { Faker::Lorem.paragraph }
  end
end
