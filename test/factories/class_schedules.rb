FactoryGirl.define do
  factory :class_schedule do
    account
    name { Faker::Lorem.word }
    start_end_time { build(:start_end_time, account: account) }
    maximum_students_per_class { (1..5).to_a.sample }
  end
end
