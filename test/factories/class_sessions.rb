FactoryGirl.define do
  factory :class_session do
    account
    class_schedule
    start_on { DateTime.now }
    end_on { DateTime.now + 3.hours }
    room
  end
end
