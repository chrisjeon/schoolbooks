FactoryGirl.define do
  factory :payment_item do
    account
    user
    payment
    paid false
    due_on { DateTime.now + (1..30).to_a.sample.days }
  end
end
