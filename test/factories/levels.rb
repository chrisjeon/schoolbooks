FactoryGirl.define do
  factory :level do
    account
    active true
    name { Faker::Name.name }
  end
end
