FactoryGirl.define do
  factory :integration_plan_enrollment do
    account
    file do
      fixture_file_upload(
        Rails.root.join('test', 'fixtures', 'files', 'plan_enrollment_integration_sample.csv'),
        'text/csv'
      )
    end
  end
end
