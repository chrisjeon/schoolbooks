FactoryGirl.define do
  factory :room do
    account
    name { Faker::Lorem.word }
  end
end
