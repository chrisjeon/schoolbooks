FactoryGirl.define do
  factory :terms_of_service do
    account
    name { Faker::Lorem.word }
    body { Faker::Lorem.paragraph }
    default false
  end
end
