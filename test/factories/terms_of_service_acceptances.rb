FactoryGirl.define do
  factory :terms_of_service_acceptance do
    account
    user
    terms_of_service
  end
end
