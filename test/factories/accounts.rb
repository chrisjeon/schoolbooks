FactoryGirl.define do
  factory :account do
    name { Faker::Company.name }
    email { Faker::Internet.email }
    time_zone 'EST'
    invoices_sent_on Account::INVOICES_SENT_ON_DAYS.sample
  end
end
