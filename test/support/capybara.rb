class ActionDispatch::IntegrationTest
  require 'capybara/rails'
  require 'capybara/poltergeist'

  include Capybara::DSL

  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end

Capybara.register_driver :poltergeist do |app|
  options = {
    timeout: 180,
    js_errors: true,
    inspector: true,
    window_size: ['1300', '1800'],
    phantomjs_options: ['--proxy-type=none', '--load-images=no', '--ignore-ssl-errors=true']
  }

  Capybara::Poltergeist::Driver.new(app, options)
end

Capybara.javascript_driver = :poltergeist
Capybara.default_max_wait_time = 5
