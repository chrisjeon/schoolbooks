require 'test_helper'

class TodosTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver

    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)

    @user = create(:user, account: @account)
    @user.add_role :admin

    sign_in @user
  end

  test 'can add todo item' do
    todo_text = 'ololo'

    visit root_path

    has_selector?('.portlet-title', text: 'todo')
    has_no_selector?('.list-group-item')

    find('.todoapp input[type="text"]').set(todo_text)
    find('.todo-send .btn').click

    has_selector?('.list-group-item', text: todo_text)

    visit root_path

    has_selector?('.list-group-item', text: todo_text)
  end

  test 'can reload the widget with a button' do
    visit root_path

    todo = create :todo, user: @user, account: @account

    has_selector?('.portlet-title', text: 'todo')
    has_no_selector?('.list-group-item')

    find('.ion-refresh').click

    has_selector?('.list-group-item', text: todo.body)
  end

  test 'can hide the widget with a button' do
    visit root_path

    has_selector?('.portlet-title', text: 'todo')

    find('.ion-close-round').click

    has_no_selector?('.portlet-title', text: 'todo')
  end

  test 'can archive multiple items' do
    todo_1_text = 'first item'
    todo_2_text = 'second item'
    todo_3_text = 'third item'

    [todo_1_text, todo_2_text, todo_3_text].each { |text| create :todo, user: @user, account: @account, body: text }

    visit root_path

    has_selector?('.todoapp h4', text: '0 of 3 remaining')
    has_selector?('.list-group-item', text: todo_1_text)
    has_selector?('.list-group-item', text: todo_2_text)
    has_selector?('.list-group-item', text: todo_3_text)

    find('.list-group-item', text: todo_1_text).find('label').click
    find('.list-group-item', text: todo_3_text).find('label').click

    has_selector?('.todoapp h4', text: '2 of 3 remaining')

    find('.btn', text: 'Archive').click

    has_valid_imtes = -> do
      has_selector?('.todoapp h4', text: '0 of 1 remaining')
      has_no_selector?('.list-group-item', text: todo_1_text)
      has_selector?('.list-group-item', text: todo_2_text)
      has_no_selector?('.list-group-item', text: todo_3_text)
    end

    has_valid_imtes.()

    visit root_path

    has_valid_imtes.()
  end
end
