require 'test_helper'

class ScheduleTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver

    @micro_plan = create(:micro_plan)
    @account = create(:account)
    @account_subscription = create(:account_subscription, account: @account, subscription_plan: @micro_plan)
    @class_schedule = create(:class_schedule, account: @account)

    @user = create(:user, account: @account)
    @user.add_role :admin

    sign_in @user

    @room = create(:room, account: @account)
  end

  def visit_wizard
    visit class_schedule_path(@class_schedule)

    find('.add-event.minutes-600').click

    assert_wizard_location
  end

  def create_students
    @student_1 = create(:user, account: @account, first_name: 'Andrew', last_name: 'Ivanoff')
    @student_2 = create(:user, account: @account, first_name: 'Ivan', last_name: 'Bogdanovich')
    @student_3 = create(:user, account: @account, first_name: 'Bruce', last_name: 'Andreev')

    [@student_1, @student_2, @student_3].each { |s| s.add_role :student }

    @student_1_name = 'Andrew Ivanoff'
    @student_2_name = 'Ivan Bogdanovich'
    @student_3_name = 'Bruce Andreev'
  end

  def student_names
    page.first('.table-select-person-wrapper').all('tbody > tr > td:nth-child(2)').map(&:text)
  end

  def assert_wizard_location
    page.has_selector?('.page-title small', text: 'Match Students to a Teacher')
  end

  test 'can filter students by name' do
    create_students
    visit_wizard

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_2_name)
    page.has_selector?('.tr', text: @student_3_name)

    searchbox = page.find('.filter-block-select-student input')

    searchbox.set('ivan')

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_2_name)
    page.has_no_selector?('.tr', text: @student_3_name)

    searchbox.set('andre')

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_3_name)
    page.has_no_selector?('.tr', text: @student_2_name)

    searchbox.set('andrew')

    page.has_selector?('.tr', text: @student_1_name)
    page.has_no_selector?('.tr', text: @student_3_name)
    page.has_no_selector?('.tr', text: @student_2_name)

    searchbox.set('')

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_2_name)
    page.has_selector?('.tr', text: @student_3_name)
  end

  test 'can filter students by level' do
    create_students

    level_A = create(:level, name: 'A')
    level_B = create(:level, name: 'B')

    LevelMembership.create(account: @account, user: @student_1, level: level_A)
    LevelMembership.create(account: @account, user: @student_2, level: level_A)
    LevelMembership.create(account: @account, user: @student_2, level: level_B)

    visit_wizard

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_2_name)
    page.has_selector?('.tr', text: @student_3_name)

    level_dropdown = page.find('.filter-block-select-student button')
    level_entry_selector = '.filter-block-select-student .dropdown-menu a'

    level_dropdown.click
    page.find(level_entry_selector, text: 'A').click

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_2_name)
    page.has_no_selector?('.tr', text: @student_3_name)

    level_dropdown.click
    page.find(level_entry_selector, text: 'B').click

    page.has_selector?('.tr', text: @student_2_name)
    page.has_no_selector?('.tr', text: @student_1_name)
    page.has_no_selector?('.tr', text: @student_3_name)

    level_dropdown.click
    page.first(level_entry_selector).click

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_2_name)
    page.has_selector?('.tr', text: @student_3_name)
  end

  test 'can sort students by name' do
    create_students
    visit_wizard

    page.has_selector?('.tr', text: @student_1_name)
    page.has_selector?('.tr', text: @student_2_name)
    page.has_selector?('.tr', text: @student_3_name)

    header_label = page.find('.th-wrapper', text: 'Name')

    header_label.has_no_selector?('.button-up.active')
    header_label.has_no_selector?('.button-down.active')
    assert_equal student_names, [@student_1_name, @student_2_name, @student_3_name]

    header_label.click

    header_label.has_selector?('.button-down.active')
    header_label.has_no_selector?('.button-up.active')
    assert_equal student_names, [@student_1_name, @student_3_name, @student_2_name]

    header_label.click

    header_label.has_selector?('.button-up.active')
    header_label.has_no_selector?('.button-down.active')
    assert_equal student_names, [@student_2_name, @student_3_name, @student_1_name]
  end

  test 'validate teacher presence' do
    visit_wizard

    page.find('.btn-success').click

    page.has_selector?('.notifyjs-wrapper', text: 'Teacher should be selected')
  end

  test 'validate students presence' do
    teacher = create(:user, account: @account)

    teacher.add_role :teacher

    visit_wizard

    page.find('.teacher tr', text: teacher.full_name).find('label').click
    page.find('.btn-success').click

    page.has_selector?('.notifyjs-wrapper', text: 'There should be at least 1 student')
  end

  test 'validate students pricing plan' do
    create_students

    teacher = create(:user, account: @account)

    teacher.add_role :teacher

    visit_wizard

    page.find('tr', text: @student_1_name).find('label').click
    page.find('.teacher tr', text: teacher.full_name).find('label').click
    page.find('.btn-success').click

    page.has_selector?('.notifyjs-wrapper', text: "#{@student_1_name} has no enough hours in pricing plan")
  end

  test 'validate room availability' do
    pricing_plan = create(:pricing_plan, hours_per_week: 50)

    create_students

    PlanEnrollment.create(account: @account, user: @student_1, pricing_plan: pricing_plan)

    teacher = create(:user, account: @account)

    teacher.add_role :teacher

    date = @class_schedule.start_end_time.start_on

    ClassSession.create(
      room: @room,
      teacher: teacher,
      account: @account,
      class_schedule: @class_schedule,
      start_end_time_attributes: {
        end_on: Time.zone.parse('11:00').change(day: date.day, year: date.year, month: date.month),
        start_on: Time.zone.parse('10:00').change(day: date.day, year: date.year, month: date.month),
        account_id: @account.id
      },
      class_attendances_attributes: [
        {
          user_id: @student_1.id,
          account_id: @account.id,
          class_schedule_id: @class_schedule.id
        }
      ]
    )

    visit class_schedule_path(@class_schedule)

    find('.add-event.minutes-60').click

    assert_wizard_location

    find('#end-picker').click
    find('.picker__list-item', text: '11:00').click
    page.find('.btn-success').click

    page.has_selector?('.notifyjs-wrapper', text: 'The room is not available for given time')
  end

  test 'can create class session' do
    pricing_plan = create(:pricing_plan, hours_per_week: 50)

    create_students

    PlanEnrollment.create(account: @account, user: @student_1, pricing_plan: pricing_plan)
    PlanEnrollment.create(account: @account, user: @student_2, pricing_plan: pricing_plan)
    PlanEnrollment.create(account: @account, user: @student_3, pricing_plan: pricing_plan)

    teacher = create(:user, account: @account)

    teacher.add_role :teacher

    visit_wizard

    page.find('tr', text: @student_1_name).find('label').click
    page.find('tr', text: @student_2_name).find('label').click
    page.find('.teacher tr', text: teacher.full_name).find('label').click
    page.find('.btn-success').click

    event = find('.event.minutes-600')

    event.has_selector?('.name', text: teacher.full_name)
    event.has_selector?('li', text: @student_1_name)
    event.has_selector?('li', text: @student_2_name)
    event.has_no_selector?('li', text: @student_3_name)
  end
end
