require 'test_helper'

class ExportAndSendScheduleJobTest < ActiveJob::TestCase
  test 'enques the mailer' do
    account = create(:account)

    room = create(:room, account: account)
    pricing_plan = create(:pricing_plan, account: account, hours_per_week: 20)
    class_schedule = create(:class_schedule, account: account)

    teacher = create(:user)
    student_1 = create(:user, first_name: 'Andrew')
    student_2 = create(:user, first_name: 'Daniel')

    teacher.add_role(:teacher)
    student_1.add_role(:student)
    student_2.add_role(:student)

    PlanEnrollment.create(user: student_1, account: account, pricing_plan: pricing_plan)
    PlanEnrollment.create(user: student_2, account: account, pricing_plan: pricing_plan)

    date = class_schedule.start_end_time.start_on

    class_session = ClassSession.create(
      room: room,
      teacher: teacher,
      account: account,
      class_schedule: class_schedule,
      start_end_time_attributes: {
        end_on: Time.zone.parse('11:00').change(day: date.day, year: date.year, month: date.month),
        start_on: Time.zone.parse('10:00').change(day: date.day, year: date.year, month: date.month),
        account_id: account.id
      },
      class_attendances_attributes: [
        {
          user_id: student_1.id,
          account_id: account.id,
          class_schedule_id: class_schedule.id
        },
        {
          user_id: student_2.id,
          account_id: account.id,
          class_schedule_id: class_schedule.id
        },
      ]
    )

    mail = mock('mail')
    emails = [teacher.email, student_1.email, student_2.email]
    csv_string = "\"Name\",\"Teacher\",\"Class\",\"Timetable\"\n"

    [student_1, student_2].each do |student|
      csv_string << "\"#{student.full_name}\",\"#{teacher.full_name}\",\"#{room.name}\",\"10:00-11:00\"\n"
    end

    ScheduleExportsMailer.expects(:schedule).with(emails, csv_string).returns(mail)
    mail.expects(:deliver_later)

    ExportAndSendScheduleJob.perform_now(class_schedule)
  end
end
