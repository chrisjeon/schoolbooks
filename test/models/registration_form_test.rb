require 'test_helper'

class RegistrationFormTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
  end
end
