require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  context 'relations' do
    should have_one(:note).dependent(:destroy)
    should belong_to :resource
    should belong_to :account
  end
end
