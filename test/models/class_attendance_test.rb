require 'test_helper'

class ClassAttendanceTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
    should belong_to :class_schedule
    should belong_to :class_session
    should belong_to :user
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :class_schedule
    should validate_presence_of :class_session
    should validate_presence_of :user
  end
end
