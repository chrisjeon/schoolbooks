require 'test_helper'

class NoteTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :noteable
  end
end
