require 'test_helper'

class LevelTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:level_memberships).dependent(:destroy)
    should have_many(:level_members).through(:level_memberships).source(:user)
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :name
    should validate_uniqueness_of(:name).scoped_to(:account_id)
  end
end
