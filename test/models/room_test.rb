require 'test_helper'

class RoomTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many :class_sessions
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :name
  end
end
