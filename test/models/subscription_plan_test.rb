require 'test_helper'

class SubscriptionPlanTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:account_subscriptions).dependent(:destroy)
    should have_many(:accounts).through(:account_subscriptions)
  end

  context 'validations' do
    should validate_presence_of :name
  end
end
