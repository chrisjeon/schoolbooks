require 'test_helper'

class TermsOfServiceTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
    should have_many(:terms_of_service_acceptances).dependent(:destroy)
    should have_many(:users).through(:terms_of_service_acceptances)
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :name
    should validate_presence_of :body
  end

  test 'there should only be one default terms of service' do
    account = create(:account)
    tos_1 = create(:terms_of_service, account: account, default: true)
    tos_2 = create(:terms_of_service, account: account, default: true)
    assert !tos_1.reload.default?
    assert tos_2.reload.default?
  end
end
