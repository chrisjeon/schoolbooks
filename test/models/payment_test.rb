require 'test_helper'

class PaymentTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:payment_items).dependent(:destroy)
    should belong_to :account
    should belong_to :user
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :user
    should validate_presence_of :due_on
    should validate_inclusion_of(:payment_method).in_array(Payment::PAYMENT_METHODS).allow_nil
  end

  test '#set_discount callback discount is nil' do
    payment = create(:payment, discount: nil)
    assert_equal 0.0, payment.discount
  end

  test '#set_discount callback discount is 24.5' do
    payment = create(:payment, discount: 24.5)
    assert_equal 0.245, payment.discount
  end

  test '#set_discount callback discount is 0.13' do
    payment = create(:payment, discount: 0.13)
    assert_equal 0.13, payment.discount
  end

  test '#set_total callback when discount is 0' do
    payment = create(:payment, subtotal: 100, discount: 0)
    assert_equal 100, payment.total.to_f
  end

  test '#set_total callback when discount is 0.125' do
    payment = create(:payment, subtotal: 100, discount: 0.25)
    assert_equal 75.0, payment.total.to_f
  end

  test '#paid_on presence validation if unpaid' do
    payment = build(:payment, paid: false, paid_on: nil)
    assert payment.valid?
  end

  test '#paid_on presence validation if paid' do
    payment = build(:payment, paid: true, paid_on: nil)
    assert !payment.valid?
  end

  test '#set_payment_items_to_paid callback' do
    payment = create(:payment, paid: false)
    3.times do
      misc_payment_item = create(:misc_payment_item, account: payment.account)
      create(:payment_item,
             itemable: misc_payment_item,
             account: payment.account,
             payment: payment,
             paid: false)
    end
    payment.update_attributes(paid: true, paid_on: DateTime.now)
    payment.reload.payment_items.each do |payment_item|
      assert payment_item.paid?
    end
  end
end
