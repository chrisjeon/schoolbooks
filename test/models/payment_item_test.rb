require 'test_helper'

class PaymentItemTest < ActiveSupport::TestCase
  context 'relations' do
    should have_one(:note).dependent(:destroy)
    should belong_to :itemable
    should belong_to :account
    should belong_to :user
    should belong_to :payment
  end

  context 'validations' do
    should validate_presence_of :itemable
    should validate_presence_of :account
    should validate_presence_of :user
  end
end
