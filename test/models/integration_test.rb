require 'test_helper'

class IntegrationTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
    should have_attached_file(:file)
    should validate_attachment_content_type(:file).allowing(%w(text/comma-separated-values text/csv application/csv application/excel))
  end
end
