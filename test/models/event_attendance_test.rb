require 'test_helper'

class EventAttendanceTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:payment_items).dependent(:destroy)
    should belong_to :event
    should belong_to :event_session
    should belong_to :user
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :event
    should validate_presence_of :user
    should validate_presence_of :account
  end
end
