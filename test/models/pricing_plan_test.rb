require 'test_helper'

class PricingPlanTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:plan_enrollments).dependent(:destroy)
    should have_many(:users).through(:plan_enrollments)
    should have_many(:start_end_times).dependent(:destroy)
    should have_many(:registration_informations).dependent(:destroy)
    should have_one(:note).dependent(:destroy)
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :name
    should validate_presence_of :frequency
    should validate_numericality_of(:frequency).only_integer
    should validate_numericality_of(:frequency).is_greater_than(0)
    should validate_presence_of :frequency_unit
    should validate_inclusion_of(:frequency_unit).in_array(%w(day week))
    should validate_uniqueness_of(:code).scoped_to(:account_id)
  end

  test 'random unique code should be set if it is blank' do
    pricing_plan = create(:pricing_plan, name: 'Lalala', code: nil)
    assert_equal "LALALA-#{pricing_plan.id}", pricing_plan.code
  end
end
