require 'test_helper'

class EventSessionTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:event_attendances).dependent(:destroy)
    should have_many(:attendees).through(:event_attendances).source(:user)
    should have_one(:start_end_time).dependent(:destroy)
    should belong_to :account
    should belong_to :event
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :event
  end
end
