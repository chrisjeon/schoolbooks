require 'test_helper'

class LevelMembershipTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
    should belong_to :user
    should belong_to :level
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :user
    should validate_presence_of :level
  end
end
