require 'test_helper'

class TermsOfServiceAcceptanceTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
    should belong_to :user
    should belong_to :terms_of_service
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :user
    should validate_presence_of :terms_of_service
  end
end
