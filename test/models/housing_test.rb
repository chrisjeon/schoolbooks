require 'test_helper'

class HousingTest < ActiveSupport::TestCase
  context 'relations' do
    should have_one(:note).dependent(:destroy)
    should have_many(:housing_occupancies).dependent(:destroy)
    should have_many(:occupants).through(:housing_occupancies).source(:user)
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :owner_name
    should validate_presence_of :email
    should validate_presence_of :phone
    should validate_presence_of :maximum_occupancy
    should validate_uniqueness_of :email
  end
end
