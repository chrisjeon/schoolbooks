require 'test_helper'

class UserTest < ActiveSupport::TestCase
  context 'relations' do
    should have_one(:plan_enrollment).dependent(:destroy)
    should have_one(:pricing_plan).through(:plan_enrollment)
    should have_one(:registration_information).dependent(:destroy)
    should have_many(:level_memberships).dependent(:destroy)
    should have_many(:levels).through(:level_memberships)
    should have_many(:housing_occupancies).dependent(:destroy)
    should have_many(:housings).through(:housing_occupancies)
    should have_many(:event_attendances).dependent(:destroy)
    should have_many(:attended_events).through(:event_attendances).source(:event)
    should have_many(:attended_event_sessions).through(:event_attendances).source(:event_session)
    should have_many(:teaching_classes).class_name('ClassSession').with_foreign_key(:user_id)
    should have_many(:class_attendances).dependent(:destroy)
    should have_many(:class_sessions).through(:class_attendances)
    should have_many(:payments).dependent(:destroy)
    should have_many(:payment_items).dependent(:destroy)
    should have_many(:misc_payment_items).dependent(:destroy)
    should have_many(:todos).dependent(:destroy)
    should have_many(:terms_of_service_acceptances).dependent(:destroy)
    should have_many(:terms_of_services).through(:terms_of_service_acceptances)
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :first_name
    should validate_presence_of :last_name
    should have_attached_file(:avatar)
    should validate_attachment_content_type(:avatar).
           allowing('image/png', 'image/gif').
           rejecting('text/plain', 'text/xml')
    should validate_attachment_size(:avatar).in(0..5.megabytes)
  end

  test '#full_name' do
    user = create(:user,
                  email: 'chris0374@gmail.com',
                  password: 'password',
                  password_confirmation: 'password',
                  first_name: 'Chris',
                  last_name: 'Jeon')
    assert_equal 'Chris Jeon', user.full_name
  end

  test '#set_class_schedule_preference callback' do
    user = build(:user,
                 email: 'chris0374@gmail.com',
                 password: 'password',
                 password_confirmation: 'password',
                 first_name: 'Chris',
                 last_name: 'Jeon')

    user.class_schedule_preference = 'both'
    user.save
    assert user.reload.available_in_morning?
    assert user.reload.available_in_afternoon?

    user.class_schedule_preference = 'morning'
    user.save
    assert user.reload.available_in_morning?
    assert !user.reload.available_in_afternoon?

    user.class_schedule_preference = 'afternoon'
    user.save
    assert !user.reload.available_in_morning?
    assert user.reload.available_in_afternoon?
  end
end
