require 'test_helper'

class RegistrationInformationTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
    should belong_to :user
    should belong_to :pricing_plan
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :user
  end
end
