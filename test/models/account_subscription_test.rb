require 'test_helper'

class AccountSubscriptionTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :subscription_plan
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :subscription_plan
    should validate_presence_of :account
  end
end
