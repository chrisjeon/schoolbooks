require 'test_helper'

class ClassSessionTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:class_attendances).dependent(:destroy)
    should have_many(:students).through(:class_attendances).source(:user)
    should have_one :start_end_time
    should belong_to :account
    should belong_to :class_schedule
    should belong_to(:teacher).class_name('User').with_foreign_key(:user_id)
    should belong_to :room
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :class_schedule
    should validate_presence_of :room
  end
end
