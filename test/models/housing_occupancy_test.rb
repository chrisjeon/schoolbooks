require 'test_helper'

class HousingOccupancyTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:payment_items).dependent(:destroy)
    should have_one(:start_end_time).dependent(:destroy)
    should belong_to :user
    should belong_to :account
    should belong_to :housing
  end

  context 'validations' do
    should validate_presence_of :user
    should validate_presence_of :account
    should validate_presence_of :housing
  end
end
