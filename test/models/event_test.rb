require 'test_helper'

class EventTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:event_sessions).dependent(:destroy)
    should have_many(:event_attendances).dependent(:destroy)
    should have_many(:attendees).through(:event_attendances).source(:user)
    should have_one(:start_end_time).dependent(:destroy)
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :name
    should validate_presence_of :maximum_capacity
    should validate_presence_of :currency
  end
end
