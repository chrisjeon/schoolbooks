require 'test_helper'

class MiscPaymentItemTest < ActiveSupport::TestCase
  context 'relations' do
    should have_one(:payment_item).dependent(:destroy)
    should belong_to :account
    should belong_to :user
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :user
  end
end
