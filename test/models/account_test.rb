require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  context 'relations' do
    should have_one(:registration_form).dependent(:destroy)
    should have_one(:account_subscription).dependent(:destroy)
    should have_one(:subscription_plan).through(:account_subscription)
    should have_many(:users).dependent(:destroy)
    should have_many(:levels).dependent(:destroy)
    should have_many(:level_memberships).dependent(:destroy)
    should have_many(:pricing_plans).dependent(:destroy)
    should have_many(:plan_enrollments).dependent(:destroy)
    should have_many(:rooms).dependent(:destroy)
    should have_many(:roles).dependent(:destroy)
    should have_many(:housings).dependent(:destroy)
    should have_many(:housing_occupancies)
    should have_many(:events).dependent(:destroy)
    should have_many(:event_sessions).dependent(:destroy)
    should have_many(:event_attendances).dependent(:destroy)
    should have_many(:class_schedules).dependent(:destroy)
    should have_many(:class_sessions).dependent(:destroy)
    should have_many(:class_attendances).dependent(:destroy)
    should have_many(:start_end_times).dependent(:destroy)
    should have_many(:payments).dependent(:destroy)
    should have_many(:payment_items).dependent(:destroy)
    should have_many(:misc_payment_items).dependent(:destroy)
    should have_many(:todos).dependent(:destroy)
    should have_many(:integrations).dependent(:destroy)
    should have_many(:terms_of_services).dependent(:destroy)
    should have_many(:terms_of_service_acceptances).dependent(:destroy)
    should have_many(:registration_informations).dependent(:destroy)
  end

  context 'validations' do
    should validate_presence_of :email
    should validate_presence_of :name
    should validate_presence_of :invoices_sent_on
    should validate_inclusion_of(:invoices_sent_on).in_array(Account::INVOICES_SENT_ON_DAYS)
    should validate_inclusion_of(:payments_due_on).in_array(Account::PAYMENTS_DUE_ON_DAYS)
    should validate_uniqueness_of(:name).case_insensitive
    should validate_uniqueness_of(:code).case_insensitive
  end

  test 'currency is set by default' do
    account = create(:account)
    assert_equal 'USD', account.currency
  end

  test 'creates registration form if the boolean value is set to true' do
    account = create(:account, has_registration_form: true)
    assert account.registration_form.present?
  end

  test 'does not create registration form if the boolean value is set to false' do
    account = create(:account, has_registration_form: false)
    assert_nil account.registration_form
  end

  test 'random unique code should be set if it is blank' do
    account = create(:account, name: 'Lalala', code: nil)
    assert_equal "LALALA-#{account.id}", account.code
  end
end
