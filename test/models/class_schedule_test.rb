require 'test_helper'

class ClassScheduleTest < ActiveSupport::TestCase
  context 'relations' do
    should have_many(:class_sessions).dependent(:destroy)
    should have_many(:class_attendances).dependent(:destroy)
    should have_many(:teachers).through(:class_sessions).source(:teacher)
    should have_many(:students).through(:class_attendances).source(:user)
    should have_one(:start_end_time)
    should belong_to :account
  end

  context 'validations' do
    should validate_presence_of :account
  end
end
