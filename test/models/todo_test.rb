require 'test_helper'

class TodoTest < ActiveSupport::TestCase
  context 'relations' do
    should have_one(:note).dependent(:destroy)
    should belong_to :account
    should belong_to :user
    should belong_to(:requester).class_name('User')
  end

  context 'validations' do
    should validate_presence_of :account
    should validate_presence_of :user
  end
end
