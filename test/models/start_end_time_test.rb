require 'test_helper'

class StartEndTimeTest < ActiveSupport::TestCase
  context 'relations' do
    should belong_to :account
    should belong_to :timeable
  end

  context 'validations' do
    should validate_presence_of :account
  end

  test '#valid_start_and_end_dates valid dates' do
    account = create(:account)
    pricing_plan = create(:pricing_plan, account: account)
    start_end_time = build(:start_end_time, account: account, timeable: pricing_plan)
    assert start_end_time.valid?
  end

  test '#valid_start_and_end_dates invalid dates' do
    account = create(:account)
    pricing_plan = create(:pricing_plan, account: account)
    start_end_time = build(:start_end_time, account: account, timeable: pricing_plan)
    start_end_time.start_on = start_end_time.end_on + 3.days
    assert !start_end_time.valid?
  end

  test '#set_on_and_end_on' do
    account = create(:account)
    pricing_plan = create(:pricing_plan, account: account)
    start_end_time = build(:start_end_time, account: account, timeable: pricing_plan)
    start_end_time.start_date = '11/2/1988'
    start_end_time.start_time = '11:11 PM'
    start_end_time.end_date = '11/3/1988'
    start_end_time.end_time = '11:11 PM'
    start_end_time.valid?
    assert_equal '02 Nov', start_end_time.start_on.to_date.to_formatted_s(:short)
    assert_equal '03 Nov', start_end_time.end_on.to_date.to_formatted_s(:short)
  end
end
